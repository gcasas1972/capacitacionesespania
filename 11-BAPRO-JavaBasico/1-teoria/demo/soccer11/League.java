package com.demo.soccer11;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class League {
    // la copa America

    public static void main(String args[]) {
        League theLeague = new League();
        Team[] theTeams=theLeague.createTeams("The Robins, The Crows, The Swallows", 3);
        Game[] theGames = createGames(theTeams);
        Game currGame = theGames[0];
        // currGame.playGame();
        currGame.playGame(3);
        System.out.println(currGame.getDescription());
        theLeague.showBestTeam(theTeams);

    }

    public  Team[] createTeams(String teamNames, int teamSize) {
        PlayerDatabase playerDB=new PlayerDatabase();
        StringTokenizer teamNameTokens= new StringTokenizer(teamNames, ",");
        Team[] theTeams=new Team[teamNameTokens.countTokens()];
         for (int i=0; i<theTeams.length-1;i++){
             theTeams[i]=new Team(teamNameTokens.nextToken(),
             playerDB.getTeam(teamSize));
         }
     
       return theTeams;
    }

    public static Game[] createGames(Team[] theTeams) {
int daysBetweenGames=0;

ArrayList<Game> theGames=new ArrayList();
for(Team homeTeam: theTeams){
    for(Team awayTeam: theTeams){
        if(homeTeam!=awayTeam){
            daysBetweenGames+=7;
            //theGames.add(new Game(homeTeam,awayTeam,LocalDateTime.now()));
            theGames.add(new Game(homeTeam,awayTeam,LocalDateTime.now().plusDays(daysBetweenGames)));
        }
}
}
       return (Game[]) theGames.toArray(new Game[1]);

    }

    public void showBestTeam(Team[] theTeams) {
        Team currBestTeam = theTeams[0];

        System.out.println("\n Team Points");
        for (Team currTeam : theTeams) {
            try{
            currBestTeam = currTeam.getPointsTotal() > currBestTeam.getPointsTotal() ? currTeam : currBestTeam;
            System.out.println(currTeam.getTeamName() + ": " + currTeam.getPointsTotal());
            }catch(Exception e){
            break;
            }
        System.out.println("\n El ganador de la LIGA es " + currBestTeam.getTeamName());
    }
}
    public String getLeagueAnnouncement(Game[] theGames){
        Period thePeriod= Period.between(theGames[0].getTheDateTime().toLocalDate(),
                theGames[theGames.length -1].getTheDateTime().toLocalDate());
        return "La liga se agendo para " + thePeriod.getMonths() + " meses, y  " +
                thePeriod.getDays() + " dias \n";
    }
}
