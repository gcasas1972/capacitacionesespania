package com.demo.soccer8;

import com.demo.soccer.*;

public class League {
    // la copa America

    public static void main(String args[]) {
        Player player1 = new Player();
        player1.playerName = "George Eliot";
        Player player2 = new Player();
        player2.playerName = "Graham Greene";
        Player player3 = new Player();
        player3.playerName = "Geoffrey Chaucer";

        Player[] thePlayers = {player1, player2, player3};
        Team team1 = new Team();
        team1.teamName = "The Greens";
        team1.playerArray = thePlayers;


        Team team2 = new Team();
        team2.teamName = "The Reds";
        team2.playerArray = new Player[3];
        team2.playerArray[0] = new Player();
        team2.playerArray[0].playerName = "Robert Service";
        team2.playerArray[1] = new Player();
        team2.playerArray[1].playerName = "Robbie Burns";
        team2.playerArray[2] = new Player();
        team2.playerArray[2].playerName = "Rafael Sabatini";

// 4to ejercicio 8-1
        Team[] theTeams = createTeams(team1, team2);
        Game[] theGames = createGames(theTeams);
        Game currGame = theGames[0];
       // currGame.playGame();
        currGame.playGame(3);
System.out.println(currGame.getDescription());


    }

     public static Team[] createTeams(Team team1, Team team2) {
        Team[] theTeams = {team1, team2};
        return theTeams;
    }

    public static Game[] createGames(Team[] theTeams) {
        Game theGame = new Game();
        theGame.homeTeam = theTeams[0];
        theGame.awayTeam = theTeams[1];
        Game[] theGames = {theGame};
        return theGames;

    }
}
