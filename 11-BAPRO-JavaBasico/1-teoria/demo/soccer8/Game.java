package com.demo.soccer8;

import com.demo.soccer.*;

public class Game {

    // Game es el partido
    public Team homeTeam; // equipo local
    public Team awayTeam; // equipo visitante
    public Goal[] goals;

    public void playGame(int maxGoals) {
        int numberOfGoals = (int) (Math.random() * (maxGoals + 1));
         this.calcGoals(numberOfGoals);
    }
      public void playGame() {
        int numberOfGoals = (int) (Math.random() * 7);
       this.calcGoals(numberOfGoals);
    }
     private void calcGoals(int numberOfGoals){
        System.out.println(numberOfGoals);
        Goal[] theGoals = new Goal[numberOfGoals];
        System.out.println(theGoals.length);
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
     } 
      
    public StringBuilder getDescription(){
        StringBuilder returnString= new StringBuilder();
        int i=0;
        for (Goal currGoal: this.goals){
            returnString.append("El Gol despues " + this.goals[i].theTime 
            + " mins de " + this.goals[i].thePlayer.playerName 
            + " del equipo " + this.goals[i].theTeam.teamName + "\n"
            );
            i++;
        }
        return returnString;
    }
}
