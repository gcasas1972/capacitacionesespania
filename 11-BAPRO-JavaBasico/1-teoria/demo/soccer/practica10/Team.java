package com.demo.soccer.practica10;

public class Team {
   // equipo onda.... Boca Juniors
    private String teamName;
    public Player[] playerArray;
    private int pointsTotal;

    /**
     * @return the pointsTotal
     */
    public int getPointsTotal() {
        return pointsTotal;
    }

    /**
     * @param pointsTotal the pointsTotal to set
     */
    public void setPointsTotal(int pointsTotal) {
        this.pointsTotal = pointsTotal;
    }
    
    public void incPointsTotal(int pointsTotal){
        this.setPointsTotal(this.getPointsTotal() + pointsTotal);
    }
            
    /**
     * @return the teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName the teamName to set
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
 
}