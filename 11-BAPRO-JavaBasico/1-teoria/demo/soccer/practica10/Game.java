package com.demo.soccer.practica10;

public class Game {
// Visibilidad o Accesibilidad
// 4 p's = PUBLIC, PRIVATE, default (PACKAGE), PROTECTED
// encapsulamiento de la informacion ...PRIVATE los atributos y PUBLIC getters/setters

    int homeTeamGoals = 0;
    int awayTeamGoals = 0;

    // Game es el partido
    public Team homeTeam; // equipo local
    public Team awayTeam; // equipo visitante
    public Goal[] goals;

    public void playGame(int maxGoals) {
        int numberOfGoals = (int) (Math.random() * (maxGoals + 1));
        this.calcGoals(numberOfGoals);
    }

    public void playGame() {
        int numberOfGoals = (int) (Math.random() * 7);
        this.calcGoals(numberOfGoals);
    }

    private void calcGoals(int numberOfGoals) {
        System.out.println(numberOfGoals);
        Goal[] theGoals = new Goal[numberOfGoals];
        System.out.println(theGoals.length);
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
    }

    public StringBuilder getDescription() {
        StringBuilder returnString = new StringBuilder();
        returnString.append(homeTeam.getTeamName()+ " vs " + awayTeam.getTeamName()+ "\n");
        int i = 0;
        for (Goal currGoal : this.goals) {
            if(currGoal.getTheTeam()== homeTeam){
                homeTeamGoals++; //homeTeamGoals=homeTeamGoals+1
            }else{
                awayTeamGoals++;
            }
            
            returnString.append("El Gol despues " + this.goals[i].theTime
                    + " mins de " + this.goals[i].thePlayer.playerName
                    + " del equipo " + this.goals[i].getTheTeam().getTeamName() + "\n"
            );
            i++;
        } // FIN DEL FOR
        if(homeTeamGoals==awayTeamGoals){
            returnString.append("Fue un EMPATE \n");
            homeTeam.incPointsTotal(1);
            awayTeam.incPointsTotal(1);
        }else if(homeTeamGoals>awayTeamGoals){
             returnString.append("Gano el equipo LOCAL: \n" + homeTeam.getTeamName());
             homeTeam.incPointsTotal(2);
        }else{
            returnString.append("Gano el equipo VISITANTE: \n " + awayTeam.getTeamName());
            awayTeam.incPointsTotal(2);
        }
        
        returnString.append( "( " + homeTeamGoals + " - " + awayTeamGoals + " )");
        
        return returnString;
    }
}
