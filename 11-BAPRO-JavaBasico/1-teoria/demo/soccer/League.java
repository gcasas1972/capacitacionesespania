package com.demo.soccer;

public class League {
     // la copa America
    public static void main(String args[]){
    // 1er ejercicio - Practica 6-1
   
    Player player1=new Player();
    player1.playerName="George Eliot";
    Player player2=new Player();
    player2.playerName="Graham Greene";
    Player player3=new Player();
    player3.playerName="Geoffrey Chaucer";
    
    Player[] thePlayers={player1, player2, player3};
    Team team1=new Team();
    team1.teamName="The Greens";
    team1.playerArray=thePlayers;
    
//    for (Player thePlayer: team1.playerArray){
//        System.out.println(thePlayer.playerName);
//    }
    
    Team team2=new Team();
    team2.teamName="The Reds";
    team2.playerArray=new Player[3];
    team2.playerArray[0]=new Player();
    team2.playerArray[0].playerName="Robert Service";
    team2.playerArray[1]=new Player();
    team2.playerArray[1].playerName="Robbie Burns";
    team2.playerArray[2]=new Player();
    team2.playerArray[2].playerName="Rafael Sabatini";
    
//     for (Player thePlayer: team2.playerArray){
//        System.out.println(thePlayer.playerName);
//    }

     imprimir(team1.playerArray);
     imprimir(team2.playerArray);
     
     // 2do ejercicio - Practica 6-2
     Game currGame= new Game();
     currGame.homeTeam=team1;
     currGame.awayTema=team1;
     
     Goal goal1=new Goal();
     goal1.thePlayer=currGame.homeTeam.playerArray[2];
     goal1.theTeam=currGame.homeTeam;
     goal1.theTime=55;
     
     Goal[] theGoals={goal1};
     currGame.goals=theGoals;
     
     System.out.println("El gol a los " + currGame.goals[0].theTime + 
             " minutos hecho por " + currGame.goals[0].thePlayer.playerName + 
             " del equipo " + currGame.goals[0].theTeam.teamName             
             );
     
     // 3er Ejercicio - Practica 7-1
     for (Player thePlayer: team2.playerArray){
         if (thePlayer.playerName.matches(".*Sab.*")){ // expresion regular
             System.out.println("Encontro a " + thePlayer.playerName);
             
             System.out.println("el Apellido es " + thePlayer.playerName.split(" ")[1]);
         }
     }
     
     StringBuilder familyNameFirst= new StringBuilder();
        for (Player thePlayer: team1.playerArray){
            String name[]= thePlayer.playerName.split(" ");
            familyNameFirst.append(name[1]);
            familyNameFirst.append(", ");
            familyNameFirst.append(name[0]);
            familyNameFirst.append(" | ");
        }
      System.out.println(familyNameFirst);
     
    }
    
    public static void imprimir(Player[] team){
       for (Player thePlayer: team){
        System.out.println(thePlayer.playerName);
    }
    }
    
}
