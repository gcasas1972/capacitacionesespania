package com.demo.soccer10;

public class Goal {
   private Team theTeam;
    public Player thePlayer;
    public double theTime;
    /**
     * @return the theTeam
     */
    public Team getTheTeam() {
        return theTeam;
    }

    /**
     * @param theTeam the theTeam to set
     */
    public void setTheTeam(Team theTeam) {
        this.theTeam = theTeam;
    }
 
}
