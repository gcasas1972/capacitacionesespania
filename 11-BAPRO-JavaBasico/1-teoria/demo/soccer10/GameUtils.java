package com.demo.soccer10;


public class GameUtils {
    public static void addGameGoals(Game currGame){
      for (int i=0;i<currGame.goals.length;i++){
        int numberOfTeam =(int) (Math.random() * 2 ); // son 2 equipos
        int minuto=(int)(Math.random() * 90 );
        int numberOfPlayer=(int)(Math.random() * 2 ); // son 3 jugadores
        
        switch (numberOfTeam){
            case 0: // equipo local
                Goal goal=new Goal();
                goal.thePlayer=currGame.homeTeam.playerArray[numberOfPlayer];
                goal.theTime=minuto;
                currGame.goals[i]=goal;
                currGame.goals[i].setTheTeam(currGame.homeTeam);
                currGame.goals[i].theTime=minuto;
                currGame.goals[i].thePlayer.playerName=currGame.homeTeam.playerArray[numberOfPlayer].playerName;
                break;
            case 1: // equipo visitante
                 Goal goal1=new Goal();
                goal1.thePlayer=currGame.awayTeam.playerArray[numberOfPlayer];
                goal1.theTime=minuto;
                currGame.goals[i]=goal1;
                currGame.goals[i].setTheTeam(currGame.awayTeam);
                currGame.goals[i].theTime=minuto;
                currGame.goals[i].thePlayer.playerName=currGame.awayTeam.playerArray[numberOfPlayer].playerName;
        }
        
      }
    }
}
