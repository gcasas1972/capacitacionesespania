package com.demo.soccer10;


public class Game {
int homeTeamGoals=0;
int awayTeamGoals=0;
    // Game es el partido
    public Team homeTeam; // equipo local
    public Team awayTeam; // equipo visitante
    public Goal[] goals;

    public void playGame(int maxGoals) {
        int numberOfGoals = (int) (Math.random() * (maxGoals + 1));
         this.calcGoals(numberOfGoals);
    }
      public void playGame() {
        int numberOfGoals = (int) (Math.random() * 7);
       this.calcGoals(numberOfGoals);
    }
     private void calcGoals(int numberOfGoals){
        System.out.println(numberOfGoals);
        Goal[] theGoals = new Goal[numberOfGoals];
        System.out.println(theGoals.length);
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
     } 
      
    public StringBuilder getDescription(){
        StringBuilder returnString= new StringBuilder();
        int i=0;
        for (Goal currGoal: this.goals){
            // agregar en Goal los getters/setters
            if (currGoal.getTheTeam() == homeTeam) {
                homeTeamGoals++;
            } else {
                awayTeamGoals++;
            }
            returnString.append("El Gol despues " + this.goals[i].theTime 
            + " mins de " + this.goals[i].thePlayer.playerName 
            + " del equipo " + this.goals[i].getTheTeam().getTeamName() + "\n"
            );
            i++;
        }
        if(homeTeamGoals == awayTeamGoals){
            returnString.append("Fue un EMPATE");
            homeTeam.incPointsTotal(1);
            awayTeam.incPointsTotal(1);
        } else if(homeTeamGoals > awayTeamGoals) {
            returnString.append("Gano el equipo LOCAL: " + homeTeam.getTeamName()+"\n");
            homeTeam.incPointsTotal(2);
        }else{
             returnString.append("Gano el equipo VISITANTE: " + awayTeam.getTeamName()+"\n");
            awayTeam.incPointsTotal(2);
        }
        
        return returnString;
    }
}
