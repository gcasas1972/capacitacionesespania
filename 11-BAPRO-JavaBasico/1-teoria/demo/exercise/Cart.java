package com.demo.exercise;

import java.util.Date;

/**
 *
 * @author Administrador
 */
public class Cart {
   private Item[] items;
    private Date date;
    private double total;
    private int indice; // asume inicio en 0
    private int cant;
    
    public Cart(int cant){
        // relacion TODO/PARTE, COMPOSITION, de rombo negro
        this.cant=cant;
        this.items= new Item[cant];
    }
    
    public boolean addItem( Item i ){
      if (indice  <= cant){
        this.items[indice]=i;
        indice++;
        } else {
           this.checkOut(); // cohesion interna
            return false;
        }
        return true;
    }
    
    public boolean checkOut(){
        System.out.println("items comprados");
        for (int i =0; i<this.items.length;i++){
            System.out.println(items[i].getDesc()+" " + items[i].getQuantity() + " total $" +
                    items[i].getPrice() * items[i].getQuantity());
        }
        System.out.println("finalizo la compra, hace checkout");
        return true;
    }
    
    public boolean cancel(){
         return true;
    }
    
    /**
     * @return the items
     */
    public Item[] getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(Item[] items) {
        this.items = items;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }
 
    
    
    
}
