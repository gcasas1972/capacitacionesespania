/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.exercise;

/**
 *
 * @author Administrador
 */
public class Customer {

    private String address;
    private String billingInfo;
    private int customerNumber;
    private int orderNumber;
    private int age;
    private String name;
    
    private Item[] items;  // lo recibo x parametro en el constructor
   // private Cart cart; // lo construyo con el NEW 
    
    public Customer(Item[] items){
        // relacion llamada COMPOSITION
      //  this.cart =new Cart(2); // la realcion de TODO/PARTE de rombo negro
        // relacion llamada AGGEGARION, TODO/PARTE de rombo blanco
        this.items=items;
    }
    
    public double requestDiscount(){
        return 0.0;
    }
    public void shop(){
        Cart cart= new Cart(this.items.length); // relacion de USO
        for (int i =0; i<this.items.length;i++){
             cart.addItem(items[i]);
        }
        cart.checkOut();
    }
    
    public void displayCustomer(){
        System.out.println(this.getName());
        System.out.println(this.getAge());
    }
        
    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the billingInfo
     */
    public String getBillingInfo() {
        return billingInfo;
    }

    /**
     * @param billingInfo the billingInfo to set
     */
    public void setBillingInfo(String billingInfo) {
        this.billingInfo = billingInfo;
    }

    /**
     * @return the customerNumber
     */
    public int getCustomerNumber() {
        return customerNumber;
    }

    /**
     * @param customerNumber the customerNumber to set
     */
    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * @return the orderNumber
     */
    public int getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }


}
