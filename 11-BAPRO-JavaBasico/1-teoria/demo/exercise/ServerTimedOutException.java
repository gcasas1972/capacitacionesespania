
package com.demo.exercise;


public class ServerTimedOutException extends Exception{
    private int puerto;
    public ServerTimedOutException(String mensaje, int puerto){
        super(mensaje);
        this.puerto=puerto;
    }
      public int getPuerto() {
        return puerto;
    }
}
