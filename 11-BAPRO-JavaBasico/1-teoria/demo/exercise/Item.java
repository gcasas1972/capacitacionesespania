/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.exercise;

/**
 *
 * @author Administrador
 */
public  class Item {


    private int id;
    private String desc;
    private double price;
    private int quantity;
    private char color;
    private String size;
    
    public void displayItem(){
    System.out.println("el item " + this.desc + " cuesta " + this.price + 
            " y para " + this.quantity + "\n el costo total asciende a " + (this.price*this.quantity));
    }
    
    public void setItemFields(String desc, int quantity, double price){
        this.desc=desc;
        this.quantity=quantity;
        this.price=price;
    }
     public int setItemFields(String desc, int quantity, double price, char colorCode){
        if (colorCode ==' '){
            return -1;
        }else {
            this.color=colorCode;
            this.setItemFields(desc, quantity, price);
            return 0;
        }
    }

    
    public void setSize(String sizeArg){ 
       this.size = sizeArg;
     }


    public char getColor() {
        return color;
    }


    public boolean setColor(char color) {
        this.color = color;
        if (color ==' '){
            return false;
        }else {
            return true;
        }
    }
 
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }
 
    
}
