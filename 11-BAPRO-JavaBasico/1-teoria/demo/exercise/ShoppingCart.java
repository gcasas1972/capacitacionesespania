package com.demo.exercise;

public class ShoppingCart {

    public static void main(String[] args) {
        System.out.println("Welcome to the Shopping Cart!");
        // lista de Alejandra
        // POLIMORFISMO
        Item i = new Item();
        i.setDesc("camisa");
        i.setQuantity(2);
        i.setPrice(400);
        i.setId(1);
        System.out.println("1 color es: " + i.setColor('a'));
        i.setSize(ItemSizes.mMed);  // LLAMADA AL ATRIBUTO STATIC mMed
        // clase.atributoStatic
        // no hacer ItemSizes is= new ItemSizes();
        // no hacer is.mMed;

        //public void setItemFields(String desc, int quantity, double price){       
        i.setItemFields("vestido", 3, 4500);
        i.displayItem();
        if (i.setItemFields("vestido", 3, 4500, ' ') > 0) {
            i.displayItem();
        } else {
            System.out.println("codigo de color NO VALIDO");
        }

        i.setItemFields("vestido", 3, 4500, 'A');
        i.displayItem();

        Item i2 = new Item();
        i2.setDesc("pantalon");
        i2.setQuantity(1);
        i2.setPrice(700);
        i2.setId(2);
        System.out.println("2 color es: " + i2.setColor(' '));

        Item i3 = new Item();
        i3.setDesc("saco");
        i3.setQuantity(1);
        i3.setPrice(1600.0);
        i3.setId(3);

        Item[] items = new Item[3];
        items[0] = i;
        items[1] = i2;
        items[2] = i3;

        Customer maridoAlejandra = new Customer(items); // es la relacion de uso, la flecha comun
        maridoAlejandra.setName("Daniel Orlando");
        int spaceIdx = maridoAlejandra.getName().indexOf(" "); // BUSCAR EL ESPACIO TIPO STRING
        System.out.println("el espacio esta en la posicion " + maridoAlejandra.getName().indexOf(32)); // BUSCAR EL ESPACIO CON EL NRO ASCII DEL CHAR

        String apellido = maridoAlejandra.getName().substring(spaceIdx + 1);
        System.out.println(apellido);
        String nombre = maridoAlejandra.getName().substring(0, spaceIdx);
        System.out.println(nombre);
        System.out.println("la O esta en la posicion " + maridoAlejandra.getName().indexOf(79)); // BUSCO LA 0 CON EL CHAR DEL ASCII

        System.out.println("el espacio esta en la posicion " + maridoAlejandra.getName().indexOf(" "));

        StringBuilder sb = new StringBuilder("Ale");
        sb.append(" Orlando");
        System.out.println(sb);

        //maridoAlejandra.shop();
    }
}
