package com.demo.exercise;
/**
 *
 * @author Administrador
 */
// ANTIPATTERN .... GOD ... BLOB ... BIG LARGE OBJECT
public class ShoppingCartGod {
    private String custName;
    private String itemDesc;
    private String[] itemsDesc;
    private String message;
    private double price;
    private double tax;
    private int quantity; // cantidad de 1 item
    private int quantityTypeItem; // cuantos items desea comprar
    private double total;
    private boolean outOfStock=false;
    
// Use an if statement to test the quantity of the item:
//if it is > 1, concatenate an 's' to message so that it indicates multiple items. 
    
    public static void main (String[] args) { 
        System.out.println("Welcome to the Shopping Cart!");
        
        ShoppingCartGod carrito=new ShoppingCartGod();
        carrito.setCustName("Maria Smith");
        carrito.setItemDesc("camisa");
        carrito.setQuantity(5);
        carrito.setTax(1.21);
        carrito.setPrice(1500);
        carrito.setQuantityTypeItem(2);
        
        String[] items={"camisa","pantalon","corbata","media"};
        carrito.setItemsDesc(items);
        System.out.println("los productos a comprar son ");
        for (String elementos : items){
            System.out.println(elementos);
        }
        
        
        if (carrito.getQuantity()>1){
             carrito.setMessage(carrito.getCustName()+" quiere comprar " +carrito.getQuantity()+" " + carrito.getItemDesc() +"s");
        } else {
            carrito.setMessage(carrito.getCustName()+" quiere comprar " +carrito.getQuantity()+" " + carrito.getItemDesc());
       }
        System.out.println("el cliente quiere comprar " +carrito.getQuantityTypeItem() +" productos");
        System.out.println(carrito.getMessage());
        
        if (carrito.isOutOfStock()){ // getter
            System.out.println(carrito.getItemDesc()+" no disponible en stock");
        }else{
        System.out.println("Total del Costo + Tax " + carrito.getPrice()*carrito.getQuantity()*carrito.getTax());
    }
    }
    
    
    /**
     * @return the custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     * @return the itemDesc
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * @param itemDesc the itemDesc to set
     */
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(double tax) {
        this.tax = tax;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }
 
/**
     * @return the outOfStock
     */
    public boolean isOutOfStock() {
        return outOfStock;
    }

    /**
     * @param outOfStock the outOfStock to set
     */
    public void setOutOfStock(boolean outOfStock) {
        this.outOfStock = outOfStock;
    }
       /**
     * @return the itemsDesc
     */
    public String[] getItemsDesc() {
        return itemsDesc;
    }

    /**
     * @param itemsDesc the itemsDesc to set
     */
    public void setItemsDesc(String[] itemsDesc) {
        this.itemsDesc = itemsDesc;
    }

    /**
     * @return the quantityTypeItem
     */
    public int getQuantityTypeItem() {
        return quantityTypeItem;
    }

    /**
     * @param quantityTypeItem the quantityTypeItem to set
     */
    public void setQuantityTypeItem(int quantityTypeItem) {
        this.quantityTypeItem = quantityTypeItem;
    }
}
