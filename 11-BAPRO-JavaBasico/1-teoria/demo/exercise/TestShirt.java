package com.demo.exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class TestShirt {

    // es un ATRIBUTO, podria verse como una variable global
    static String lista;

    public static void main(String args[]) {
        // declaracion POLIMORFICA  de objetos
        // item es la clase padre ABSTRACTA
        //   Item i= new Item(); // item es astract, no puede ser instaciado
        Item i1 = new Shirt();
        Item i2 = new Hat();
        Item i3 = new Trousers();
        // returnable es una INTERFACE
        Returnable r1 = new Trousers();
        Returnable r2 = new Shirt();

        i1.setColor('A');
        i1.setDesc("camisa leñadora");
        i1.setPrice(800);
        i2.setDesc("tengo un sombrero");
        i3.setDesc("pantalones de corderoy rosa");

        mostrar(i1);
        mostrar(r1);
        mostrar();
    }

    public static void mostrar() {
        // DECLARAR VARIABLE LOCAL
        var list = new ArrayList<String>();
        Vector v = new Vector(); // es un objeto viejo de la version 1.0
        String[] nums = {"uno", "dos", "tres"}; // [] SE DEFINE UN VECTOR
        List<String> myList = Arrays.asList(nums); // llamada estatica asList, sin el NEW
        
        for (var s : myList) {
            System.out.print(s.toUpperCase() + ", ");
            
        }
        System.out.println("Despues del FOR " + myList);
       
        myList.replaceAll(s ->s.toUpperCase()); // -> operador unario, expresion LAMBDA
        System.out.println("Aplicando LAMBAS " + myList);
        
         myList.removeIf (s -> s.equals("uno"));
         myList.removeIf (s -> s.length() < 2);
System.out.println("Despues del REMOVE " + myList);
    }

    // OVERLOAD --- Sobrecarga
    public static void mostrar(Item i) { // recibo la clase padre, la abstracta
        if (i instanceof Shirt) {
            System.out.println((Shirt) i); // casting a la clase hija
            ((Shirt) i).print();
            System.out.println(((Shirt) i).doReturn());

        } else if (i instanceof Hat) {
            System.out.println((Hat) i); // imprime Hat@4617c264
            System.out.println(((Hat) i).getDesc()); // tengo un sombrero
        } else {
            System.out.println((Trousers) i); // Trousers@4617c264
            System.out.println(((Trousers) i).getDesc()); // pantalones de corderoy rosa
        }
    }

    public static void mostrar(Returnable r) {
        if (r instanceof Shirt) {
            System.out.println(((Shirt) r).doReturn());
        } else if (r instanceof Trousers) {
            System.out.println(((Trousers) r).doReturn());
        }

    }

    public static void probar() {
        Shirt shirt = new Shirt();
        shirt.setColor("rojo");
        shirt.setDesc("camisa leñadora");
        shirt.setPrice(800);
        shirt.setColorCode(4);
        System.out.println(shirt);
        shirt.setSize();
        System.out.println(shirt.getSize());

    }
    
   
}
