package com.demo.exercise;

public class Shirt extends Item implements Printable, Returnable{
    private String desc;
    private double price;
    private String color;
    private int colorCode;
    
      @Override
    public String doReturn() {
       return "devolvi la camisa leñadora";
    }
    // override del Object
    public String toString(){
    return "This shirt is a " + getDesc() + ";"
       + " price: " + getPrice() + ","
        + " color: " + getColor(getColorCode());
  }
    // override del Item
public void setSize(){
    super.setSize("XXXX");
    System.out.println("puse un nuevo tamaño");
}
// implementando el metodo definido en la interfaz Printable
    @Override
    public void print() {
        System.out.println("estoy en el print de Shirt");
    }
    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the color
     */
    public String getColor(int colorCode) {
        
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the colorCode
     */
    public int getColorCode() {
        return colorCode;
    }

    /**
     * @param colorCode the colorCode to set
     */
    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

  


}
