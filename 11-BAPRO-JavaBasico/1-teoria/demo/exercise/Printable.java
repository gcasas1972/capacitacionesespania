package com.demo.exercise;

public interface Printable {
    public void print();
}
