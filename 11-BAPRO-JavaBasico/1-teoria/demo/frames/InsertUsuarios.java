package com.demo.frames;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertUsuarios {
        public static void main(String[] args){
        String url="jdbc:h2:~/test";
        Connection con;
        Statement stmt;

        try {
            // establecer la conexion 
            con=DriverManager.getConnection(url,"sa","");
            stmt=con.createStatement();
            stmt.executeUpdate("insert into USUARIOS "
                    + "values(1, 'Ale', 'aaaa')");
            stmt.executeUpdate("insert into USUARIOS "
                    + "values(2, 'Manuel', 'bbbb')");
            stmt.executeUpdate("insert into USUARIOS "
                    + "values(3, 'Alfredo', 'cccc')");
            stmt.executeUpdate("insert into USUARIOS "
                    + "values(4, 'Tania', 'dddd')");
            stmt.executeUpdate("insert into USUARIOS "
                    + "values(5, 'Kiana', 'eeee')");
  stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        
    }
}
