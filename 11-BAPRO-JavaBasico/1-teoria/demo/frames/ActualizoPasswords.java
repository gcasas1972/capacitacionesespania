
package com.demo.frames;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ActualizoPasswords {
  // aaaa es -->           74b87337454200d4d33f80c4663dc5e5
  // bbbb es -->           65ba841e01d6db7733e90a5b7f9e6f80
    
    public static void main(String[] args){
        String url="jdbc:h2:~/test";
        Connection con;
        Statement stmt;

        try {
            // establecer la conexion 
            con=DriverManager.getConnection(url,"sa","");
            stmt=con.createStatement();
            stmt.executeUpdate("update USUARIOS "
                    + "set password='74b87337454200d4d33f80c4663dc5e5' where name='Ale'");
            stmt.executeUpdate("update USUARIOS "
                    + "set password='65ba841e01d6db7733e90a5b7f9e6f80' where name='Manuel'");
  stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        
    }
}


    

