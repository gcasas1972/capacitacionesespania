package com.demo.frames;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CrearUsuarios {
   public static void main(String[] args){
        String url="jdbc:h2:~/test";
        Connection con;
        String query="create table USUARIOS "
                + "(ID int,"
                + " NAME varchar(32),"
                + " PASSWORD varchar(32))";
        Statement stmt;
  // se hace 1 vez al principio la validacion de la existencia del driver      
        try{
            Class.forName("org.h2.Driver");
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
 // fin de la validacion
        try {
            // establecer la conexion 
            con=DriverManager.getConnection(url,"sa","");
            stmt=con.createStatement();
            stmt.executeUpdate(query);
            
            stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        
    }
}
