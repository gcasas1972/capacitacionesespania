package com.demo.soccer12;

import com.demo.soccer11.*;


public class Team {
    // equipo onda.... Boca Juniors
    private String teamName;
    private Player[] playerArray;
    private int pointsTotal;
    
    public Team(String teamName, Player[] playerArray){
        this.teamName=teamName;
        this.playerArray=playerArray;
        
    }
 
    public void incPointsTotal(int pointsTotal){
        this.pointsTotal+= pointsTotal;
    }
    
    
    /**
     * @return the pointsTotal
     */
    public int getPointsTotal() {
        return pointsTotal;
    }

    /**
     * @param pointsTotal the pointsTotal to set
     */
    public void setPointsTotal(int pointsTotal) {
        this.pointsTotal = pointsTotal;
    }

    
    /**
     * @return the teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param teamName the teamName to set
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * @return the playerArray
     */
    public Player[] getPlayerArray() {
        return playerArray;
    }

    /**
     * @param playerArray the playerArray to set
     */
    public void setPlayerArray(Player[] playerArray) {
        this.playerArray = playerArray;
    }

}
