package com.demo.soccer12;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Game {

    private LocalDateTime theDateTime;
    int homeTeamGoals = 0;
    int awayTeamGoals = 0;
    // Game es el partido
    public Team homeTeam; // equipo local
    public Team awayTeam; // equipo visitante
    public GameEvent[] gameEvents;
    

    public Game() {
    }

    public Game(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;

    }

    public Game(Team homeTeam, Team awayTeam, LocalDateTime theDateTime) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.theDateTime = theDateTime;
    }

//    public void playGame(int maxGoals) {
//        int numberOfGoals = (int) (Math.random() * (maxGoals + 1));
//        this.calcGoals(numberOfGoals);
//    }
    public void playGame() {
//        int numberOfGoals = (int) (Math.random() * 7);
//        this.calcGoals(numberOfGoals);
        ArrayList<GameEvent> eventList = new ArrayList();
        GameEvent currEvent;
        for (int i = 1; i <= 90; i++) {
            if (Math.random() > 0.95) {
                // System.out.println(i);
         //       currEvent = new Goal();
        currEvent = Math.random()>0.6? new Goal(): new Possession();    
                    currEvent.setTheTeam(Math.random() > 0.5 ? homeTeam : awayTeam);
                    currEvent.setThePlayer(currEvent.getTheTeam().getPlayerArray()
                            [(int) (Math.random()
                            * currEvent.getTheTeam().getPlayerArray().length)]);
                    currEvent.setTheTime(i);
                    eventList.add(currEvent);
            }
        }
        this.gameEvents = new Goal[eventList.size()];
        eventList.toArray(gameEvents);
    }

    private void calcGoals(int numberOfGoals) {
        System.out.println(numberOfGoals);
        GameEvent[] theGoals = new Goal[numberOfGoals];
        System.out.println(theGoals.length);
        this.gameEvents = theGoals;
        GameUtils.addGameGoals(this);
    }

    public StringBuilder getDescription() {
        StringBuilder returnString = new StringBuilder();
        int i = 0;
        for (GameEvent currEvent : this.gameEvents) {
            // agregar en Goal los getters/setters
            if (currEvent.getTheTeam() == homeTeam) {
                homeTeamGoals++;
            } else {
                awayTeamGoals++;
            }
//            returnString.append("El Gol despues " + this.goals[i].theTime
//                    + " mins de " + this.goals[i].thePlayer.playerName
//                    + " del equipo " + this.goals[i].getTheTeam().getTeamName() + "\n"
//            );
            returnString.append("El Gol despues " + this.gameEvents[i].getTheTime()
                    + " mins de " + this.gameEvents[i].getThePlayer().playerName
                    + " del equipo " + this.gameEvents[i].getTheTeam().getTeamName() + "\n"
                    + "Date " + this.getTheDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE) + "\n"
            );

            i++;
        }
        if (homeTeamGoals == awayTeamGoals) {
            returnString.append("Fue un EMPATE");
            homeTeam.incPointsTotal(1);
            awayTeam.incPointsTotal(1);
        } else if (homeTeamGoals > awayTeamGoals) {
            returnString.append("Gano el equipo LOCAL: " + homeTeam.getTeamName() + "\n");
            homeTeam.incPointsTotal(2);
        } else {
            returnString.append("Gano el equipo VISITANTE: " + awayTeam.getTeamName() + "\n");
            awayTeam.incPointsTotal(2);
        }

        return returnString;
    }

    /**
     * @return the theDateTime
     */
    public LocalDateTime getTheDateTime() {
        return theDateTime;
    }

    /**
     * @param theDateTime the theDateTime to set
     */
    public void setTheDateTime(LocalDateTime theDateTime) {
        this.theDateTime = theDateTime;
    }
}
