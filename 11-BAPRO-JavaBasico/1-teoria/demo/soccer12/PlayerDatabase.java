package com.demo.soccer12;

import com.demo.soccer11.*;
import java.util.ArrayList;
import java.util.StringTokenizer;
 
public class PlayerDatabase {
    private ArrayList<Player> players;
    String authorList="George Eliot,Graham Greene,Geoffrey Chaucer,Robert Service,Robbie Burns,Rafael Sabatini";
    
    public PlayerDatabase(){
        StringTokenizer authorTokens= new StringTokenizer(authorList, ",");
        players=new ArrayList();
        while(authorTokens.hasMoreTokens()){
            players.add(new Player(authorTokens.nextToken()));
        }
    }
    public Player[] getTeam(int numberOfPlayers){
        Player[] teamPlayers = new Player[numberOfPlayers];
        for (int i=0; i<numberOfPlayers;i++){
            int playerIndex =(int)(Math.random()*players.size());
            teamPlayers[i]=players.get(playerIndex);
            players.remove(playerIndex);
        }
        return teamPlayers;
    }
}
