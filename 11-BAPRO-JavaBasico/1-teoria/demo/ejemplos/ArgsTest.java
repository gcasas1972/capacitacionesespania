package com.demo.ejemplos;

public class ArgsTest {

    public static void main(String[] args) {
        System.out.println("args[0] is " + args[0]);
        System.out.println("args[1] is " + args[1]);
// no suma dos numeros pasados por argumento, los concatena
        System.out.println("Total is:" + (args[0] + args[1]));
        // int -->tipo primitivo
        // su wrapper class es INTEGER
        // parseInt --> metodo para convertir un String en un INTEGER
//        int arg1 = Integer.parseInt(args[0]);
//        int arg2 = Integer.parseInt(args[1]);
//        System.out.println("Total is: " + (arg1 + arg2));

    }

}
