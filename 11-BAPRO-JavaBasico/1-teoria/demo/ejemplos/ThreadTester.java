package com.demo.ejemplos;


public class ThreadTester {
   public static void main(String args[]) {
       HolaRunner r=new HolaRunner();
       Thread t= new Thread(r);
       t.start();
       Thread t1= new Thread(r);
       t1.start();
   } 
}


class HolaRunner implements Runnable{
int i;
    @Override
    public void run() {
       i=0;
       while(true){
           System.out.println("hola " + i++);
           try{
              Thread.sleep(100);
           } catch (InterruptedException e){
               System.out.println(e.getMessage());
           }
           if (i == 50){
               break;
           }
       }
    }
    
}