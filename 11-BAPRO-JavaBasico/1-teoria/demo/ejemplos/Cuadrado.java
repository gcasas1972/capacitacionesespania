
package com.demo.ejemplos;

public class Cuadrado {
    // no uso STATIC
    // calcularPerimetro es un metodo de INSTANCIA
    public double calcularPerimetro(double lado){
      //  double rpta = lado *4;
      //  return rpta;
        return lado * 4;
    }
}
