package com.demo.ejemplos;

public class DemoPPT13 {

    public static void main(String args[]) {
        Object o = new Object(); 
        
        // obtengo el hashCode del objeto, de manera predeterminada
        System.out.println(o); // java.lang.Object@76ed5528
        System.out.println(o.toString());// java.lang.Object@76ed5528
        System.out.println(new Object()); // java.lang.Object@2c7b84de
        // override del metodo toString de Object
        System.out.println(new StringBuilder("hola gente"));
         System.out.println(new First()); //com.demo.ejemplos.First@5acf9800
        
    }
}

class First{
    public String toString(){ // override del metodo toString heredado de Object
        return "hola Ale";
    }
}
class Second{}