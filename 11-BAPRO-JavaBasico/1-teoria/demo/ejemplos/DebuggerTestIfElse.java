package com.demo.ejemplos;

public class DebuggerTestIfElse {

    public static void main(String args[]) {
        String saludo="hola";
        boolean isLeapYear = true;
        int month = 1;
        if (month == 1 || month == 3 || month == 5 || month == 7
                || month == 8 || month == 10 || month == 12) {
            System.out.println("31 days in the month.");
            saludo="todo ok";
        } else if (month == 2) {
            if (!isLeapYear) {
                System.out.println("28 days in the month.");
            } else {
                System.out.println("29 days in the month.");
            }
        } else if (month == 4 || month == 6 || month == 9
                || month == 11) {
            System.out.println("30 days in the month.");
        } else {
            System.out.println("Invalid month.");
        }
        // salto la logica
         if (isLeapYear) {
         isLeapYear=false;
         }
        saludo="chau";
        System.out.println(saludo);
    }

}
