/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class TestMatrices {

    public static void main(String[] args) {
        String[][] ciudades = {{"BsAs", "Sao Pablo", "Madrid"},
        {"Argentina", "Brasil", "España"}};

        for (int i = 0; i < ciudades.length; i++) {// recorre por filas length=2
            for (int j = 0; j < ciudades[i].length; j++) { // recorre por columnas 3
                System.out.print(ciudades[i][j] + " ");
            }
            System.out.println("");
        }

        String[][] rios = new String[3][4];
        rios[0][0] = "Missippi";
        rios[0][1] = "Duero";
        rios[0][2] = "Rhin";
        rios[0][3] = "Po";
        rios[1][0] = "Guadalquivir";
        rios[1][1] = "Tamesis";
        rios[1][2] = "Sena";
        rios[1][3] = "Amazonas";
        rios[2][0] = "de La Plata";
        rios[2][1] = "Negro";
        rios[2][2] = "Pilcomayo";
        rios[2][3] = "Nilo";

        for (int i = 0; i < rios.length; i++) {// recorre por filas length=2
            for (int j = 0; j < rios[i].length; j++) { // recorre por columnas 3
                System.out.print(rios[i][j] + " ");
            }
            System.out.println("");
        }

        int[][] valores = new int[5][5];
        for (int i = 0; i < valores.length; i++) {// recorre por filas length=2
            for (int j = 0; j < valores[i].length; j++) { // recorre por columnas 3
                valores[i][j] = (int) (Math.random() * 100);
            }
        }

        for (int i = 0; i < valores.length; i++) {// recorre por filas length=2
            for (int j = 0; j < valores[i].length; j++) { // recorre por columnas 3
                System.out.print(valores[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
