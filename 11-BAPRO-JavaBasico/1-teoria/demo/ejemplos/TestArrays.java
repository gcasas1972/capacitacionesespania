/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class TestArrays {

    public static void main(String[] args) {
        // 1)
        String[] names; // declarando el tipo de dato de los elementos
        names = new String[3]; //declaro la dimension o largo del vector
        names[0] = "Georgianna";
        names[1] = "Jen";
        names[2] = "Simon";
        //2)
        String[] nombres = {"Alejandra", "Daniel", "Juan"}; // declaro tipo, dimension, y valores
        //3)
        String[] valores = new String[3]; //declarar tipo y dimension
        valores[0] = "Peru";
        valores[1] = "Argentina";
        valores[2] = "Uruguay";

        // RECORRER UN ARRAY
        //for(int i=0;i<=2;i++){
        for (int i = 0; i < names.length; i++) { // posicion de arranque; posicion final; incremento
            System.out.println("el valor de la posicion " + i + " es " + names[i]);
        }

        for (int i = valores.length - 1; i >= 0; i--) { // lee de atras para adelante
            System.out.println("el valor de la posicion " + i + " es " + valores[i]);
        }
        System.out.println("****************************************");
        // SALIR DE UN FOR ANTES DE QUE PROCESE TODOS LOS ELEMENTOS
        for (int i = 0; i < nombres.length; i++) { // posicion de arranque; posicion final; incremento
            if (nombres[i] == "Daniel") {
                System.out.println("encontre a Daniel");
                break;
            }
            System.out.println("el valor de la posicion " + i + " es " + nombres[i]);
        }
        System.out.println("****************************************");
        for (int i = 0; i < nombres.length; i++) { // posicion de arranque; posicion final; incremento
            if (nombres[i] == "Daniel") {
                System.out.println("encontre a Daniel");
                continue;
            }
            System.out.println("el valor de la posicion " + i + " es " + nombres[i]);
        }
        System.out.println("****************************************");
        for (String element : valores) { // for each
            System.out.println(element);
        }
        System.out.println("****************************************");

        int[] myArray = {1, 2, 3, 4, 5, 6};
        // nuevo array más grande
        int[] hold = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        // copia todo lo de myArray al array hold
        System.arraycopy(myArray, 0, hold, 0, myArray.length);
        
        for (int element : hold) { // for each
            System.out.println(element);
        }
    }
}
