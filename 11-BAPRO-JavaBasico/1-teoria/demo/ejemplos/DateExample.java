
package com.demo.ejemplos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.JapaneseDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateExample {
    public static void main(String args[]){
        LocalDate myDate=  LocalDate.now(); // static now()
        
 System.out.println("****************************************");
          System.out.println( myDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
       
System.out.println("****************************************");
  JapaneseDate jDate = JapaneseDate.from(myDate);
  System.out.println("Japanese date: "+ jDate);
  System.out.println("****************************************");

  LocalDateTime today = LocalDateTime.now();
 System.out.println("Today's date time (no formatting): " + today);

 String sdate = 
    today.format(DateTimeFormatter.ISO_DATE_TIME);
  System.out.println("Date in ISO_DATE_TIME format: " + sdate);

String fdate = 
   today.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));
  System.out.println("Formatted with MEDIUM FormatStyle: " + fdate);

  
    }
}
