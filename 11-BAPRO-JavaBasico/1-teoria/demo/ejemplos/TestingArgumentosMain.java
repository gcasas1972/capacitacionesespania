package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class TestingArgumentosMain {
    // para que se ejecute la lectura de estos argumentos
    // hay que hacer el RUN desde el icono con la flecha VERDE
    public static void main(String[] args){
        for(int i=0;i<args.length;i++)
            System.out.println("veo " + args[i]);
    }
}
