/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class TestDemoNros {
public static void main(String args[]){
        // instanciar un objeto de DemoNros
        DemoNros demoNros=new DemoNros();
        demoNros.setNroA(9);
        demoNros.setNroB(6);
        demoNros.sumar();
     
       int valor= demoNros.multiplicar();
       System.out.println(valor);
       System.out.println(demoNros.multiplicar());
       imprimir();
       
       TestDemoNros demo=new TestDemoNros();
       demo.mostrar();
        System.out.println("**********************");
       demo.ecuacionesReducidas();
    }


public static void imprimir(){
    byte nroByte = 32; // 8 bits
    int nroInt = 27;   // 32 bits
    nroByte = (byte) nroInt; // casting 
    System.out.println(nroByte);
    
    String valor = "123"; // string es una cadena de caracters
    //int valor2 = Integer.getInteger(valor); // asignado a una variable de tipo primitiva
    System.out.println(Integer.getInteger(valor));
}
    

public void mostrar(){
    float nroF = 51.49f;
    double nroD = 39.44;
    double nroX=0.0;
    String nombre; // el valor default null
   // nombre = new String();
    
    System.out.println(Math.round(nroF));
    System.out.println(Math.ceil(nroD)); // ceiling ...cielorraso , redondea para arriba
    System.out.println(Math.floor(nroD)); // floor ...piso, redondea para abajo

    System.out.println(Math.ceil(Math.sqrt(2))); // es un patron de diseño DECORATOR

}

 public void ecuacionesReducidas(){
     int x= 4;
     x++;
     System.out.println(x); // asume x= x+1
     
     x+=2;
    System.out.println(x); // asume x = x +2
    int y=1;
    
    y= x++ ; // se usa para guardar el valor anterior en y
    System.out.println("x: "+x+"y: " + y);
    
    x=7;    
    y= ++x ; // se pisa el valor anterior
    System.out.println("x: "+x+"y: " + y);
    
    y*=2; // y=y*2
    
    Math.pow(y, x); // power = potencia
 }   
}
