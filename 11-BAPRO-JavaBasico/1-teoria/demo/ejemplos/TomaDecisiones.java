package com.demo.ejemplos;

public class TomaDecisiones {
    public static void main(String args[]){
     // 1er ejemplo
        boolean feliz=true;
        if (feliz)
            System.out.println("Estoy feliz");
        
        
     // 2do ejemplo
        boolean triste=false;
        if (triste){
            System.out.println("Estoy triste");
        } else {
            System.out.println("Estoy feliz");
        }
    // 3er ejemplo
         int i= 5;
         
         if ( i == 5)
             System.out.println("i es 5");
    // 4to ejemplo
          i=0;
         if ( i == 5){
            System.out.println("i es 5");
         } else {
            System.out.println("i NO ES 5");
         }
         
     // 5TO ejemplo
     int maxDistancia=10;
     int distanciaAcasa=11;
     int velocidad=5;
     
     if (distanciaAcasa > maxDistancia){
         System.out.println("no llego");
     } else {
         System.out.println("llego justo");
     }
       // 6TO ejemplo
     if (distanciaAcasa < maxDistancia){
         System.out.println("no llego");
     } else if (velocidad>=5) {
         System.out.println("llego justo");
     } else {
         System.out.println("me quede sin nafta");
     }
     
      // 7mo ejemplo
     if (distanciaAcasa < maxDistancia){
         if (velocidad >=5){
              System.out.println("llego justo");
         } else {
             System.out.println("me quede sin nafta");
         }
     }
     
      // 7mo BIS ejemplo
     if (distanciaAcasa < maxDistancia && velocidad >=5){
               System.out.println("llego justo");
         } else {
             System.out.println("me quede sin nafta");
         }
     
     // 8vo ejemplo
     int a = 6;
     int b= 8;
     if (a != b){
         System.out.println("a NO ES b");
     }
     
     if (Math.abs(a-b) < 3){
         System.out.println("a - b");
     }
   
     String rpta=(a>b?"ok":"not ok"); // if reducido
      System.out.println(rpta);
    }
}
