package com.demo.ejemplos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ArrayListExample {

    public static void main(String args[]) {
        // ejemplo TIPADO, solo acepta STRING
        ArrayList<String> names = new ArrayList();

        names.add("Jamie");
        names.add("Gustav");
        names.add("Alisa");
        names.add("Jose");
        names.add(2, "Prashant");

        names.remove(0);
        names.remove(names.size() - 1);
        names.remove("Gustav");

        System.out.println(names);

    // ejemplo NO TIPADO, donde los elementos son cualquier cosas    
   //List list = new ArrayList();
        ArrayList list = new ArrayList();
// acepta elementos de valor duplicado
        list.add("one");
        list.add("second");
        list.add("3rd");
        list.add(4);
        list.add( 5.0F);
        list.add("second"); // duplicate, is added
        list.add(4); // duplicate, is added
 System.out.println(list);
  System.out.println("****************************************");
 //Set set = new HashSet();
 HashSet set = new HashSet();
 set.add("one");
 set.add("second");
 set.add("3rd");
 set.add(4);
 set.add(5.0F);
 set.add("second"); // duplicate, not added
 set.add(4); // duplicate, not added
 System.out.println(set);
 
 
 
    }
}
