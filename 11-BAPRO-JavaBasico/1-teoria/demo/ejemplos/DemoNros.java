package com.demo.ejemplos;

public class DemoNros {
    // ATRIBUTOS
    // variables, propiedades, caracteristicas, 
    // campos, 
    // inicializacion default para INT es CERO
    private int nroA;
    private int nroB;
    private int nroC=8;
    private String nombre="Ale"; // default es null
    
    // METODOS
    // rutinas, subrutinas, funciones, 
    // comportamiento, operatoria, acciones, servicios
    
    public void sumar(){ // void indica que es una RUTINA
        // rpta es una VARIABLE
        // NO ASUME VALOR CERO COMO DEFAULT
        int rpta=0;
        rpta = getNroA() + getNroB();
        System.out.println("la suma dio " + rpta);
        int rpta1=this.nroA + this.nroB;
        System.out.println("la suma2 dio " + rpta1);
         // no necesito definir una variable
        System.out.println("la suma3 dio " + (this.nroA + this.nroB));
    }
    public int multiplicar(){ // int indica que es una FUNCION, y necesita RETURN
        return getNroA() * getNroB();
    }
    
     /**
     * @return the nroA
     */
    public int getNroA() {
        return nroA;
    }
    /**
     * @param nroA the nroA to set
     */
    public void setNroA(int nroA) {
        this.nroA = nroA;
    }

    /**
     * @return the nroB
     */
    public int getNroB() {
        return nroB;
    }

    /**
     * @param nroB the nroB to set
     */
    public void setNroB(int nroB) {
        this.nroB = nroB;
    }

    /**
     * @return the nroC
     */
    public int getNroC() {
        return nroC;
    }

    /**
     * @param nroC the nroC to set
     */
    public void setNroC(int nroC) {
        this.nroC = nroC;
    }

}
