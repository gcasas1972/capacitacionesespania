package com.demo.ejemplos;

public class Consumer implements Runnable {

    private SyncStack theStack;
    private int num;
    private static int counter = 1;

    public Consumer(SyncStack s) {
        theStack = s;
        num = counter++;
    }

    @Override
    public void run() {
        char c;
        for (int i = 0; i < 200; i++) {
            c=theStack.pop(); // pop= pick on pile
             System.out.println("Consumer " + num + ": " + c);
           try{
               Thread.sleep((int)(Math.random()*3000));
           }catch (InterruptedException e){
               System.out.println(e.getMessage());
           }
        }
    }
}
