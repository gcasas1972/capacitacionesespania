package com.demo.ejemplos;

public class Test2 {
    public static void main(String args[]){
        Cuadrado nuevo= new Cuadrado();
        // nuevo es el nombre de la instancia de la clase Cuadrado
        // new es el operador que se usa para crear instancias
        
        System.out.println("el perimetro es " + nuevo.calcularPerimetro(4.5));
        // Cuadrado.calcularPerimetro(4.5) => es un metodo de clase 
        // y deberia estar marcado con STATIC
        
    }
    
}
