/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class Empleado {
   protected int salario;
   protected String nombre;
   private int ahorrosDolares;
   public String hobby;
   String email;
   
   protected void verDetalles(){
       System.out.println("el salario de " + this.getNombre() +" es " + this.getSalario());
   }
   
    /**
     * @return the salario
     */
    public int getSalario() {
        return salario;
    }

    /**
     * @param salario the salario to set
     */
    public void setSalario(int salario) {
        this.salario = salario;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * @return the ahorrosDolares
     */
    private int getAhorrosDolares() {
        return ahorrosDolares;
    }

    /**
     * @param ahorrosDolares the ahorrosDolares to set
     */
    private void setAhorrosDolares(int ahorrosDolares) {
        this.ahorrosDolares = ahorrosDolares;
    }
}
