/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class TestBucles {

    public static void main(String[] args) {
        int cont = 0;

        while (cont++ < 10) {
            System.out.println("contador: " + cont);
        }
        System.out.println("****************************************");
        
        //cont = 0;
        do {
            System.out.println("contador: " + cont);
        } while (cont++ < 10);

    }

}
