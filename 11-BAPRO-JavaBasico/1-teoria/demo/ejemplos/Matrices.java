package com.demo.ejemplos;

public class Matrices {

    public static void main(String args[]) {
        int[][] yearlySales = new int[5][4]; //FILA / COLUMNA
        yearlySales[0][0] = 1000;
        yearlySales[0][1] = 1500;
        yearlySales[0][2] = 1800;
        yearlySales[1][0] = 1000;
        yearlySales[3][3] = 2000;

        for (int row = 0; row < yearlySales.length; row++) { // recorre x filas
            for (int col = 0; col < yearlySales[row].length; col++) { // recorre x columnas
                System.out.print(yearlySales[row][col] + "---");
            }
            System.out.println();
        }
        ventasAnuales();
 //       dobleWhile();
//        dobleFor();
//        changeFloor(3);

        for (int i = 1; i < 5; i++) {
            System.out.print("i = " + i + "; ");
        }

    }

    public static int currentFloor = 1;

    public static void changeFloor(int targetFloor) {
        while (currentFloor != targetFloor) {
            if (currentFloor < targetFloor) {
                System.out.println("subiendo");
            } else {
                System.out.println("bajando");
            }
            currentFloor = targetFloor;
        }

    }

    public static void dobleWhile() {
        String name = "Lenny";
        String guess = "";
        int attempts = 0;
        while (!guess.equalsIgnoreCase(name)) {
            guess = "";
            while (guess.length() < name.length()) {
                char asciiChar = (char) (Math.random() * 26 + 97);
                guess += asciiChar;
            }
            attempts++;
        }
        System.out.println(name + " found after " + attempts + " tries!");

    }

    public static void dobleFor() {
        int height = 4, width = 10;

        for (int row = 0; row < height; row++) { // recorre x filas
            for (int col = 0; col < width; col++) { // recorre x columnas
                System.out.print("@");
            }
            System.out.println();
        }

        System.out.print("");
    }

    public static void ventasAnuales() {
        int sales[][] = new int[5][4];
         sales[0][0] = 1000;
        sales[0][1] = 1500;
        sales[0][2] = 1800;
        sales[1][0] = 1000;
        sales[3][3] = 2000;
       // initArray(sales);  //initialize the array
        System.out.println("Yearly sales by quarter beginning 2010:");
        for (int i = 0; i < sales.length; i++) {
            for (int j = 0; j < sales[i].length; j++) {
                System.out.println("\tQ" + (j + 1) + " " + sales[i][j]);
            }
            System.out.println();
        }

    }
}
