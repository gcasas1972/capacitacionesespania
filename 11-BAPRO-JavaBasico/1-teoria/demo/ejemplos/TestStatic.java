/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.ejemplos;

/**
 *
 * @author Administrador
 */
public class TestStatic {
    public static void  main(String[] args){
        imprimir();
        System.out.println(verEdad());
        // llamada a un metodo estatico de otra clase
        ClaseEstatica.mostrar();
        // se ve el protected  DENTRO DEL MISMO PACKAGE      
        Empleado e = new Empleado();
        e.nombre="aaa";
        e.salario=2333;
        // el PROTECTED SE COMPORTA COMO PACKAGE
        System.out.println(e.getNombre());
    }
    
    static void imprimir(){
        System.out.println("estoy en imprimir STATIC");
    }
    
    static int verEdad(){
        return 44;
    }
}
