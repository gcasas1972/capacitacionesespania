package com.demo.db;

import java.sql.*;


public class CrearCafe {
    public static void main(String[] args){
        String url="jdbc:h2:~/test";
        Connection con;
        String query="create table CAFES "
                + "(COF_NAME varchar(32),"
                + "SUP_ID int,"
                + "PRICE float,"
                + "SALES int)";
        Statement stmt;
  // se hace 1 vez al principio la validacion de la existencia del driver      
        try{
            Class.forName("org.h2.Driver");
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
 // fin de la validacion
        try {
            // establecer la conexion 
            con=DriverManager.getConnection(url,"sa","");
            stmt=con.createStatement();
            stmt.executeUpdate(query);
            
            stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        
    }
}
