package com.demo.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertCafes {
    public static void main(String[] args){
        String url="jdbc:h2:~/test";
        Connection con;
        Statement stmt;
        String query="Select * from CAFES";

        try {
            // establecer la conexion 
            con=DriverManager.getConnection(url,"sa","");
            stmt=con.createStatement();
            stmt.executeUpdate("insert into CAFES "
                    + "values('Colombian', 00101, 7.99, 0)");
            stmt.executeUpdate("insert into CAFES "
                    + "values('French Roast', 00049, 8.99, 0)");
            stmt.executeUpdate("insert into CAFES "
                    + "values('Colombian Decaf', 00151, 9.99, 0)");
            stmt.executeUpdate("insert into CAFES "
                    + "values('Expresso', 00101, 6.99, 0)");
            stmt.executeUpdate("insert into CAFES "
                    + "values('French Decaf', 00041, 7.99, 0)");
            
            
            // el query del tipo Select devuelve registros de datos
            ResultSet rs=stmt.executeQuery(query);
            System.out.println("Lista de Cafes y Precios");
            System.out.println("==========================");
            while (rs.next()){
                String s=rs.getString("COF_NAME");
                float f=rs.getFloat("PRICE");
                System.out.println(s + " " + f);
            }
            
            
            stmt.close();
            con.close();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        
    }
}
