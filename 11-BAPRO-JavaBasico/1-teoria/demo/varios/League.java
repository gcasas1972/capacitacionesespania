package com.demo.varios;


public class League {
         public static void main(String[] args){

             // 1er ejercicio - Practica 6-1
             Player player1= new Player();
             player1.playerName="George Eliot";
             Player player2= new Player();
             player2.playerName="Graham Greene"; 
             Player player3= new Player();
             player3.playerName="Geoffrey Chaucer"; 
             
             Player[] thePlayers={player1, player2, player3};
             Team team1= new Team();
             team1.teamName="The Greens";
             team1.playerArray= thePlayers;
             
             for (Player thePlayer: team1.playerArray){
                 System.out.println(thePlayer.playerName);
             }
             
             Team team2= new Team();
             team2.teamName="The Reds";
             team2.playerArray= new Player[3];
             team2.playerArray[0]= new Player();
             team2.playerArray[0].playerName="Robert Service";
             team2.playerArray[1]= new Player();
             team2.playerArray[1].playerName="Robbie Burns";
             team2.playerArray[2]= new Player();
             team2.playerArray[2].playerName="Rafael Sabatini";
             
               for (Player thePlayer: team2.playerArray){
                 System.out.println(thePlayer.playerName);
             }
             
             // 4to ejercicio - Practica 8-1
             Team[] theTeams= createTeams(team1, team2);
             Game[] theGames=createGames(theTeams);  
             
             Game currGame = theGames[0];
             currGame.playGame();
             // 2do ejercicio - Practica 6-2
//             Game currGame= new Game();
//             currGame.homeTeam=team1;
//             currGame.awayTeam=team2;
             
// 1er ejercicio - Practica 8-2
// *************
//int numberOfGoals = (int) (Math.random() *7);
//System.out.println(numberOfGoals);
//Goal[] theGoals=new Goal[numberOfGoals];
//System.out.println(theGoals.length);
// currGame.goals=theGoals;  
// GameUtils.addGameGoals(currGame);
// *************

// int i=0;
// for (Goal currGoal: currGame.goals){
//     System.out.println("Goal after " + currGame.goals[i].theTime+ 
//                     " mins by " + currGame.goals[i].thePlayer.playerName +
//                     " of " + currGame.goals[i].theTeam.teamName
//                     );
//     i++;
// }
 
 System.out.println(currGame.getDescription());
 
//             Goal goal1= new Goal();
//             goal1.thePlayer=currGame.homeTeam.playerArray[2];
//             goal1.theTeam=currGame.homeTeam;
//             goal1.theTime=55;
//             
//             Goal[] theGoals={goal1};
//             currGame.goals=theGoals;
             
//             System.out.println("Goal after " + currGame.goals[0].theTime + 
//                     " mins by " + currGame.goals[0].thePlayer.playerName +
//                     " of " + currGame.goals[0].theTeam.teamName
//                     );
//             
   // 3er ejercicio - Practica 7-1          
              for (Player thePlayer: team2.playerArray){
                  if (thePlayer.playerName.matches(".*Sab.*")){
                    System.out.println("Found " + thePlayer.playerName);
                    
                     System.out.println("Last name is " + 
                             thePlayer.playerName.split(" ")[1]
                             );
                    
                  }
             }
              
         StringBuilder familyNameFirst= new StringBuilder();
        for (Player thePlayer: team1.playerArray){   
            String name[]= thePlayer.playerName.split(" ");
            familyNameFirst.append(name[1]);
            familyNameFirst.append(", ");
            familyNameFirst.append(name[0]);
             familyNameFirst.append("; ");
        } 
            
        System.out.println(familyNameFirst);
             
         }
         // 1er ejercicio - Practica 8-1 
  public static Team[] createTeams(Team team1, Team team2){
      Team[] theTeams={team1,team2};
      return theTeams;
  } 
  // 2do y 3er ejercicio - Practica 8-1
  public static Game[] createGames(Team[] theTeams){
      Game theGame=new Game();
      theGame.homeTeam = theTeams[0];
      theGame.awayTeam = theTeams[1];
      Game[] theGames={theGame};
      return theGames;
  }
}
