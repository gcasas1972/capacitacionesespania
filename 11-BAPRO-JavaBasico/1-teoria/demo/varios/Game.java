package com.demo.varios;

public class Game {

    public Team homeTeam;
    public Team awayTeam;
    public Goal[] goals;
    
    public void playGame(int maxGoals) {
        int numberOfGoals = (int) (Math.random() * (maxGoals + 1));
        System.out.println(numberOfGoals);
        Goal[] theGoals = new Goal[numberOfGoals];
        System.out.println(theGoals.length);
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
    }
 
    
    public void playGame() {
        int numberOfGoals = (int) (Math.random() * 7);
        System.out.println(numberOfGoals);
        Goal[] theGoals = new Goal[numberOfGoals];
        System.out.println(theGoals.length);
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
    }

    public String getDescription() {
        StringBuilder returnString = new StringBuilder();
        int i = 0;
        for (Goal currGoal : this.goals) {
            returnString.append("Goal after " + this.goals[i].theTime
                    + " mins by " + this.goals[i].thePlayer.playerName
                    + " of " + this.goals[i].theTeam.teamName + "\n"
            );
            i++;
        }
        return returnString.toString();
    }
}
