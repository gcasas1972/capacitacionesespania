// question: 0  name: Switch category to $course$/top/Por defecto en Javascript
$CATEGORY: $course$/top/Por defecto en Javascript


// question: 0  name: Switch category to $course$/top/Por defecto en Javascript/3-IteratorAndGenerator
$CATEGORY: $course$/top/Por defecto en Javascript/3-IteratorAndGenerator


// question: 535  name: 1-Que  Son Iteradores y generadores
::1-Que  Son Iteradores y generadores::[html]<p>determinar cual de las siguientes afirmaciones es correcta con relaciones a los iteradores</p>{
	~<p>Un iterador es una forma de recorrer datos a traves de una colección</p>
	~<p>Iterar sobre una estructura significa acceder a cada uno de sus elementos en orden</p>
	~<p>Un objeto es un iterador cuando conoce como acceder a sus items de una colección de a una a la vez</p>
	~<p>Para crear un iterador debemos definir una función que tome una colección como parámetro y devuelva un objeto</p>
	=<p>Todas las respuestas con correctas</p>
}


// question: 536  name: 1-Que  Son Iteradores y generadores
::1-Que  Son Iteradores y generadores::[html]<p>determinar cual de las siguientes afirmaciones es correcta con relaciones a los iteradores</p>{
	=<p>Un iterador es una forma de recorrer datos a traves de una colección</p>
	~<p>Un iterador resuelve una opración sobre una coleccion por ejemplo la suma</p>
	~<p>Un iterador devuelve una colección de datos</p>
	~<p>Un iterador recibe indice como parametro y devuelve el indice siguiente</p>
	~<p>Todas las respuestas con correctas</p>
}


// question: 537  name: 1-Que  Son Iteradores y generadores
::1-Que  Son Iteradores y generadores::[html]<p>determinar cual de las siguientes afirmaciones es correcta con relaciones a los iteradores</p>{
	~<p>Un iterador resuelve una opración sobre una coleccion por ejemplo la suma</p>
	=<p>Iterar sobre una estructura significa acceder a cada uno de sus elementos en orden<br></p>
	~<p>Un iterador devuelve una colección de datos</p>
	~<p>Un iterador recibe indice como parametro y devuelve el indice siguiente</p>
	~<p>Todas las respuestas con correctas</p>
}


// question: 538  name: 1-Que  Son Iteradores y generadores
::1-Que  Son Iteradores y generadores::[html]<p>determinar cual de las siguientes afirmaciones es correcta con relaciones a los iteradores</p>{
	~<p>Un iterador resuelve una opración sobre una coleccion por ejemplo la suma</p>
	~<p>Un iterador devuelve una colección de datos<br></p>
	=<p>Un objeto es un iterador cuando conoce como acceder a sus items de una colección de a una a la vez<br></p>
	~<p>Un iterador recibe indice como parametro y devuelve el indice siguiente</p>
	~<p>Todas las respuestas con correctas</p>
}


// question: 539  name: 1-Que  Son Iteradores y generadores
::1-Que  Son Iteradores y generadores::[html]<p>determinar cual de las siguientes afirmaciones es correcta con relaciones a los iteradores</p>{
	~<p>Un iterador resuelve una opración sobre una coleccion por ejemplo la suma</p>
	~<p>Un iterador devuelve una colección de datos<br></p>
	~<p>Un iterador recibe indice como parametro y devuelve el indice siguiente<br></p>
	=<p>Para crear un iterador debemos definir una función que tome una colección como parámetro y devuelva un objeto<br></p>
	~<p>Todas las respuestas con correctas</p>
}


// question: 540  name: 2-Que es un generador
::2-Que es un generador::[html]<p>Determinar cual de las siguientes afirmaciones correcta.</p>{
	~<p>Un generador provee una forma iterativa para construir una colección de datos</p>
	~<p>Un generador puede retornar un valor a la vez mientras pausa la ejecución hasta que el próximo valor sea requerido</p>
	~<p>Un generador mantiene el seguimiento del estado interno en cada momento que es requerido</p>
	~<p>Para crear un generador debemos definicr una función con un&nbsp; asterisco delante de la función y utilizar la sentencia yield en el cuerpo.</p>
	=<p>Todas las respuestas son correctas</p>
}


// question: 544  name: 2-Que es un generador
::2-Que es un generador::[html]<p>Determinar cual de las siguientes afirmaciones correcta.</p>{
	~<p>Un generador es una forma de recorrer datos a traves de una colección</p>
	~<p>Un generador&nbsp;conoce como acceder a sus items de una colección de a una a la vez<br></p>
	~<p>un generador resuelve operaciones sobre una colección por ejemplo el promedio</p>
	=<p>Para crear un generador debemos definicr una función con un&nbsp; asterisco delante de la función y utilizar la sentencia yield en el cuerpo.<br></p>
	~<p>Todas las respuestas son correctas</p>
}


// question: 541  name: 2-Que es un generador (copia)
::2-Que es un generador (copia)::[html]<p>Determinar cual de las siguientes afirmaciones correcta.</p>{
	=<p>Un generador provee una forma iterativa para construir una colección de datos</p>
	~<p>Un generador es una forma de recorrer datos a traves de una colección</p>
	~<p>Un generador&nbsp;conoce como acceder a sus items de una colección de a una a la vez</p>
	~<p>un generador resuelve operaciones sobre una colección por ejemplo el promedio</p>
	~<p>Todas las respuestas son correctas</p>
}


// question: 542  name: 2-Que es un generador (copia)
::2-Que es un generador (copia)::[html]<p>Determinar cual de las siguientes afirmaciones correcta.</p>{
	~<p>Un generador es una forma de recorrer datos a traves de una colección</p>
	=<p>Un generador puede retornar un valor a la vez mientras pausa la ejecución hasta que el próximo valor sea requerido<br></p>
	~<p>Un generador&nbsp;conoce como acceder a sus items de una colección de a una a la vez</p>
	~<p>un generador resuelve operaciones sobre una colección por ejemplo el promedio</p>
	~<p>Todas las respuestas son correctas</p>
}


// question: 543  name: 2-Que es un generador (copia)
::2-Que es un generador (copia)::[html]<p>Determinar cual de las siguientes afirmaciones correcta.</p>{
	~<p>Un generador es una forma de recorrer datos a traves de una colección</p>
	~<p>Un generador&nbsp;conoce como acceder a sus items de una colección de a una a la vez<br></p>
	=<p>Un generador mantiene el seguimiento del estado interno en cada momento que es requerido</p>
	~<p>un generador resuelve operaciones sobre una colección por ejemplo el promedio</p>
	~<p>Todas las respuestas son correctas</p>
}


