package fr.eni.editions.jdbc.consulta;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class TestMappingTypeJavaSQL {

	public static void main(String[] args) {
		try (Connection cnx = getConnexion();
				PreparedStatement pstm = cnx.prepareStatement("insert into PERSONAS(nombre, apellido, fechaNacimiento) values(?,?,?)");
				Statement stm = cnx.createStatement()) {
			insererPersonas(pstm);
			leerPersonas(stm);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

	private static void insererPersonas(PreparedStatement pstm) throws SQLException {
		//La siguiente instrucción permite hacer coincidir
		//la timezona de la JVM con la timezona del servidor MySQL.
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
		//Agregar  una primera persona
		//Aplicar los parámetros
		pstm.setString(1, "Eastwood");
		pstm.setString(2, "Clint");
		//Escritura de la fecha de nacimiento
		//pstm.setObject(3, LocalDate.of(1930, 5, 31));
		//ou
		pstm.setDate(3, Date.valueOf(LocalDate.of(1930, 5, 31)));
		
		//Ejecutar la consulta
		int resultado = pstm.executeUpdate();
		if(resultado==1)
		{
			System.out.println("La inserción ha tenido éxito");
		}
		
		//Agregar una segunda persona
		//Aplicar los parámetros
		pstm.setString(1, "Tarantino");
		pstm.setString(2, "Quentin");
		pstm.setObject(3, null);
		
		//Ejecutar la consulta
		resultado = pstm.executeUpdate();
		if(resultado==1)
		{
			System.out.println("La inserción ha tenido éxito");
		}
		
	}

	private static void leerPersonas(Statement stm) throws SQLException {
		ResultSet rs = stm.executeQuery("select id, nombre, apellido, fechaNacimiento from PERSONAS");
		System.out.println("id\t\tnombre\t\tapellido\t\tfecha de nacimiento");
		while(rs.next())
		{
			int id = rs.getInt("id");
			System.out.print(rs.wasNull()?"desconocido":id);
			System.out.print("\t\t");
			
			String nombre = rs.getString("nombre");
			System.out.print(rs.wasNull()?"desconocido":nombre);
			System.out.print("\t\t");
			
			String apellido = rs.getString("apellido");
			System.out.print(rs.wasNull()?"desconocido":apellido);
			System.out.print("\t\t");
			
			Date sqlDate = rs.getDate("fechaNacimiento");
			System.out.println(rs.wasNull()?"desconocida":DateTimeFormatter.ofPattern("dd/MM/yyyy").format(sqlDate.toLocalDate()));
		}
		
	}
	private static Connection getConnexion() throws SQLException {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		return DriverManager.getConnection(url, user, password);
	}
}
