package fr.eni.editions.jdbc.conexión;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestConnexionTryWithResources {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		//Uso de la instrucción try with resources
		try(Connection cnx = DriverManager.getConnection(url, user, password)) {
			System.out.println("Estado de la conexión :");
			System.out.println(cnx.isClosed() ? "cerrada" : "abierta");
		}//cierre automática de la conexión 
		catch (SQLException e) {
			System.out.println("Un error se ha producido durante la conexión a la base de datos");
			e.printStackTrace();
		}
	}
}
