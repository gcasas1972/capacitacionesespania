package fr.eni.editions.jdbc.consulta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestTransaction {

	public static void main(String[] args) {
		movimiento("123456789", "987654321", 1000);
	}

	public static void movimiento(String cuentaDebito, String cuentaCredito, double suma) {
		try (Connection cnx = getConnexion()) {
			try (PreparedStatement pstm = cnx.prepareStatement("update CUENTAS set saldo=saldo + ? where numero=?");) {
				cnx.setAutoCommit(false);
				pstm.setDouble(1, suma * -1);
				pstm.setString(2, cuentaDebito);
				pstm.executeUpdate();
				impressionRapport(cuentaDebito, suma * -1);
				pstm.setDouble(1, suma);
				pstm.setString(2, cuentaCredito);
				pstm.executeUpdate();
				impressionRapport(cuentaCredito, suma);
				//Validación de las operaciones
				System.out.println("Validación de la transacción");
				cnx.commit();
			} catch (Exception e) {
				try {
					//Anulación de las operaciones
					System.out.println("Anulación de la transacción");
					cnx.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	private static void impressionRapport(String cuenta, double suma) throws Exception {
		System.out.println(cuenta + " : " + suma);
		//Descomente la siguiente línea para provocar la anulación de las operaciones
		throw new Exception("Error en la edición del informe");
	}

	private static Connection getConnexion() throws SQLException {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		return DriverManager.getConnection(url, user, password);
	}
}
