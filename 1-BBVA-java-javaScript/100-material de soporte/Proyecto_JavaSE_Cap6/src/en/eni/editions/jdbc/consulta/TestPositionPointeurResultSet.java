package fr.eni.editions.jdbc.consulta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestPosicionPunteroResultSet {

	public static void main(String[] args) {
		try (Connection cnx = getConnexion();
				Statement stm = cnx.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

			// Ejecutar la consulta y obtener el resultado
			ResultSet rs = stm.executeQuery("select * from PERSONAS");
			
			mostrarPosicionPuntero(rs);
			// Mientras hay registros
			while (rs.next()) {
				mostrarPosicionPuntero(rs);
			}
			mostrarPosicionPuntero(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void mostrarPosicionPuntero(ResultSet rs) {
		try {
			if (rs.isBeforeFirst()) {
				System.out.println("el puntero está antes del primer registro");
			}
			if (rs.isAfterLast()) {
				System.out.println("el puntero está después del último registro");
			}
			if (rs.isFirst()) {
				System.out.println("el puntero está en el primer registro");
			}
			if (rs.isLast()) {
				System.out.println("el puntero está en el último registro");
			}
			int position;
			position = rs.getRow();
			if (position != 0) {
				System.out.println("es el registro número " + position);
			}
		} catch (SQLException e) {
			// TODO Bloque catch autogenerado
			e.printStackTrace();
		}
	}

	private static Connection getConnexion() throws SQLException {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		return DriverManager.getConnection(url, user, password);
	}

}
