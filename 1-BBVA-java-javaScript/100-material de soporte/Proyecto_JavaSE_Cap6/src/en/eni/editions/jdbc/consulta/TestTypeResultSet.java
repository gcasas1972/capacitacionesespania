package fr.eni.editions.jdbc.consulta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestTypeResultSet {

	public static void main(String[] args) {
		try (Connection cnx = getConnexion();
				PreparedStatement pstm = cnx.prepareStatement("select * from PERSONAS where nombre = ?",
						ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("rellenar el nombre de la persona buscada :");
			String nombre = br.readLine();
			// Aplicar los parámetros
			pstm.setString(1, nombre);
			// Ejecutar la consulta y obtener el resultado
			ResultSet rs = pstm.executeQuery();
			mostrarTypeResultSet(rs);
			// Mientras hay registros
			while (rs.next()) {
				// Buclar por el número de columnas para mostrar la información
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					System.out.print(rs.getString(i) + "\t");
				}
				System.out.println();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void mostrarTypeResultSet(ResultSet rs) {
		try {
			switch (rs.getType()) {
			case ResultSet.TYPE_FORWARD_ONLY:
				System.out.println("el ResultSet solo se desplaza hacia adelante");
				break;
			case ResultSet.TYPE_SCROLL_INSENSITIVE:
				System.out.println("el ResultSet se puede recorrer en ambos sentidos");
				System.out.println("no es sensible a las modificaciones hechas por los otros usuarios");
				break;
			case ResultSet.TYPE_SCROLL_SENSITIVE:
				System.out.println("el ResultSet se puede recorrer en ambos sentidos");
				System.out.println("il est sensible a las modificaciones hechas por los otros usuarios");
				break;
			}
			switch (rs.getConcurrency()) {
			case ResultSet.CONCUR_READ_ONLY:
				System.out.println("los datos contenidos en el ResulSet son de solo lectura ");
				break;
			case ResultSet.CONCUR_UPDATABLE:
				System.out.println("los datos contenidos en el ResulSet son modificables");
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnexion() throws SQLException {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		return DriverManager.getConnection(url, user, password);
	}

}
