package fr.eni.editions.jdbc.conexión;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;

public class TestConnexion {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		try {
			Connection cnx = DriverManager.getConnection(url, user, password);
			System.out.println("Estado de la conexión :");
			System.out.println(cnx.isClosed() ? "cerrada" : "abierta");

			testEsValido(cnx);
			testReadingSolo(cnx);
			visualizacionWarnings(cnx);
			cambiarDeBaseDeDatos(cnx);
			cnx.setCatalog("demo_java");
			infosBase(cnx);
		} catch (SQLException e) {
			System.out.println("Un error se ha producido durante la conexión a la base de datos");
			e.printStackTrace();
		}

	}

	public static void testEsValido(Connection cnx) {
		boolean esValido;
		try {
			esValido = cnx.isValid(0);
			if (esValido) {
				System.out.println("la conexión es válida");
			} else {
				System.out.println("La conexión no es válida");
			}
		} catch (Exception e) {
			System.out.println("El método isValid no se soporta por el controlador");
			e.printStackTrace();
		}
	}

	public static void testReadingSolo(Connection cnx) {
		boolean estado;
		try {
			etat = cnx.isReadOnly();
			cnx.setReadOnly(!estado);
			if (cnx.isReadOnly() != estado) 
				System.out.println("el modo solo lectura se soporta por este controlador");
			} else {
				System.out.println("el modo solo lectura no se soporta por este controlador");
			}
			cnx.setReadOnly(estado);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void visualizacionWarnings(Connection cnx) {
		SQLWarning advertencia;
		try {
			advertencia = cnx.getWarnings();
			if (advertencia == null) {
				System.out.println("No hay advertencia");
			} else {
				while (advertencia != null) {
					System.out.println(advertencia.getMessage());
					System.out.println(advertencia.getSQLState());
					System.out.println(advertencia.getErrorCode());
					advertencia = advertencia.getNextWarning();
				}
			}
			cnx.clearWarnings();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void cambiarDeBaseDeDatos(Connection cnx) {
		try {
			System.out.println("base actual : " + cnx.getCatalog());
			System.out.println("cambio de base de datos");
			cnx.setCatalog("demo_java_2");
			System.out.println("base actual : " + cnx.getCatalog());
		} catch (SQLException e) {
			System.out.println("Problema en la manipulación de las bases de datos en el servidor");
			e.printStackTrace();
		}
	}

	public static void infosBase(Connection cnx) {
		ResultSet rs;
		DatabaseMetaData dbmd;
		try {
			dbmd = cnx.getMetaData();
			System.out.println("tipo de base : " + dbmd.getDatabaseProductName());
			System.out.println("versión: " + dbmd.getDatabaseProductVersion());
			System.out.println("nombre del controlador : " + dbmd.getDriverName());
			System.out.println("versión del controlador : " + dbmd.getDriverVersion());
			System.out.println("nombre del usuario : " + dbmd.getUserName());
			System.out.println("url de conexión : " + dbmd.getURL());
			rs = dbmd.getTables(null, null, "%", null);
			System.out.println("estructura de la base");
			System.out.println("base\tesquema\tnombre table\ttype table");
			while (rs.next()) {
				for (int i = 1; i <= 4; i++) {
					System.out.print(rs.getString(i) + "\t");
				}
				System.out.println();
			}
			rs.close();
			rs = dbmd.getProcedures(null, null, "%");
			System.out.println("los procedimientos almacenados");
			System.out.println("base\tesquema\tnombre procedimiento");
			while (rs.next()) {
				for (int i = 1; i <= 3; i++) {
					System.out.print(rs.getString(i) + "\t");
				}
				System.out.println();
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
