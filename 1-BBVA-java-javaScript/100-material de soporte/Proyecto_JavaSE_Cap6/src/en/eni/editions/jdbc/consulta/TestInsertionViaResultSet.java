package fr.eni.editions.jdbc.consulta;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;

public class TestInsertionViaResultSet {

	public static void main(String[] args) {
		try (Connection cnx = getConnexion();
				Statement stmInsertion = cnx.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				Statement stmReading = cnx.createStatement()) {
			agregarPersona(stmInsertion);
			leerPersonas(stmReading);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

	private static void agregarPersona(Statement stmInsertion) throws SQLException {
		ResultSet rs = stmInsertion.executeQuery("select id, nombre, apellido, fechaNacimiento from PERSONAS");
		rs.moveToInsertRow();
		rs.updateString("nombre","BESSON");
		rs.updateString("apellido","Luc");
		rs.insertRow();
	}

	private static void leerPersonas(Statement stm) throws SQLException {
		ResultSet rs = stm.executeQuery("select id, nombre, apellido, fechaNacimiento from PERSONAS");
		System.out.println("id\t\tnombre\t\tapellido\t\tfecha de nacimiento");
		while(rs.next())
		{
			int id = rs.getInt("id");
			System.out.print(rs.wasNull()?"desconocido":id);
			System.out.print("\t\t");
			
			String nombre = rs.getString("nombre");
			System.out.print(rs.wasNull()?"desconocido":nombre);
			System.out.print("\t\t");
			
			String apellido = rs.getString("apellido");
			System.out.print(rs.wasNull()?"desconocido":apellido);
			System.out.print("\t\t");
			
			Date sqlDate = rs.getDate("fechaNacimiento");
			System.out.println(rs.wasNull()?"desconocida":DateTimeFormatter.ofPattern("dd/MM/yyyy").format(sqlDate.toLocalDate()));
		}
		
	}
	private static Connection getConnexion() throws SQLException {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		return DriverManager.getConnection(url, user, password);
	}
}
