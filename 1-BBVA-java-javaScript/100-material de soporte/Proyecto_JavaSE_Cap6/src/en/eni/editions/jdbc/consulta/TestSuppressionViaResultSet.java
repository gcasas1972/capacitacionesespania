package fr.eni.editions.jdbc.consulta;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;

public class TestDeleteViaResultSet {

	public static void main(String[] args) {
		try (Connection cnx = getConnexion();
				Statement stmDelete = cnx.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				Statement stmReading = cnx.createStatement()) {
			leerYEliminarPersonas(stmDelete);
			leerPersonas(stmReading);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

	private static void leerYEliminarPersonas(Statement stmDelete) throws SQLException {
		ResultSet rs = stmDelete.executeQuery("select id, nombre, apellido, fechaNacimiento from PERSONAS");
		while(rs.next())
		{
			//Delete el registro si la personas se llama Eastwood
			System.out.println("Persona actual : " + rs.getString("nombre"));
			if("Eastwood".equalsIgnoreCase(rs.getString("nombre")))
			{
				System.out.println("Elimino la persona");
				rs.deleteRow();
			}
		}
	}

	private static void leerPersonas(Statement stm) throws SQLException {
		ResultSet rs = stm.executeQuery("select id, nombre, apellido, fechaNacimiento from PERSONAS");
		System.out.println("id\t\tnombre\t\tapellido\t\tfecha de nacimiento");
		while(rs.next())
		{
			int id = rs.getInt("id");
			System.out.print(rs.wasNull()?"desconocido":id);
			System.out.print("\t\t");
			
			String nombre = rs.getString("nombre");
			System.out.print(rs.wasNull()?"desconocido":nombre);
			System.out.print("\t\t");
			
			String apellido = rs.getString("apellido");
			System.out.print(rs.wasNull()?"desconocido":apellido);
			System.out.print("\t\t");
			
			Date sqlDate = rs.getDate("fechaNacimiento");
			System.out.println(rs.wasNull()?"desconocida":DateTimeFormatter.ofPattern("dd/MM/yyyy").format(sqlDate.toLocalDate()));
		}
		
	}
	private static Connection getConnexion() throws SQLException {
		String url = "jdbc:mysql://localhost/demo_java?serverTimezone=UTC";
		String user = "mysqluser";
		String password = "Pa$$w0rd";
		return DriverManager.getConnection(url, user, password);
	}
}
