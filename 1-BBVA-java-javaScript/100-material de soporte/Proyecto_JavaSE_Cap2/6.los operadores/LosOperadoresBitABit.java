
public class LosOperadoresBitABit {

	public static void main(String[] args) {
		System.out.println("45 &(et) 22 = " + (45 & 22));
		System.out.println("99 |(o) 46 = " + (99 | 46));
		System.out.println("99 ^(o exclusivo) 46 = " + (99 ^ 46));
		System.out.println("26 >>(desplazamiento hacia la derecha) 1 = " + (26 >> 1));
		System.out.println("-26 >>(desplazamiento hacia la derecha) 1 = " + (-26 >> 1));
		System.out.println("26 <<(desplazamiento hacia la izquierda) 1 = " + (26 << 1));
		System.out.println("26 >>>(desplazamiento hacia la izquierda) 1 = " + (26 >>> 1));
		System.out.println("-26 >>>(desplazamiento hacia la derecha) 1 = " + (-26 >>> 1));
	}

}
