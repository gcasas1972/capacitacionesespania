
public class LosOperadoresUnarios {
	public static void main(String[] args) {
			int i;
			
			//Operador postfijado (evalúa el uso del valor en la expresión)		
			i=3; 
			System.out.println("i = " + (i++));
			
			//Operador postfijado (evalúa antes el uso del valor en la expresión)
			i=3;
			System.out.println("i = " + (++i));
	}
}
