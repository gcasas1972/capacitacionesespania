
public class LosOperadoresLogicos {
	static boolean test1;
	static boolean test2;
	public static void main(String[] args) {
		test1=true;
		test2=true;
		probar();
		
		test1=false;
		test2=false;
		probar();
		
		test1=true;
		test2=false;
		probar();

		test1=false;
		test2=true;
		probar();

	}

	private static void probar() {
		System.out.println("test1 = " + test1 + " y test2 = " + test2 );
		System.out.println();
		System.out.println(" =>test1 & test2");
		System.out.println(" resultado = " + (isTest1() & isTest2()));
		System.out.println();
		System.out.println(" =>test1 | test2");
		System.out.println(" resultado = " + (isTest1() | isTest2()));
		System.out.println();
		System.out.println(" =>test1 ^ test2");
		System.out.println(" resultado = " + (isTest1() ^ isTest2()));
		System.out.println();
		System.out.println(" =>!test1");
		System.out.println(" resultado = " + (!isTest1()));
		System.out.println();
		System.out.println(" =>test1 && test2");
		System.out.println(" resultado " + (isTest1() && isTest2()));
		System.out.println();
		System.out.println(" =>test1 || test2");
		System.out.println(" résutlat = " + (isTest1() || isTest2()));
		
		System.out.println();
	}
	
	public static boolean isTest1() {
		System.out.println(" --evaluación test1 ("+test1+")--");
		return test1;
	}
	public static boolean isTest2() {
		System.out.println(" --evaluación test1 ("+test2+")--");
		return test2;
	}

}
