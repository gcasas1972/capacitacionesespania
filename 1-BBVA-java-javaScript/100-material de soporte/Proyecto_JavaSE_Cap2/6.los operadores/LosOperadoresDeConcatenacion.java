
public class ElOpedadorDeConcatenacion {

	public static void main(String[] args) {
		long duracion; 
		String liebre; 
		String tortuga=""; 
		long inicio, fin; 
		inicio = System.currentTimeMillis(); 
		for (int i = 0; i <= 10000; i++) 
		{ 
		     tortuga = tortuga + " " + i; 
		} 
		fin = System.currentTimeMillis(); 
		duracion = fin-inicio; 
		System.out.println("duración para la tortuga : " + duracion + "ms"); 
		inicio = System.currentTimeMillis(); 
		StringBuilder sb = new StringBuilder(); 
		for (int i = 0; i <= 10000; i++) 
		{ 
		     sb.append(" "); 
		     sb.append(i); 
		} 
		liebre = sb.toString(); 
		fin = System.currentTimeMillis(); 
		duracion = fin-inicio; 
		System.out.println("duración para la liebre : " + duracion + "ms"); 
		if (liebre.equals(tortuga)) 
		{ 
		     System.out.println("las dos cadenas son idénticas"); 
		}
	}
}
