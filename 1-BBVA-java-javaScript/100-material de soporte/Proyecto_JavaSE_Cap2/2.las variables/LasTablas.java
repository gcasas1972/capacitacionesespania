import java.util.Arrays;

public class LasTablas {

	public static void main(String[] args) {
		/*
		//Déclaración 
		int[] cifraNegocio;
		//Initialización
		cifraNegocio  = new int[12];
		cifraNegocio[0]=123;
		cifraNegocio[1]=563;
		//...
		*/
		
		//Declaración e inicialización con los valores conocidos
		int[] cifraNegocio = {123,563,657,453,986,678,564,234,786,123,534,975};
		
		//Reading
		int cifraNegocioMesEnero = cifraNegocio[0];
		//Escritura
		cifraNegocio[0]=354;
		
		//Declaración de un tabla de 2 dimensiones con el tamaño definido para todas las dimensiones.
		int[][] cifraNegocioPorRegion;
		cifraNegocioPorRegion = new int[2][12];
		//...
		
		
		//Declaración de un tabla de 2 dimensiones creando solo la primera dimension en la initialización
		int[][] matriz;
		matriz = new int[2][];
		//Initialización de la segunda dimensión para cada casos de la primera dimensión.
		//Est posible inicializar con las tablas de tamaños diferentes.
		matriz[0]=new int[3];
		matriz[1]=new int[2];
		
		//Reading
		int case_0_0 = matriz[0][0];
		//Escritura
		matriz[1][1]=14;
		
		/*
		//Posilidad de initializar con valores conocidos:
		int[][] matriz = {
							{11,12,13},
							{21,22,23}
						  };
		*/
		
		//Manipulación de un tabla
		matriz = new int[8][3];
		
		//Conocer el tamaño de un tabla
		System.out.println("la tabla tiene " + matriz.length + " casillas en " + matriz[0].length + " casillas");
		
		//Ver el contenido de un tabla a 1 dimensión:
		System.out.println("Contenido de la tabla cifraNegocio:");
		System.out.println(Arrays.toString(cifraNegocio));
		
		//Ver el contenido de una tabla a N dimensiones:
		int[][] malla = {{11,12,13},{21,22,23},{31,32,33}};
		System.out.println("Contenido de la tabla malla:");
		System.out.println(Arrays.deepToString(malla));
		
		//Ordensa el contenido de una tabla:
		System.out.println("Contenido ordenado de la tabla cifraNegocio:");
		Arrays.sort(cifraNegocio);
		System.out.println(Arrays.toString(cifraNegocio));
		
		//Búsqueda de un elemento en el tabla (ordenada)
		System.out.println("Búsqueda del primer índice de la tabla con una cifra de negocio de 123. Devuelve un valor negativo si no existe");
		System.out.println(Arrays.binarySearch(cifraNegocio, 123));
		
		//Copiar una tabla
		System.out.println("Copiar una tabla");
		int[] copiaCifraNegocio = Arrays.copyOf(cifraNegocio, 24);
		System.out.println(Arrays.toString(copieCifraNegocio));
		
		//Copiar una parte de la tabla:
		System.out.println("Copiar una parte de una tabla");
		int[] primerTrimestre = Arrays.copyOfRange(cifraNegocio, 0, 3);
		System.out.println(Arrays.toString(primerTrimestre));
		
		//Rellenar una tabla con un valor concreto
		System.out.println("Rem<llenar una tabla con las 2");
		int[] tabla = new int[5];
		Arrays.fill(tabla, 2);
		System.out.println(Arrays.toString(tabla));
		
	}

}