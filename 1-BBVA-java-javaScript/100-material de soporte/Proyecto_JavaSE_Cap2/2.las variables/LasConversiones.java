
public class LasConversiones {
	public static void main(String[] args) {
		{
			//Conversiones entre numéricos
			float superficie; 
			superficie=2356.8f; 
			//Conversión implícita
			double grandeSuperficie=superficie; 
			//Conversión explícita
			int aproximacion=(int)superficie;
			//Visualización de la información
			System.out.println("Superficie en float : " + superficie);
			System.out.println("Superficie en double : " + granSuperficie);
			System.out.println("Superficie en int : " + aproximacion);
		}
		{
			//Conversión implícita numérico a String
			double precioHt; 
			precioHt=152; 
			String recap; 
			recap="la cantidad del comando es : " + precioHt*1.20;
			System.out.println(recap);
		}
		{
			//Conversión explícita numérico a String
			double precioHt; 
			precioHt=152; 
			String recap; 
			recap="la cantidad del comando es : " + String.valueOf(precioHt*1.20);
			System.out.println(recap);
		}
		{
			//Conversión cadena de caracteres a wrappers
			Integer entero = Integer.valueOf("10");
			entero = Integer.valueOf(10);
			//autoboxing
			entero = 10;
			//undoxing
			int x = entero;
			System.out.println("x es un int resultado del Integer entero por unboxing");
			System.out.println("entero : " + entero);
			System.out.println("x : " + x);
			//Conversión cadena de caracteres a numérico básico
			int numero = Integer.parseInt("10");
			System.out.println("la variable número es resultado de la conversión del texto \"10\" en tipo primitivo (int)");
			System.out.println("número : " + numero);
		}
	}
}
