public class LosCaracteres {
	public static void main(String[] args) {
		char caracterC = 'c';
		char euro1 = '€';
		char euro2 = '\u20AC';
		
		System.out.println(caracterC);
		System.out.println(euro1);
		System.out.println(euro2);
	}
}
