import java.util.ArrayList;

public class LaInferenciaDeTipo {

	public static void main(String[] args) {
		var cadena = "Hola soy una cadena de caracteres";
		var numero = 10;
		var miLista = new ArrayList<String>();
		var num = getNombre();
		//var unaVariable; //no compila, se debe inicializar 
		//var unaVariable=null; //no compila, se debe inicializar con un valor tipado
	}
	
	public static int getNombre()
	{
		return 2;
	}
}
