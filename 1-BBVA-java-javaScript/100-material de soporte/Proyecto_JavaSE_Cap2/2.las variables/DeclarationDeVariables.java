import java.util.Date;

public class DeclaracionDeVariables {

	public static void main(String[] args) {
		//DeclaraciÓn
		int edad;
		Date hoy, ayer;
		//Initialización
		edad = 41;
		//Declaración seguida de una inicializacion para un tipo de valor
		int edadPrimerGemela = 25;
		int edadSegundaGemela = edadPrimerGemela;
		//Declaración seguida de una inicialización para un tipo de referencia
		//Puede comprobar que el constructor está tachado porque el método está depreciado
		//esto significa que no hay que usarlo más.
		//El javadoc indica que es preferible utilizar la clase Calendar o la clase GregorianCalendar para inicializar una fecha.
		//En el marco del ejemplo, el constructor depreciado se usa por simplicidad.
		Date d1 = new Date(2019, 3, 6);
		Date d2 = d1;
	}

}
