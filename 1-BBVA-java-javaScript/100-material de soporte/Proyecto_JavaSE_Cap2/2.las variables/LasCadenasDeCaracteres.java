
public class LasCadenasDeCaracteres {

	public static void main(String[] args) {
		//Declaración de una cadena de caracteres
		String nomDelCapitan = "Crochet";
		String cadena1 = "eni";
		String cadena2 = new String("eni");
		
		//Declaración de una cadena de caracteres con una comilla
		String cadena = "Ha dicho: \"es suficiente\"";
		System.out.println(cadena);
		
		//Manipulación de las cadenas de caracteres:
		cadena1 = "el invierno será lluvioso";
		cadena2 = "el invierno será frio";
		System.out.println("cadena 1 = "+cadena1);
		System.out.println("cadena 2 = "+cadena2);
		
		//Extraer un carácter particular:
		System.out.println("el tercer carácter de la cadena 1 es " + cadena1.charAt(2));
		
		//Obtener la longitud de una cadena
		System.out.println("la cadena 1 contiene " + cadena1.length() + " caracteres");
		
		//Recortar una cadena
		System.out.println("un trozo de la cadena 1 : " + cadena1.substring(2,7));
		
		//Probar la igualdad del contenido de 2 cadenas
		if(cadena1.equals(cadena2))
		{
			System.out.println("las dos cadenas son idénticas");
		}
		else
		{
			System.out.println("las dos cadenas son diferentes");
		}
		
		//Comparar 2 cadenas
		if(cadena1.compareTo(cadena2)>0)
		{
			System.out.println("cadena1 est superior a cadena2");
		}
		else if(cadena1.compareTo(cadena2)<0)
		{
			System.out.println("cadena1 es inferior a cadena2");
		}
		else
		{
			System.out.println("las dos cadenas son idénticas");
		}
		
		String nombre = "Code.java";
		if(nombre.endsWith(".java"))
		{
			System.out.println("es un fichero fuente java");
		}
		
		//Eliminar los espacios
		String eni="          eni         ";
		System.out.println("longitud de la cadena : " + eni.length());
		System.out.println("longitud de la cadena limpia :" + eni.trim().length());
		
		//Cambiar el tamaño de letra
		System.out.println(cadena1.toUpperCase());
		System.out.println(cadena1.toLowerCase());
		
		//Buscar en una cadena de caracteres
		String busqueda;
		int posicion;
		busqueda="e";
		posicion = cadena1.indexOf(busqueda,0);
		while(posicion>=0)
		{
			System.out.println("cadena encontrada en la posición " + posicion);
			posicion = cadena1.indexOf(busqueda, posicion+1);
		}
		System.out.println("fin de la búsqueda");

		//Sustituir una parte de una cadena
		String cadena3 = cadena1.replace("invierno", "verano");
		System.out.println(cadena3);
		
		//Formatear una cadena
		boolean b = true;
		String s = "cadena";
		int i=56;
		double d=5.5;
		System.out.println(
							String.format("boolean : %b %n" +
										  "cadena de caracteres : %s %n" +
										  "entero : %d %n" +
										  "entero en hexadecimal : %x %n" +
										  "entero en bytes: %o %n" +
										  "décimal : %f %n" +
										  "decimal exacto a la décima: %.1f %n" +
										  "decimal en formato científico : %e %n", 
										  b,s,i,i,i,d,d,d)
						  );
			
	}

}
