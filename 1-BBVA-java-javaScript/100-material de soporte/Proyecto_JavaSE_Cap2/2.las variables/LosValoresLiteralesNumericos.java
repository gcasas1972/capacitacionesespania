
public class LosValoresLiteralesNumericos {

	public static void main(String[] args) {
		//Los valores literales numéricos enteros se consideran como de tipo int 
		int poblacionFrancesa = 66990000;
		//Los valores literales numéricos decimales se consideran como de tipo double
		double porcentageMujeres = 51.649;
		
		//La línea siguiente no compila.
		//El compilador considera 153 como un entero de tipo int.
		//byte b = 153;
		
		//La línea siguiente compila: 
		//El compilador considera 22 como un entero de tipo byte en este caso.
		//El compilador es capaz de evaluar el valor intrínseco de un literal entero. 
		byte b = 22;
		
		//La línea siguiente no compila.
		//El compilador considera 10.1 como un décimal de tipo double.
		//10.1 forma parte de la franja de un float.
		//El compilador no es capaz de evaluar el valor intrínseco de un literal decimal.
		//float f = 10.1;
		
		//La línea siguiente compila:
		//El sufijo F (o f) tiene el valor literal impuesto al compilador del intérprete como un tipo float.
		float f = 10.1F;
		
		//La línea siguiente no compila:
		//El compilador considera que el valor literal no es un entero de tipo int válido. En efecto, sobrepasa el valor máximo
		//10123456789 form parte de la franja de un long.
		//long l = 10123456789;
		
		//La línea siguiente compile:
		//El sufijo L (o l) tiene el valor literal impuesto al compilador del intérprete como un tipo long.
		long l = 10123456789L;
	}

}
