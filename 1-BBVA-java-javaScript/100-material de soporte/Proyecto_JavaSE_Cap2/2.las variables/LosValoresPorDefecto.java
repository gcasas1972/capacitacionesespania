
public class LosValoresPorDefecto {

	//Las variables de instancia y de clase tienen un valor por defecto
	int miVariableDInstancia;
	static int maVariableDeClasse;
	public static void main(String[] args) {
		//Visualización del valor por defecto de la variable de instancia
		//Para esto, es necesario crear una instancia de la clase LosValoresPorDefecto
		//El método main es estátici, no existe por el momento ninguna instancia para la que no sea posible acceder a esta variable
		//Para más información, consulte el capítulo siguiente en la programación orientada a objetos.
		LosValoresPorDefecto lvpd = new LosValoresPorDefecto();
		System.out.println("El valor por defecto de miVariableDInstancia es : " + lvpd.miVariableDInstance);
				
		// Visualización del valor por defecto de la variable de clase
		System.out.println("El valor por defecto de miVariableDInstancia es : " + maVariableDeClase);
		
		// Las variables locales no tienen valor por defectoi
		int miVariableLocal;
		//La línea siguiente no compila porque miVariableLocal no está inicializada.
		//System.out.println("El valor por defecto de miVariableLocal es :" + miVariableLocal);
	}
}
