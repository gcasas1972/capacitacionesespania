import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class LesDatesEtLesHeures {
	public static void main(String[] args) throws ParseException 
	{
		{
			/***************************************
			 * Gestión de las fechas antiguas: 
			 ***************************************/
			System.out.println("Gestión de las fechas antiguas");
			System.out.println();
			
			//Creación de una fechas inicializada a ahora
			Date ahora = new Date();
			System.out.println("Hoy es " + ahora.toString());
			
			//Formateo 
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			System.out.println("Hoy es " + sdf.format(ahora));
			
			//Conversión de cadena de caracteres a Date:
			String cadenaDate = "25/12/2019";
			Date date25_12_2019 = sdf.parse(cadenaDate);
			System.out.println(date25_12_2019);
			
			//Uso de la clase Calendar
			Calendar calendarioAyer = Calendar.getInstance();//initializa a hoy
			calendarioAyer.add(Calendar.DAY_OF_MONTH, -1);//quitga un día
			Date ayer = calendarioAyer.getTime();//permite obtener un objeto de tipo Date a partir de un objeto de tipo Calendar
			System.out.println("Ayer era el " + sdf.format(ayer));
			
			//Creación de una fecha concreta:
			Date noel2019 = new GregorianCalendar(2019, 11, 25).getTime();//Mes en base 0
			System.out.println(sdf.format(noel2019));
			
			System.out.println();
		}
		{
			/***************************************
			 * Gestión de las fechas nuevo método: 
			 ***************************************/
			System.out.println("Gestión de las fechas nuevas");
			System.out.println();
		
			//Creación de una fecha initalizada a ahora / hoy
			LocalDateTime ahora = LocalDateTime.now();
			LocalDate hoy = LocalDate.now();
			System.out.println("Hoy es" + hoy.toString());
			
			//Formatage
			DateTimeFormatter dtf =DateTimeFormatter.ofPattern("dd/MM/yyyy");
			System.out.println("Hoy es" + dtf.format(hoy));
					
			//Conversión cadena de caracteres a LocalTime
			LocalTime reloj = LocalTime.parse("22:45:03");
			System.out.println(reloj);
			LocalDate date25_12_2019 = LocalDate.parse("25/12/2019", dtf);
			System.out.println(date25_12_2019);
			
			//Cambiar la hora
			LocalTime nuevaReloj = reloj.withHour(9);
			System.out.println(nuevaReloj);
			
			//Agregar/retirar tiempo
			LocalDate paques = LocalDate.of(2019, 4, 22);
			LocalDate ascension = paques.plusDays(39);
			System.out.println("En 2019, la ascension es el " + dtf.format(ascension));
			
			//Programa que permita mostrar el número de días festivos un sábado o un domingio entre 2019 y 2030
			MonthDay[] fiestas; 
			fiestas=new MonthDay[8]; 
			fiestas[0]=MonthDay.of(1,1); 
			fiestas[1]=MonthDay.of(5,1); 
			fiestas[2]=MonthDay.of(5,8); 
			fiestas[3]=MonthDay.of(7,14); 
			fiestas[4]=MonthDay.of(8,15); 
			fiestas[5]=MonthDay.of(11,1); 
			fiestas[6]=MonthDay.of(11,11); 
			fiestas[7]=MonthDay.of(12,25); 
			         
			int nbDias; 
			int anio; 
			LocalDate diaTest; 
			for (anio=2019;anio<2030;anio++) 
			{ 
			     nbDias=0; 
			     for(MonthDay test:fiestas) 
			     { 
			          diaTest=test.atYear(anio); 
			          if (diaTest.getDayOfWeek()==DayOfWeek.SATURDAY
			              ||diaTest.getDayOfWeek()==DayOfWeek.SUNDAY) 
			          { 
			               nbDias++; 
			          } 
			     } 
			     System.out.println("en " + anio + 
			                        " hay " + nbDias + 
			                        " dia(s) festivo(s)" +
			                        " un sábado o un domingo"); 
			}  

			
		}
	}
}
