
public class LasConstantes {

	static final double IMPUESTOIVA = 1.20;
	static final int TOTAL = 100;
	static final int MITAD = TOTAL/2;
	
	public static void main(String[] args) {
		double precioNeto = 10;
		double precioIVA = precioNeto * IMPUESTOIVA;
		System.out.println("El precio con IVA es de : " + precioIVA);

		System.out.println("La constante calculada MITAD vale " + MITAD);
	}

}
