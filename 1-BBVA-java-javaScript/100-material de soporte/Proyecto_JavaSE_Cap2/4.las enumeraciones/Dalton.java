
public enum Dalton {
	   JOE (1.40, 52), 
	   WILLIAM (1.68, 72), 
	   JACK (1.93, 83), 
	   AVERELL (2.13, 89); 
	  
	   private final double tamanio;   
	   private final double peso; 
	 
	   private Dalton(double tamanio, double peso)  
	   { 
	      this.tamanio = tamanio; 
	      this.peso = peso; 
	   } 
	   public double tamanio() { return tamanio; } 
	   public double peso() { return peso; } 
	 
	   double imc()  
	   { 
	            
	      return peso/(tamanio+tamanio); 
	   } 
}
