
public class ManipulacionEnumeradas {

	public static void main(String[] args) {
		//Declaración de una variable de tipoc Dalton inicializada con un valor de la enumerada.
		Dalton d = Dalton.JACK;
		System.out.println(d.toString());

		//Asignación de la variable con el valor de enumerada correspondiente al texto "JOE" 
		d = Dalton.valueOf("JOE"); 
		System.out.println("peso : "+ d.peso()); 
		System.out.println("tamanio : "+ d.tamanio());

		//Recorrer todos los valores de la enumerada
		System.out.println("los hermanos Dalton"); 
		for(Dalton dalton: Dalton.values()) 
		{ 
		   System.out.println(dalton.toString()); 
		}
		
		//Probar el valor de una variable de un tipo enumerado
		Dia repera = Dia.LUNES;
		switch (repere) 
		{ 
		    case LUNES: 
		    case MARTES: 
		    case MIRCOLES: 
		    case JUEVES: 
		         System.out.println("Es duro trabajar"); 
		         break; 
		    case VIERNES: 
		         System.out.println("Buen fin de semana"); 
		         break; 
		    case SABADO: 
		         System.out.println("Para terminar"); 
		          break; 
		     case DOMINGO: 
		          System.out.println("Y empezamos"); 
		          break; 
		}

	}

}
