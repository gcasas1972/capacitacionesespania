import java.time.Duration;
import java.time.OffsetTime;
import java.util.Random;
import java.util.Scanner; 

public class Ejercicio4  
{ 
    public static void main(String[] args) 
    { 
        int numIntentos=0; 
        int secret; 
        int numIntroducido;
        Random rd = new Random();
        
        secret=rd.nextInt(1000); 
        Scanner sc=new Scanner(System.in); 
        //Registro del tiempo al inicio del programa
        OffsetTime inicio = OffsetTime.now(); 
        System.out.println("Rellene un entero entre 0 y 1000");
        do 
        { 
        	//recuperación del valor introducido por el usuario
            numIntroducido=sc.nextInt(); 
            //incremento del número de intentos
            numIntentos++; 
            //comparación con el número secret.
            //visualización de un mensaje si el número es diferente
            if(numIntroducido<secret) 
            { 
                 System.out.println("es más"); 
            } 
            else if(numIntroducido>secret) 
            { 
                 System.out.println("es menos"); 
            }   
          //Se continua si el numIntroducido no es igual al número secreto
        } while (numIntroducido!=secret); 
        //Registro del tiempo al final del programa
        OffsetTime fin = OffsetTime.now();
        //Cálculo de la duración entre el inicio y el fin
        Duracion duracion = Duration.between(inicio, fin);
        System.out.println("Bravo lo ha encontrado en " + numIntentos + " intento(s)");
        //Visualización del tiempo en segundas
        System.out.println("Ha usado " + duracion.getSeconds() + " segundos");
    } 
}