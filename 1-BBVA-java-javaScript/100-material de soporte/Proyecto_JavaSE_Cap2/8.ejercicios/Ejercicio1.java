import java.util.Arrays;

public  class Ejercicio1  
{	 
     static String[] direcciones; 
     static String[] listaBrutaProveedores; 
     static String[] listaProveedoresDistintos; 
     static int[] numeroClientes;
     
     public static void main(String[] args) 
     { 
          //INICIALIZACIÓN DE LA TABLA
         direcciones=new String[10]; 
         direcciones[0]="jpp@sfr.fr"; 
         direcciones[1]="tom@gmail.com"; 
         direcciones[2]="fred@sfr.fr"; 
         direcciones[3]="victor@sfr.fr"; 
         direcciones[4]="chris@sfr.fr"; 
         direcciones[5]="robert@orange.fr"; 
         direcciones[6]="paul@sfr.fr"; 
         direcciones[7]="lise@gmail.com"; 
         direcciones[8]="thierry@eni.fr"; 
         direcciones[9]="marie@eni.fr"; 
         
         //RECUPERACIÓN DE LOS PROVEEDORES DE CADA DIRECCIÓN
         listaBrutaProveedores =new String[10]; 
         for (int i=0;i<direcciones.length;i++) 
         { 
        	 //Para cada dirección, recuperación de la cadena después de la @:
             listaBrutaProveedores[i]=direcciones[i].substring(direcciones[i].indexOf('@')+1); 
         } 
         System.out.println("Lista bruta de los proveedores:");
         System.out.println(Arrays.toString(listaBrutaProveedores));
         
         //RECUPERACIÓN DE LA LISTA DE LOS PROVEEDORES DISTINTOS Y NÚMERO DE CLIENTES
         //1-Ordenar la tabla
         Arrays.sort(listaBrutaProveedores);
         System.out.println("Lista ordenada de los proveedores:");
         System.out.println(Arrays.toString(listaBrutaProveedores));
         //2-Inicializar la tabla de los proveedores distintos con un tamaño de 1 (porque hay al menos 1 proveedor)
         //e inicializar la tabla del número de clientes por proveedor
         listaProveedoresDistintos = new String[1];
         numeroClientes = new int[1];
         //3-Agregar el primer proveedor y asociarle 1 cliente:
         listaProveedoresDistintos[0]=listaBrutaProveedores[0];
         numeroClientes[0]=1;
         //4-Recorrer la tabla ordenada de los proveedores para drtectar eventuales proveedores
         //El recorrido comienza en el índice 1 porque el de índice 0 se ha agregado a la lista de los proveedores distinctos
         for(int i=1;i<listaBrutaProveedores.length;i++)
         {
        	 //5-Si el proveedor actual es diferente del ultimo proveedor distincto agregado
        	 //Es es un nuevo proveedor.
        	 if(!listaBrutaProveedores[i].equals(listaProveedoresDistintos[listaProveedoresDistintos.length-1]))
        	 {
        		//Agrandar la tabla de los proveedores distintos de una casilla
        		//y agrandar la tabla del número de clientes de una casilla
        		listaProveedoresDistintos = Arrays.copyOf(listaProveedoresDistintos, listaProveedoresDistintos.length+1);
        		numeroClientes = Arrays.copyOf(numeroClientes, numeroClientes.length+1);
        		//Agregar este nuevo proveedor a la tabla y asociarle 1 cliente
        		listaProveedoresDistintos[listaProveedoresDistintos.length-1]=listaBrutaProveedores[i];
        		numeroClientes[numeroClientes.length-1]=1;
        	 }
        	 else
        	 {
        		 //Es un proveedor ya referenciado, es suficiente con agregar un cliente
        		 numeroClientes[numeroClientes.length-1]+=1;
        	 }
         }
         System.out.println("Lista de los proveedores distintos:");
         System.out.println(Arrays.toString(listaProveedoresDistintos));
         System.out.println("Número de clientes para cada proveedor");
         System.out.println(Arrays.toString(numeroClientes));
         
         //VER LA PARTE DE MERCADO DE CADA PROVEEDOR:
         System.out.println("Partes del mercado:");
         for(int i=0;i<listaProveedoresDistintos.length;i++)
         {
        	 System.out.println(String.format("=> %s \t: %d clientes en %d (%.1f%%)"
        			 											, listaProveedoresDistintos[i]
        			 											, numeroClientes[i]
        			 											, direcciones.length,
        			 											(numeroClientes[i]*100.0/direcciones.length)));
         }
         
     } 
 }
