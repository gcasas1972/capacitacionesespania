import java.util.Random;

public class Ejercicio2  
{ 
    public static void main(String[] args) 
    { 
    	int numeroTentativa=0; 
        int nb1,nb2,nb3;
        Random rd = new Random();
        //El bucle do...while se adapta porque hay al menos una tentativa
        do 
        { 
        	//Tiro al azar de las 3 variables
        	nb1=rd.nextInt(1000); 
            nb2=rd.nextInt(1000); 
            nb3=rd.nextInt(1000); 
            //Incremento del número de tentativa
            numeroTentativa++; 
            //Visualización de la tentativa
            System.out.println("número 1:" + nb1 + " número 2:" + nb2 + " número 3:" + nb3);
            //Se comienza de nuevi si nb1 es impar o si nb2 es impar o si nb3 es par.
            //para esto, se evalúa el resto de la division entera por 2.
            //si el resto es igual a 1, el número est impar
            //si el rest o es igual a 0, el número es par
         } while(nb1 % 2==1 || nb2 % 2==1 || nb3 % 2==0);
         System.out.println("Resultado obtenido en " + numeroTentativa + " intento(s)"); 	
    } 
}
