import java.util.Random;
import java.util.Scanner; 

public class Ejercicio3  
{ 
    public static void main(String[] args) 
    { 
        int numIntentos=0; 
        int secret; 
        int numIntroducido;
        Random rd = new Random();
        
        secret=rd.nextInt(1000); 
        Scanner sc=new Scanner(System.in); 
        System.out.println("Rellene un entero entre 0 y 1000");
        do 
        { 
        	//recuperación del valor introducido por el usuario
            numIntroducido=sc.nextInt(); 
            //incremento del número de intentos
            numIntentos++; 
            //comparación con el número secret.
            //visualizacion de un mensaje si el número es diferente
            if(numIntroducido<secret) 
            { 
                 System.out.println("es más"); 
            } 
            else if(numIntroducido>secret) 
            { 
                 System.out.println("es menos"); 
            }   
          //Se continua si el numIntroducido no es igual al número secreto!
        } while (numIntroducido!=secret); 
        System.out.println("Bravo, lo ha encontrado en " + numIntentos + " intento(s)"); 
    } 
}