
public class LeIf {
	public static void main(String[] args) {
		boolean test=true;
		//Estructura la más simple:
		System.out.println("if(test)");
		System.out.println("resultado: " + test);
		if(test)
			System.out.println("   instrucción condicional por el resultado del test");
		System.out.println("   instrucción obligatoria ejecutada");
		
		System.out.println();
		
		//Estructura que permita condicionar la ejecución de instrucciones
		System.out.println("if(test){...}");
		System.out.println("resultado: " + test);
		if(test)
		{
			System.out.println("   instruccion 1 (if)");
			System.out.println("   ... (if)");
			System.out.println("   instruccion n (if)");
		}
		
		System.out.println();
		
		//Estructura que permita agregar una alternative con un else
		test=false;
		System.out.println("if(test){...}else{...}");
		System.out.println("resultado: " + test);
		if(test)
		{
			System.out.println("   instruccion 1 (if)");
			System.out.println("   ... (if)");
			System.out.println("   instruccion n (if)");
		}
		else
		{
			System.out.println("   instruccion 1 (else)");
			System.out.println("   ... (else)");
			System.out.println("   instruccion n (else)");
		}
		
		System.out.println();
		
		//Estructura que permita agregar una condición con else if
		test=false;
		boolean test2=true;
		System.out.println("if(test){...}else if{test2}else{...}");
		System.out.println("resultado if: " + test);
		System.out.println("resultado else if: " + test2);
		if(test)
		{
			System.out.println("   instruccion 1 (if)");
			System.out.println("   ... (if)");
			System.out.println("   instruccion n (if)");
		}
		else if(test2)
		{
			System.out.println("   instruccion 1 (else if)");
			System.out.println("   ... (else if)");
			System.out.println("   instruccion n (else if)");
		}
		else
		{
			System.out.println("   instruccion 1 (else)");
			System.out.println("   ... (else)");
			System.out.println("   instruccion n (else)");
		}
		
		System.out.println();
		System.out.println("Ejemplo concreto:");
		int age = 25;
		System.out.println("Su edad es : " + age);
		if(age>=18)
		{
			System.out.println("Es un adulto");
		}
		else
		{
			System.out.println("Es un niño");
		}
	}
}
