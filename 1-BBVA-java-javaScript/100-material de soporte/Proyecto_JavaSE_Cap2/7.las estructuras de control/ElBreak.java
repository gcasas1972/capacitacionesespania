
public class ElBreak {

	public static void main(String[] args) {
		System.out.println("¿En qué casilla está el 0 en la tabla?");
		int[][] points = {  
	               { 10,10,}, 
	               { 0,10 }, 
	               { 45,24 }}; 
		int x=0,y=0; //las variables se declaran fuera del bucles para acordarse de las coordenadas de la casilla que contiene el 0
					 //después de la salida de los dos bucles
		boolean encontrado=false; //El boolean permite saber si el 0 se ha encontrado
	
		search: 
		for (x = 0; x <points.length; x++) 
		{ 
		   for (y = 0; y < points[x].length;y++)  
		   { 
		      if (points[x][y] == 0) 
		      { 
		         encontrado = true; 
		         break search; 
		      } 
		   } 
		} 
	
		if (encontrado) //si el 0 se ha encontrado, x-y se corresponden con las coordenadas del 0 si no, se corresponden a la última casilla probada
		{ 
		   System.out.println("resultado encontrado en la casilla " 
		                      + x + "-" + y); 
		} 
		else 
		{ 
		   System.out.println("búsqueda sin éxito"); 
		}
	}
}
