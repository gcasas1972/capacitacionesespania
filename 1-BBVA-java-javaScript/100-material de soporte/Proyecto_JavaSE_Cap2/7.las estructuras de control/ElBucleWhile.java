
public class ElBucleWhile {

	public static void main(String[] args) {
		System.out.println("Todos los números de la franja [0;10]");
		int i=0;
		while(i<=10)//Condición cuyo resultado puede evolucionar entre 2 iteraciones.
		{
			System.out.println(i);
			i+=1;//Instrucción que permitr hacer evolucionar el resutlado de la condición
		}
	}

}
