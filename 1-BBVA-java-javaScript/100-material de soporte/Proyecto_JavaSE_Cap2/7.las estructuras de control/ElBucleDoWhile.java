
public class ElBucleDoWhile {

	public static void main(String[] args) {
		System.out.println("Todos los números de la franja [0;10]");
		int i=0;
		do
		{
			System.out.println(i);
			i+=1;//Instrucción que permite hacer evolucionar el resultado de la condición
		}while(i<=10);//Condición cuyo resultado puede evolucionar entre 2 iteraciones.
	}
}
