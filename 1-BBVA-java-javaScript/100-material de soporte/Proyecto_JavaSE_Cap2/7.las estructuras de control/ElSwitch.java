import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ElSwitch {

	//La gestión de las excepciones se gestiona mejor en el próximo capítulo
	public static void main(String[] args) throws IOException {
		System.out.println("Responda sí, no o cualquier cosa:");
		//La instrucción siguiente permite acceder a lo que introduce el usuario en la consola
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)); 
		String reponse=""; 
		//La instrucción siguiente permite leer lo que introduce el usuario hasta que pulsa la tecla Intro
		reponse=br.readLine(); 
		switch (reponse) 
		{ 
		     case "sí": 
		     case "SÍ": 
		          System.out.println("respuesta positiva"); 
		          break; 
		     case "no": 
		     case "NO": 
		          System.out.println("respuesta negativa"); 
		          break; 
		     default:  
		          System.out.println("respuesta incorrecta"); 
		}
	}
}
