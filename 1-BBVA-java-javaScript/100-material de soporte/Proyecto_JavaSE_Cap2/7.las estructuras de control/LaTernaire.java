
public class ElTernario {

	public static void main(String[] args) {
		int numeroCaballos = 1;
		String formatMessageNumeroCaballos = "hay %d caballos";
		//Hay que determinar la terminación: caballo o caballos?
		/*if(numeroCaballos>1)
		{
			formatMessageNumeroCaballos+="os";
		}
		else
		{
			formatMessageNumeroCaballos+="o";
		}*/
		formatMessageNumeroCaballos+= (numeroCaballos>1?"os":"o");
		System.out.println(String.format(formatMessageNumeroCaballos, numeroCaballos));
	}

}
