
public class ElBucleFor {

	public static void main(String[] args) {
		System.out.println("Todos los números de la franja [0;10]");
		for(int i=0;i<=10;i++)
		{
			System.out.println(i);
		}
		
		System.out.println("Las tablas de multiplicación");
		int multiplicador; 
		for(multiplicador=1;multiplicador<=10;multiplicador++) 
		{ 
		    for (int tabla = 1; tabla <= 10; tabla++) 
		    { 
		       System.out.print(tabla*multiplicador + "\t"); 
		    } 
		    System.out.println(); 
		}
	}
}
