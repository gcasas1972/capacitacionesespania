import java.util.ArrayList;

public class ElBucleForEach {

	public static void main(String[] args) {
		System.out.println("Los colores persentes en la tabla:");
		String[] tabla={"rojo","verde","azul","blanco"}; 
		/*for (int cpt = 0; cpt < tabla.length; cpt++) 
		{ 
		      System.out.println(tabla[cpt]); 
		}*/
		for (String s : tabla) 
		{ 
		     System.out.println(s); 
		}
		
		System.out.println();
		
		System.out.println("Lista de clientes e intento de modificación en un bucle foreach");
		ArrayList<String> lst=new ArrayList<String>(); 
		lst.add("cliente 1"); 
		lst.add("cliente 2"); 
		lst.add("cliente 3"); 
		lst.add("cliente 5"); 
		   
		for(String st:lst) 
		{ 
		   System.out.println(st); 
		   if(st.endsWith("3")) 
		   { 
			   //La línea siguiente provoca un error porque durante la iteración es en modo solo lectura
			   lst.add("cliente 4"); 
		   } 
		}

	}
}
