package fr.eni.editions.lambdas;

import java.time.LocalDate;

public class Cliente extends Persona {

	public Client() {
		super();
	}

	public Client(String n, String p, LocalDate d) {
		super(n, p, d);
	}

	@Override
	public String toString() {
		return "Cliente [getNombre()=" + getNombre() + ", getApellido()=" + getApellido() + ", getFechaNacimiento()="
				+ getFechaNacimiento() + ", calcularEdad()=" + calcularEdad() + "]";
	}

	
	
	

}