package fr.eni.editions.lambdas;

@FunctionalInterface
public interface MiInterfazFuncional {
	int miMetodo(int unParametro);
}
