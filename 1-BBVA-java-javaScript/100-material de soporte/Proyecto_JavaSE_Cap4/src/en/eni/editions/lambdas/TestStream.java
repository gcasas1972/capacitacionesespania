package fr.eni.editions.lambdas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class TestStream {

	public static void main(String[] args) {
//Obtención de un stream genérico
		List<Persona> listaPersonas = new ArrayList<Persona>(
											List.of(new Persona("Wayne", "John", LocalDate.of(1907, 5, 26)),
											        new Persona("McQueen", "Steeve", LocalDate.of(1930, 3, 24)),
											        new Persona("Lennon", "John", LocalDate.of(1940, 10, 9)),
											        new Persona("Gibson", "Mel", LocalDate.of(1956, 1, 3)),
										            new Persona("Willis", "Bruce", LocalDate.of(1955, 3, 19))));
		Stream<Persona> streamPersonas = listaPersonas.stream();
		
		Stream<String> streamStrings = Stream.<String>builder()
													   .add("Hola")
													   .add("tout")
													   .add("le")
													   .add("mundo")
													   .build();

//Obtención de un stream numérico
		
		//IntStream streamNumericoEnteros = IntStream.builder().add(1).add(15).build();
		
		List<Integer> listaEntiers = List.of(1, 15, 13, 5, 48, 6, 89, 25, 46, 35);
		IntStream streamNumericoEnteros = listaEntiers.stream().mapToInt(i->i);
		
		IntStream streamEdadesPersonas = listaPersonas.stream().mapToInt(p->p.calcularEdad());
		
		
//Manipulación de un stream genérico
		//FILTRER ET TRIER
		System.out.println("Las palabras de 5 caracteres mínimo:");
		List<String> palabras5CaracteresMin = streamStrings.filter(s->s.length()>=5)
													   .sorted()
													   .collect(Collectors.toList());
		palabras5CaracteresMin.forEach(s->System.out.println(s));
		
		if(listaPersonas.stream().allMatch(p->p.getFechaNacimiento().getYear()>1945))
			System.out.println("todas las personas nacieron antes de 1945"); 
		else 
			System.out.println("alguans personas nacieron antes de 1945"); 
		   
		   
				listaPersonas.stream().filter(p->p.getFechaNacimiento().getMonthValue()==3)
									   .forEach(p->System.out.println(p.getNombre())); 
		       
		// búsqueda de la persona de más edad de la lista 
		// la expresión lambda representa la implementación
		// de la interfaz Comparator 
		   
		System.out.println(listaPersonas.stream().max((pe1,pe2)-> pe1.calcularEdad()-pe2.calcularEdad())
												  .get()
												  .getNombre());
		   
		// cálculo de la edad media de las personas presentes en la  
		// lista mapToLong genera un nuevo Stream de tipo long
		// obtenido a partir de la referencia de la función 
		// average calcula la medida de este nuevo flujo 
		// getAsDouble lo tranforma en tipo double 
		double edadMedia = listaPersonas.stream().mapToLong(p->p.calcularEdad()) 
												 .average()
												 .getAsDouble(); 
		 
		System.out.println("edad media de las personas de la lista : " + edadMedia + " años"); 

		System.out.println("Creación de una lista de clientes a partir de la lista de personas:");
		//Transformar una colección de personas en colección de clientes
		List<Client> listaClientes = listaPersonas.stream().map(p->new Client(p.getNombre(), 
																			  p.getApellido(), 
																			  p.getFechaNacimiento()))
														   .collect(Collectors.toList());
		listaClientes.forEach(c->System.out.println(c));
//Manipulación de un stream numérico		
		System.out.println("Suma de los números");
		int suma = listaEntiers.stream().mapToInt(i->i)
										 .sum();
		System.out.println("La suma es de " + suma);
		
		System.out.println("Media de las números");
		OptionalDouble media = listaEntiers.stream().mapToInt(i->i)
											 .average();
		System.out.println("La media es de " + media);
		
		
		System.out.println("El valor máximo es :");
		OptionalInt optMax = listaEntiers.stream().mapToInt(i->i).max();
		if(optMax.isPresent())
		{
			System.out.println(optMax.getAsInt());
		}
		else
		{
			System.out.println("Sin valor máximo");
		}
		System.out.println("Devolver el valor max o dar un valor por defecto:");
		int valMax = listaEntiers.stream().mapToInt(i->i).max().orElse(123);
		
	}

}
