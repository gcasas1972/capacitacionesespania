package fr.eni.editions.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class TestLista {

	public static void main(String[] args) {
		List<String> lista = new ArrayList<String>(List.of("hola", "tout", "le", "mundo"));

		System.out.println("Visualización clásica:");
		for(String element : lista)
		{
			System.out.print(element+" ");
		}
		
		System.out.println();
		System.out.println("Visualización con una expresión lambda :");
		
		System.out.println("Ejemplo 1:");
		//s toma sucesivamente el valor 
		//de cada elemento de la lista.
		//La expresión lambda respeta 
		//la firma de la interfaz Consumer<T> :
		//void accept(T t);
		lista.forEach((String element)->{System.out.print(element+" ");});
		
		System.out.println();
		System.out.println("Ejemplo 2:");
		//Sintaxis más condensada
		lista.forEach(s->System.out.print(s+" "));
		
		System.out.println();
		System.out.println("Ejemplo 3:");
		//Definición de una variable del tipo de la interfaz:
		Consumer<String> visualizador = s->System.out.print(s+" ");
		lista.forEach(visualizador);
		
		System.out.println();
		System.out.println("Ejemplo 4:");
		//Uso de una referencia de método
		lista.forEach(TestLista::visualizadorStatic);

		System.out.println();
		System.out.println("Delete de los elementos con un o");
		for(int i=lista.size()-1;i>=0;i--)
		{
			if(lista.get(i).contains("o"))
			{
				lista.remove(i);
			}
		}
		lista.forEach(s->System.out.println(s));
		
		System.out.println();
		System.out.println("Delete con una expresión lambda");
		
		System.out.println();
		System.out.println("Ejemplo 1:");
		lista.removeIf((String s)->{return s.contains("o");});
		lista.forEach(s->System.out.println(s));
		
		System.out.println();
		System.out.println("Ejemplo 2:");
		//Sintaxis más condensada
		lista.removeIf(s->s.contains("o"));
		lista.forEach(s->System.out.println(s));
		
		//Reinicialización de la lista
		lista = new ArrayList<String>(List.of("hola", "todo", "el ", "mundo"));
		
		System.out.println();
		System.out.println("Ordenación de la lista");
		
		
		System.out.println();
		System.out.println("Ejemplo 1:");
		lista.sort((String s1, String s2)->{return s1.compareTo(s2);});
		lista.forEach(s->System.out.println(s));
		
		System.out.println();
		System.out.println("Ejemplo 2:");
		//Sintaxis más condensada
		lista.sort((s1, s2)->s1.compareTo(s2));
		lista.forEach(s->System.out.println(s));
		
	}
	
	//Definición de un método que respecta el método void accept(T t)
	//de la interfaz Consumer<T>
	private static void visualizadorStatic(String s) {
		System.out.print(s+" ");
	}
}
