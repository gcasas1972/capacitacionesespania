package fr.eni.editions.ihm11; 
 
import java.awt.event.WindowEvent; 
import java.awt.event.WindowAdapter; 
 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JPanel; 
 
public class Pantalla extends JFrame 
 
{ 
      public Pantalla() 
      { 
            setTitle("primera ventana en JAVA"); 
            setBounds(0,0,300,100); 
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            // creación de los tres botones 
            JButton b1,b2,b3; 
            b1=new JButton("Rojo"); 
            b2=new JButton("Verde"); 
            b3=new JButton("Azul"); 
            // creación del contenedor intermedio 
            JPanel pano; 
            pano=new JPanel(); 
            // agregar los botones en el contenedor intermedio 
            pano.add(b1); 
            pano.add(b2); 
            pano.add(b3); 
            // agregar el contenedor intermedio en el ContentPane 
            getContentPane().add(pano); 
            // creación de una instancia de una clase anonyme 
            // encargada de administrar los eventos 
            addWindowListaner(new WindowAdapter() 
            // inicio de la definición de la clase 
            { 
                 public void windowClosing(WindowEvent arg0) 
                 { 
                      System.exit(0); 
                 } 
            } // fin de la definición de la clase 
            ); // fin de la llamada del método addWindowListaner 
      }// fin del constructor 
      public void mostrar()
      {
 	     this.setVisible(true);
      }
}// fin de la clase Pantalla 
