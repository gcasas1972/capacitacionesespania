package fr.eni.editions.ihm1;

import javax.swing.JFrame;

public class Principal { 
     public static void main(String[] args) 
     { 
          JFrame ventana; 
          // creación de la instancia de la clase JFrame 
          ventana=new JFrame(); 
          // modificación de la posición y de la 
          // tamanio de la ventana 
          ventana.setBounds(0,0,300,400); 
          // modificación del título de la ventana 
          ventana.setTitle("primera ventana en JAVA"); 
          // visualizacion de la ventana 
          ventana.setVisible(true); 
     }
}

