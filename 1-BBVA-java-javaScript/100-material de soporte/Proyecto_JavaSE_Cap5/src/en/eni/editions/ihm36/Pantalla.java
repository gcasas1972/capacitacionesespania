package fr.eni.editions.ihm36;

import java.time.LocalDate;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Pantalla extends JFrame {

	public Pantalla() {
		setTitle("Cuado de diálogo");
		setBounds(0, 0, 400, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		String nombre;
		nom = JOptionPane.showInputDialog("rellenar el nombre del cliente");
		System.out.println("El nombre introducido es : " + nombre);

		nom = JOptionPane.showInputDialog(null, "rellenar el nombre del cliente", "búsqueda de un cliente",
				JOptionPane.WARNING_MESSAGE);
		System.out.println("El nombre introducido es : " + nombre);

		Persona[] opcion;
		opcion = new Persona[5];
		opcion[0] = new Persona("Wayne", "John", LocalDate.of(1907, 5, 26));
		opcion[1] = new Persona("McQueen", "Steeve", LocalDate.of(1930, 3, 24));
		opcion[2] = new Persona("Lennon", "John", LocalDate.of(1940, 10, 9));
		opcion[3] = new Persona("Gibson", "Mel", LocalDate.of(1956, 1, 3));
		opcion[4] = new Persona("Willis", "Bruce", LocalDate.of(1955, 3, 19));

		Persona elegida;

		elegida = (Persona) JOptionPane.showInputDialog(null, "seleccionar el cliente", "búsqueda de un cliente",
				JOptionPane.WARNING_MESSAGE, null, opcion, opcion[1]);
		System.out.println("La persona elegida es : " + elegida);

		JOptionPane.showMessageDialog(null, "el cliente no se puede encontrar", "búsqueda de un cliente",
				JOptionPane.WARNING_MESSAGE);

		int reponse = JOptionPane.showConfirmDialog(null, "¿Salir sin guardar?",
				"cierre de la aplicación", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		System.out.println("La respuesta es : " + reponse);

	}

	public void mostrar() {
		this.setVisible(true);
	}
}
