package fr.eni.editions.ihm36;

import java.time.LocalDate;
import java.util.Random;

public class Persona {
	
	private String nombre; 
    private String apellido; 
    private LocalDate fechaNacimiento;

    //Aceso / Getters/Setters
    public String getNombre() 
    { 
        return nombre; 
    } 
 
    public void setNombre(String n) 
    { 
        if(n!=null)
        {
            nombre = n.toUpperCase(); 
        }
    } 
 
    public String getApellido() 
    { 
        return apellido; 
    } 
 
    public void setApellido(String p) 
    { 
        if(p!=null)
        {
            apellido = p.toLowerCase(); 
        }
    }
    
    public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


    public Persona(String n, String p, LocalDate d) 
    {   
        setNombre(n);
        setApellido(p);
        fechaNacimiento=d; 
    }

	@Override
	public String toString() {
		return  nombre + " " + apellido;
	}
    
    
}
