package fr.eni.editions.ihm22;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Pantalla extends JFrame {
	JPanel pano;
	JCheckBox chkNegrita, chkItalica;
	JLabel lblTamanio, lblEjemplo;
	JComboBox<String> cboTamanio;
	JList<String> lstTiposLetra;
	JScrollPane desfilaTiposLetra;

	public Pantalla() {
		setTitle("opción de un tipoLetra");
		setBounds(0, 0, 400, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes 
		pano = new JPanel();
		chkNegrita = new JCheckBox("negrita");
		chkItalica = new JCheckBox("italica");
		lblTamanio = new JLabel("tamanio");
		lblEjemplo = new JLabel("prueba un tipoLetra de caracteres");
		cboTamanio = new JComboBox(new String[] { "10", "12", "14", "16", "18", "20" });
		lstTiposLetra = new JList(
				new String[] { "arial", "courier", "letter", "helvetica", "times roman", "symbole", "antique" });
		desfilaTiposLetra = new JScrollPane(lstTiposLetra);

		// agregar en el contenedor 
		pano.setLayout(null);
		pano.add(desfilaTiposLetra);
		pano.add(chkNegrita);
		pano.add(chkItalica);
		pano.add(lblTamanio);
		pano.add(cboTamanio);
		pano.add(lblEjemplo);

		// posición de los componentes. 
		desfilaTiposLetra.setBounds(24, 29, 156, 255);
		chkNegrita.setBounds(258, 78, 170, 25);
		chkItalica.setBounds(261, 139, 167, 46);
		lblTamanio.setBounds(215, 211, 106, 24);
		cboTamanio.setBounds(354, 208, 107, 32);
		lblEjemplo.setBounds(17, 309, 459, 28);
		getContentPane().add(pano);
	}
	public void mostrar() {
		this.setVisible(true);
	}
}