package fr.eni.editions.ihm19;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Pantalla extends JFrame

{
	
	public Pantalla() {
		setTitle("primera ventana en JAVA");
		setBounds(0, 0, 500, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel linea; 
		linea=new JPanel(); 
		BoxLayout bl; 
		bl=new BoxLayout(linea,BoxLayout.X_AXIS); 
		linea.setLayout(bl); 
		JButton b1,b2,b3,b4,b5; 
		// creación de un botón con alineación superior 
		b1=new JButton("petit"); 
		b1.setAlignmentY(0); 
		linea.add(b1);
		
		//Creación de un separador de 10px
		//linea.add(Box.createHorizontalStrut(10));
		//Creación de un separador de 50px de largo y 150px de altura
		linea.add(Box.createRigidArea(new Dimension(50,150))); 
		
		// creación de un botón con alineación inferior 
		b2=new JButton("   medio   "); 
		b2.setAlignmentY(1); 
		linea.add(b2); 
		// uso de html para la etiqueta del botón 
		b3=new JButton("<html>muy<BR>alto</html>"); 
		linea.add(b3); 
		b4=new JButton("      muy    largo      "); 
		linea.add(b4); 
		b5=new JButton("<html>muy alto<br>et<br>muy largo</html>"); 
		linea.add(b5); 
		getContentPane().add(linea);

	}

	public void mostrar() {
		this.setVisible(true);
	}

}
