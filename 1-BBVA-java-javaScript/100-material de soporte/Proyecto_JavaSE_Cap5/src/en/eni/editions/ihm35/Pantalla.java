package fr.eni.editions.ihm35;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListaner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListaner;

public class Pantalla extends JFrame {
	JPanel pano;
	JTextArea txt;
	JScrollPane desfila;
	JMenuBar barra;
	JMenu mnuFichero, mnuEdicion, mnuCopiaSeguridad;
	JMenuItem mnuNuevo, mnuAbrir, mnuGuardar, mnuGuardarEn, mnuSalir;
	JMenuItem mnuCopiar, mnuCortar, mnuPegar;
	File fichero;
	JCheckBox chkNegrita, chkItalica;
	JList<String> tipoLetra;
	JComboBox<Integer> cboTamanio;

	public Pantalla() {
		setTitle("editor de texto");
		setBounds(0, 0, 600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes
		pano = new JPanel();
		pano.setLayout(new BorderLayout());
		txt = new JTextArea();
		desfila = new JScrollPane(txt);
		pano.add(desfila, BorderLayout.CENTER);
		getContentPane().add(pano);
		// creación de los componentes de los menús
		barra = new JMenuBar();
		mnuFichero = new JMenu("Fichero");
		mnuEdicion = new JMenu("Edicion");
		mnuCopiaSeguridad = new JMenu("CopiaSeguridad");
		mnuNuevo = new JMenuItem("Nuevo");
		mnuAbrir = new JMenuItem("Abrir");
		mnuGuardar = new JMenuItem("Guardar");
		mnuGuardar.setEnabled(false);
		mnuGuardarEn = new JMenuItem("Guardar en");
		mnuCopiar = new JMenuItem("Copiar");
		mnuCortar = new JMenuItem("Cortar");
		mnuPegar = new JMenuItem("Pegar");
		mnuSalir = new JMenuItem("Salir");
		// asociación de los elementos
		barra.add(mnuFichero);
		barra.add(mnuEdicion);
		mnuFichero.add(mnuNuevo);
		mnuFichero.add(mnuAbrir);
		mnuFichero.add(mnuCopiaSeguridad);
		mnuCopiaSeguridad.add(mnuGuardar);
		mnuCopiaSeguridad.add(mnuGuardarEn);
		mnuFichero.add(new JSeparator());
		mnuFichero.add(mnuSalir);
		mnuEdicion.add(mnuCopiar);
		mnuEdicion.add(mnuCortar);
		mnuEdicion.add(mnuPegar);
		// asociación del menú con la JFrame
		setJMenuBar(barra);
		// los listaners asociados a las diferentes menús
		mnuNuevo.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent arg0) {
				fichero = null;
				txt.setText("");
				mnuGuardar.setEnabled(false);
			}

		});
		mnuAbrir.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser dlg;
				dlg = new JFileChooser();
				dlg.showDialog(null, "Abrir");
				fichero = dlg.getSelectedFile();
				FileInputStream in;
				try {
					in = new FileInputStream(fichero);
					BufferedReader br;
					br = new BufferedReader(new InputStreamReader(in));
					String linea;
					txt.setText("");
					while ((linea = br.readLine()) != null) {
						txt.append(linea + "\r\n");
					}
					br.close();
					mnuGuardar.setEnabled(true);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		mnuSalir.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnuCopiar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.copy();
			}
		});
		mnuCortar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.cut();
			}
		});
		mnuPegar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.paste();
			}
		});
		mnuGuardar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					PrintWriter pw;
					pw = new PrintWriter(fichero);
					pw.write(txt.getText());
					pw.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		mnuGuardarEn.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					JFileChooser dlg;
					dlg = new JFileChooser();
					dlg.showDialog(null, "guardar en");
					fichero = dlg.getSelectedFile();
					PrintWriter pw;
					pw = new PrintWriter(fichero);
					pw.write(txt.getText());
					pw.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		});

		JToolBar tlbr;
		tlbr = new JToolBar();
		JButton btnNuevo, btnAbrir, btnGuardar;
		JButton btnCopiar, btnCortar, btnPegar;
		// creación de los botones
		btnNuevo = new JButton(new ImageIcon(getClass().getResource("new.png")));
		btnAbrir = new JButton(new ImageIcon(getClass().getResource("open.png")));
		btnGuardar = new JButton(new ImageIcon(getClass().getResource("save.png")));
		btnCopiar = new JButton(new ImageIcon(getClass().getResource("copy.png")));
		btnPegar = new JButton(new ImageIcon(getClass().getResource("paste.png")));
		btnCortar = new JButton(new ImageIcon(getClass().getResource("cut.png")));
		// agregar los botones a la barra de herramientas
		tlbr.add(btnNuevo);
		tlbr.add(btnAbrir);
		tlbr.add(btnGuardar);
		tlbr.addSeparator();
		tlbr.add(btnCopiar);
		tlbr.add(btnCortar);
		tlbr.add(btnPegar);
		// agregar la barra de herramientas en su contenedor
		pano.add(tlbr, BorderLayout.NORTH);
		// reutilización listaners ya asociados a los menús
		btnNuevo.addActionListaner(mnuNuevo.getActionListaners()[0]);
		btnAbrir.addActionListaner(mnuAbrir.getActionListaners()[0]);
		btnGuardar.addActionListaner(mnuGuardar.getActionListaners()[0]);
		btnCopiar.addActionListaner(mnuCopiar.getActionListaners()[0]);
		btnCortar.addActionListaner(mnuCortar.getActionListaners()[0]);
		btnPegar.addActionListaner(mnuPegar.getActionListaners()[0]);

		// Agregar las opciones para el estilo de escritura
		JPanel options;
		GridLayout gl;
		options = new JPanel();
		gl = new GridLayout(2, 1);
		options.setLayout(gl);
		chkNegrita = new JCheckBox("Negrita");
		chkItalica = new JCheckBox("Italica");
		options.add(chkNegrita);
		options.add(chkItalica);
		chkNegrita.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				cambiaTipoLetra();
			}
		});
		chkItalica.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				cambiaTipoLetra();
			}
		});

		// Agregar las opciones para la tipoLetra de escritura
		JScrollPane desfilaTiposLetra;

		String[] nombreTiposLetra = { "Dialog", "DialogInput", "Monospaced", "Serif", "SansSerif" };
		tipoLetra = new JList<String>(nombreTiposLetra);
		tipoLetra.setSelectedIndex(0);
		desfilaTiposLetra = new JScrollPane(tipoLetra);
		desfilaTiposLetra.setPreferredSize(new Dimension(100, 60));
		options.add(desfilaTiposLetra);
		tipoLetra.addListSelectionListaner(new ListSelectionListaner() {

			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					cambiaTipoLetra();
				}
			}
		});

		// Agregar las optiones para la tamanio de la tipoLetra
		Integer tamanios[] = { 10, 12, 14, 16, 20 };
		cboTamanio = new JComboBox<Integer>(tamanios);
		cboTamanio.setEditable(true);
		options.add(cboTamanio);
		cboTamanio.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				cambiaTipoLetra();
			}
		});

		pano.add(options, BorderLayout.SOUTH);

		// Agregar las opciones para administrar el color del texto
		// y el color de fondo
		JRadioButton optFondoRojo, optFondoVerde, optFondoAzul;
		JRadioButton optRojo, optVerde, optAzul;
		JPanel color, colorFondo;
		ButtonGroup grpColor, grpColorFondo;
		// creación de los botones
		optRojo = new JRadioButton("Rojo");
		optVerde = new JRadioButton("Verde");
		optAzul = new JRadioButton("Azul");
		optFondoRojo = new JRadioButton("Rojo");
		optFondoVerde = new JRadioButton("Verde");
		optFondoAzul = new JRadioButton("Azul");
		// agrupación lógica de los botones
		grpColor = new ButtonGroup();
		grpColor.add(optRojo);
		grpColor.add(optVerde);
		grpColor.add(optAzul);
		grpColorFond = new ButtonGroup();
		grpColorFond.add(optFondoRojo);
		grpColorFond.add(optFondoVerde);
		grpColorFond.add(optFondoAzul);
		// agrupación física de los botones
		color = new JPanel();
		color.setLayout(new GridLayout(0, 1));
		color.add(optRojo);
		color.add(optVerde);
		color.add(optAzul);

		pano.add(color, BorderLayout.WEST);

		colorFondo = new JPanel();
		colorFondo.setLayout(new GridLayout(0, 1));
		colorFondo.add(optFondoRojo);
		colorFondo.add(optFondoVerde);
		colorFondo.add(optFondoAzul);

		pano.add(colorFondo, BorderLayout.EAST);

		// agregar un borde con título
		color.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Texto"));
		colorFondo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Fondo"));
		// referencia de los listaners
		optAzul.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setForeground(Color.BLUE);
			}
		});
		optVerde.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setForeground(Color.GREEN);
			}
		});
		optRojo.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setForeground(Color.RED);
			}
		});
		optFondoAzul.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setBackground(Color.BLUE);
			}
		});
		optFondoRojo.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setBackground(Color.RED);
			}
		});
		optFondoVerde.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setBackground(Color.GREEN);
			}
		});

	}

	public void cambiaTipoLetra() {
		int atributos;
		atributos = 0;
		if (chkNegrita.isSelected()) {
			atributos = atributos + Font.BOLD;
		}
		if (chkItalica.isSelected()) {
			atributos = atributos + Font.ITALIC;
		}

		Font tipoLetra;
		tipoLetra = new Font(tipoLetra.getSelectedValue().toString(), atributos,
				cboTamanio.getItemAt(cboTamanio.getSelectedIndex()));
		txt.setFont(tipoLetra);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
