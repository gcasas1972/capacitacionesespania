package fr.eni.editions.ihm24;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Pantalla extends JFrame

{
	JPanel pano;

	public Pantalla() {
		setTitle("Presentación JLabel");
		setBounds(0, 0, 220, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes

		pano = new JPanel();
		
		JLabel legende=new JLabel("nombre"); 
		legende.setBorder(BorderFactory.createEtchedBorder()); 
		legende.setPreferredSize(new Dimension(200,50)); 
		legende.setHorizontalAlignment(SwingConstants.LEFT); 
		legende.setVerdeicalAlignment(SwingConstants.TOP); 

		pano.add(legende);
		
		getContentPane().add(pano);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
