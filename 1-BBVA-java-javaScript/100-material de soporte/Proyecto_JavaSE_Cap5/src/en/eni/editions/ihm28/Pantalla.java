package fr.eni.editions.ihm28;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListaner;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListaner;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.undo.UndoManager;

public class Pantalla extends JFrame

{
	JPanel pano;

	public Pantalla() {
		setTitle("Presentación JTextField");
		setBounds(0, 0, 400, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes
		pano = new JPanel();

		JTextField txt = new JTextField(10);
		txt.addFocusListaner(new FocusListaner() {
			// Selección del texto en el focus
			public void focusGained(FocusEvent e) {
				txt.selectAll();
			}

			// Deselección del texto al perder el focus
			public void focusLost(FocusEvent e) {
				txt.setSelectionStart(0);
				txt.setSelectionEnd(0);
			}
		});

		// Establecimiento de un menú para administrar los copiar/cortar/pegar.
		JPopupMenu mnu = new JPopupMenu();
		JMenuItem mnuCopiar;
		JMenuItem mnuCortar;
		JMenuItem mnuPegar;
		mnuCopiar = new JMenuItem("Copiar");
		mnuCortar = new JMenuItem("Cortar");
		mnuPegar = new JMenuItem("Pegar");
		mnuCopiar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				// Todo copiar
				txt.selectAll();
				txt.copy();
			}
		});
		mnuCortar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				// Todo cortar
				txt.selectAll();
				txt.cut();
			}
		});
		mnuPegar.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.paste();
			}
		});
		mnu.add(mnuCopiar);
		mnu.add(mnuCortar);
		mnu.add(mnuPegar);
		// Hacer aparecer el menú con un clic derecho en la zona de texto
		txt.addMouseListaner(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					mnu.show((Component) e.getSource(), e.getX(), e.getY());
				}
			}
		});

		// creación del UndoManager
		UndoManager udm;
		udm = new UndoManager();
		// asociación con el JTextField
		txt.getDocument().addUndoableEditListaner(udm);
		// agregar un listaner para interceptar el ctrl Z
		txt.addKeyListaner(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == (int) 'Z' && e.isControlDown()) {
					udm.undo();
				}
			}
		});

		JTextField txt2 = new JTextField(10);

		pano.add(txt);
		pano.add(txt2);
		pano.add(mnu);

		getContentPane().add(pano);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
