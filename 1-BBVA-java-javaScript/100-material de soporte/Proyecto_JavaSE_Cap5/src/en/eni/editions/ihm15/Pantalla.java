package fr.eni.editions.ihm15;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListaner;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Pantalla extends JFrame

{
	JPanel pano;
	JButton btnRojo, btnVerde, btnAzul;
	JMenuItem mnuRojo, mnuVerde, mnuAzul;
	ConsoleLog lg;

	public Pantalla() {
		setTitle("primera ventana en JAVA");
		setBounds(0, 0, 300, 130);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		// creación de los tres botones

		btnRojo = new JButton("Rojo");
		btnRojo.setActionCommand("red");
		btnVerde = new JButton("Verde");
		btnVerde.setActionCommand("green");
		btnAzul = new JButton("Azul");
		btnAzul.setActionCommand("blue");
		// creación del único listaner
		ListanerColor ec;
		ec = new ListanerColor();
		// asociación del listaner a cada botón
		btnRojo.addActionListaner(ec);
		btnVerde.addActionListaner(ec);
		btnAzul.addActionListaner(ec);
		// creación de la casilla de selección
		JCheckBox chkLog;
		chkLog = new JCheckBox("log en consola");
		// agregar un listaner a la casilla de selección
		chkLog.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent arg0) {
				JCheckBox chk;
				chk = (JCheckBox) arg0.getSource();
				if (chk.isSelected()) // Si la casilla está marcada
				{
					// agregar un listaner adicional
					// a los botones y menús
					lg = new ConsoleLog();
					btnAzul.addActionListaner(lg);
					btnRojo.addActionListaner(lg);
					btnVerde.addActionListaner(lg);
					mnuAzul.addActionListaner(lg);
					mnuRojo.addActionListaner(lg);
					mnuVerde.addActionListaner(lg);
				} else {
					// eliminación del listaner adicional
					// de los botones y menús
					btnAzul.removeActionListaner(lg);
					btnRojo.removeActionListaner(lg);
					btnVerde.removeActionListaner(lg);
					mnuAzul.removeActionListaner(lg);
					mnuRojo.removeActionListaner(lg);
					mnuVerde.removeActionListaner(lg);
				}

			}
		});
		// Creación del menú
		JMenuBar barraMenu;
		barraMenu = new JMenuBar();
		JMenu mnuColors;
		mnuColors = new JMenu("Colores");
		barraMenu.add(mnuColors);
		mnuRojo = new JMenuItem("Rojo");
		mnuRojo.setActionCommand("red");
		mnuVerde = new JMenuItem("Verde");
		mnuVerde.setActionCommand("green");
		mnuAzul = new JMenuItem("Azul");
		mnuAzul.setActionCommand("blue");
		mnuColors.add(mnuRojo);
		mnuColors.add(mnuVerde);
		mnuColors.add(mnuAzul);
		// asociación del listaner a cada menú
		// (el mismo que para los botones)
		mnuRojo.addActionListaner(ec);
		mnuVerde.addActionListaner(ec);
		mnuAzul.addActionListaner(ec);
		// agregar el menú en la ventana
		setJMenuBar(barraMenu);
		// creación del contenedor intermedio
		pano = new JPanel();
		// agregar los botones en el contenedor intermedio
		pano.add(btnRojo);
		pano.add(btnVerde);
		pano.add(btnAzul);
		pano.add(chkLog);
		// agregar el contenedor intermedio en el ContentPane
		getContentPane().add(pano);
		// creación de una instancia de una clase anonyme
		// encargada de administrar los eventos
		addWindowListaner(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public class ListanerColor implements ActionListaner {
		public void actionPerformed(ActionEvent arg0) {
			String comando;
			comando = arg0.getActionCommand();
			if (comando.equals("red")) {
				pano.setBackground(Color.RED);
			}
			if (comando.equals("green")) {
				pano.setBackground(Color.GREEN);
			}
			if (comando.equals("blue")) {
				pano.setBackground(Color.BLUE);
			}
		}
	}

}
