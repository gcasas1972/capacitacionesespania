package fr.eni.editions.ihm2; 
 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JPanel; 
 
public class Principal { 
 
 
     public static void main(String[] args) 
 
     { 
          // creación de la ventana 
          JFrame ventana; 
          ventana=new JFrame(); 
          ventana.setTitle("primera ventana en JAVA"); 
          ventana.setBounds(0,0,300,100); 
          ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
          // creación de los tres botones 
          JButton b1,b2,b3; 
          b1=new JButton("Rojo"); 
          b2=new JButton("Verde"); 
          b3=new JButton("Azul"); 
          // creación del contenedor intermedio 
          JPanel pano; 
          pano=new JPanel(); 
          // agregar los botones en el contenedor intermedio 
          pano.add(b1); 
          pano.add(b2); 
          pano.add(b3); 
          // agregar el contenedor intermedio en el ContentPane 
          ventana.getContentPane().add(pano); 
          // visualizacion de la ventana 
          ventana.setVisible(true); 
     } 
}
