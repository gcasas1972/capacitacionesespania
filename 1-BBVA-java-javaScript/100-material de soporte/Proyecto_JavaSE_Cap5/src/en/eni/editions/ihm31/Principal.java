package fr.eni.editions.ihm31;

import javax.swing.SwingUtilities;

public class Principal { 
 
     public static void main(String[] args) 
     { 
          SwingUtilities.invokeLater(()->new Pantalla().mostrar());
     } 
}
