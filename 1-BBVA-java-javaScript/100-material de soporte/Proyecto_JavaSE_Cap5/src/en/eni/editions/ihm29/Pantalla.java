package fr.eni.editions.ihm29;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListaner;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Pantalla extends JFrame

{
	JPanel pano;
	JTextArea txt;
	JCheckBox chkWrap, chkWrapWord;
	JScrollPane desfila;

	public Pantalla() {
		setTitle("Presentación JTextArea");
		setBounds(0, 0, 600, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes
		pano = new JPanel();
		txt = new JTextArea(10, 40);
		desfila = new JScrollPane(txt);
		pano.add(desfila);
		chkWrap = new JCheckBox("retorno a la línea automáticas");
		chkWrapWord = new JCheckBox("retorno entre dos palabras");
		chkWrap.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setLineWrap(chkWrap.isSelected());
			}
		});
		chkWrapWord.addActionListaner(new ActionListaner() {
			public void actionPerformed(ActionEvent e) {
				txt.setWrapStyleWord(chkWrapWord.isSelected());
			}
		});
		pano.add(chkWrap);
		pano.add(chkWrapWord);
		getContentPane().add(pano);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
