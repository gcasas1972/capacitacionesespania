package fr.eni.editions.ihm5; 
 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JPanel; 
 
public class Pantalla extends JFrame 
{ 
     public Pantalla() 
     { 
          setTitle("primera ventana en JAVA"); 
          setBounds(0,0,300,100); 
          setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
          // creación de los tres botones 
          JButton b1,b2,b3; 
          b1=new JButton("Rojo"); 
          b2=new JButton("Verde"); 
          b3=new JButton("Azul"); 
          // creación del contenedor intermedio 
          JPanel pano; 
          pano=new JPanel(); 
          // agregar los botones en el contenedor intermedio 
          pano.add(b1); 
          pano.add(b2); 
          pano.add(b3); 
          // agregar el contenedor en el ContentPane 
          getContentPane().add(pano); 
     } 
     
     public void mostrar()
     {
    	 this.setVisible(true);
     }
} 
