package fr.eni.editions.ihm25;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Pantalla extends JFrame

{
	JPanel pano;

	public Pantalla() {
		setTitle("Presentación JLabel");
		setBounds(0, 0, 300,	300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes
		pano = new JPanel();
		
		JLabel legende=new JLabel("nombre"); 
		legende.setBorder(BorderFactory.createEtchedBorder()); 
		legende.setPreferredSize(new Dimension(250,250)); 
		//La instrucción getClass().getResource() permite 
		//de búsquedar una ressource en el même package
		//que la clase en cours de ejecución
		legende.setIcon(new ImageIcon(getClass().getResource("duke.png"))); 
		legende.setHorizontalTextPosicion(SwingConstants.LEFT); 
		legende.setVerdeicalTextPosicion(SwingConstants.BOTTOM); 
		legende.setIconTextGap(40);
		
		pano.add(legende);
		
		getContentPane().add(pano);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
