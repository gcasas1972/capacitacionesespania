package fr.eni.editions.ihm12;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListaner;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Pantalla extends JFrame

{
	JPanel pano;

	public Pantalla() {
		setTitle("primera ventana en JAVA");
		setBounds(0, 0, 300, 100);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		// creación de los tres botones
		JButton btnRojo, btnVerde, btnAzul;
		btnRojo = new JButton("Rojo");
		btnVerde = new JButton("Verde");
		btnAzul = new JButton("Azul");
		// creación de los tres listaners
		ListanerRojo ecR;
		ListanerVerde ecV;
		ListanerAzul ecB;
		ecR = new ListanerRojo();
		ecV = new ListanerVerde();
		ecB = new ListanerAzul();
		// asociación del listaner a cada botón
		btnRojo.addActionListaner(ecR);
		btnVerde.addActionListaner(ecV);
		btnAzul.addActionListaner(ecB);
		// Creación del menú
		JMenuBar barraMenu;
		barraMenu = new JMenuBar();
		JMenu mnuColors;
		mnuColors = new JMenu("Colores");
		barraMenu.add(mnuColors);
		JMenuItem mnuRojo, mnuVerde, mnuAzul;
		mnuRojo = new JMenuItem("Rojo");
		mnuVerde = new JMenuItem("Verde");
		mnuAzul = new JMenuItem("Azul");
		mnuColors.add(mnuRojo);
		mnuColors.add(mnuVerde);
		mnuColors.add(mnuAzul);
		// asociación del listaner a cada menú
		// (los mismos que para los botones)
		mnuRojo.addActionListaner(ecR);
		mnuVerde.addActionListaner(ecV);
		mnuAzul.addActionListaner(ecB);
		// agregar el menú en la ventana
		setJMenuBar(barraMenu);
		// creación del contenedor intermedio
		pano = new JPanel();
		// agregar los botones en el contenedor intermedio
		pano.add(btnRojo);
		pano.add(btnVerde);
		pano.add(btnAzul);
		// agregar el contenedor intermedio en el ContentPane
		getContentPane().add(pano);
		// creación de una instancia de una clase anonyme
		// encargada de administrar los eventos de la ventana
		addWindowListaner(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public class ListanerRojo implements ActionListaner {
		public void actionPerformed(ActionEvent arg0) {
			pano.setBackground(Color.RED);
		}
	}

	public class ListanerVerde implements ActionListaner {
		public void actionPerformed(ActionEvent arg0) {
			pano.setBackground(Color.GREEN);
		}
	}

	public class ListanerAzul implements ActionListaner {
		public void actionPerformed(ActionEvent arg0) {
			pano.setBackground(Color.BLUE);
		}
	}

}
