package fr.eni.editions.ihm23;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListaner;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Pantalla extends JFrame

{
	JPanel pano;
	JCheckBox chkNegrita, chkItalica;
	JLabel lblTamanio, lblEjemplo;
	JComboBox<String> cboTamanio;
	JList<String> lstTiposLetra;
	JScrollPane desfilaTiposLetra;

	public Pantalla() {
		setTitle("opción de un tipoLetra");
		setBounds(0, 0, 400, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes

		pano = new JPanel();
		chkNegrita = new JCheckBox("negrita");
		chkItalica = new JCheckBox("italica");
		lblTamanio = new JLabel("tamanio");
		lblEjemplo = new JLabel("intento de tipoLetra de caracteres");
		cboTamanio = new JComboBox(new String[] { "10", "12", "14", "16", "18", "20" });
		lstTiposLetra = new JList(
				new String[] { "arial", "courier", "letter", "helvetica", "times roman", "symbole", "antique" });
		desfilaTiposLetra = new JScrollPane(lstTiposLetra);

		GridBagLayout gbl;
		gbl = new GridBagLayout();
		pano.setLayout(gbl);

		GridBagConstraints gbc;
		gbc = new GridBagConstraints();
		// posición en la casilla 0,0
		gbc.gridy = 0;
		// en una columna de longitud
		gbc.gridwidth = 1;
		// y en tres líneas de altura
		gbc.gridheight = 3;
		// ponderación en caso de agrandar el contenedor
		gbc.weightx = 100;
		gbc.weighty = 100;
		// el componente se redimensiona para ocupar
		// todo el espacio disponible en su contenedor
		gbc.fill = GridBagConstraints.BOTH;
		pano.add(desfilaTiposLetra, gbc);
		// posición en la casilla 1,0
		gbc.gridx = 1;
		gbc.gridy = 0;
		// en dos columnas de longitud
		gbc.gridwidth = 2;
		// y en una línea de altura
		gbc.gridheight = 1;
		// ponderación en caso de agrandar el contenedor
		gbc.weightx = 100;
		gbc.weighty = 100;
		// el componente no está redimensionado para ocupar
		// todo el espacio disponible en su contenedor
		gbc.fill = GridBagConstraints.NONE;
		pano.add(chkNegrita, gbc);
		// posición en la casilla 1,1
		gbc.gridx = 1;
		gbc.gridy = 1;
		// en dos columnas de longitud
		gbc.gridwidth = 2;
		// y en una línea de altura
		gbc.gridheight = 1;
		// ponderación en caso de agrandar el contenedor
		gbc.weightx = 100;
		gbc.weighty = 100;
		pano.add(chkItalica, gbc);
		// posición en la casilla 1,2
		gbc.gridx = 1;
		gbc.gridy = 2;
		// en una columna de longitud
		gbc.gridwidth = 1;
		// y en una línea de altura
		gbc.gridheight = 1;
		// ponderación en caso de agrandar el contenedor
		gbc.weightx = 100;
		gbc.weighty = 100;
		pano.add(lblTamanio, gbc);
		// posición en la casilla 2,2
		gbc.gridx = 2;
		gbc.gridy = 2;
		// en una columna de longitud
		gbc.gridwidth = 1;
		// y en una línea de altura
		gbc.gridheight = 1;
		// ponderación en caso de agrandar el contenedor
		gbc.weightx = 100;
		gbc.weighty = 100;
		pano.add(cboTamanio, gbc);
		// posición en la casilla 0,3
		gbc.gridx = 0;
		gbc.gridy = 3;
		// en tres columnas de longitud
		gbc.gridwidth = 3;
		// y en una línea de altura
		gbc.gridheight = 1;
		// ponderación en caso de agrandar el contenedor
		gbc.weightx = 100;
		gbc.weighty = 100;
		pano.add(lblEjemplo, gbc);
		getContentPane().add(pano);
		
		//Gestión de la apariencia:
		//lstTiposLetra.setBackground(Color.red); 
		//lstTiposLetra.setForeground(Color.GREEN);
		lstTiposLetra.setBackground(new Color(23,67,89)); 
		lstTiposLetra.setForeground(new Color(167,86,23));
		lstTiposLetra.setFont(new Font("Serif",Font.BOLD,24));
		lstTiposLetra.setCursor(new Cursor(Cursor.HAND_CURSOR));
		chkNegrita.setVisible(false); 
		chkItalica.setEnabled(false); 
		lstTiposLetra.addFocusListaner(new FocusListaner() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				lstTiposLetra.setForeground(Color.RED); 
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				lstTiposLetra.setForeground(Color.BLACK); 
			}
		});

	}

	public void mostrar() {
		this.setVisible(true);
	}
}
