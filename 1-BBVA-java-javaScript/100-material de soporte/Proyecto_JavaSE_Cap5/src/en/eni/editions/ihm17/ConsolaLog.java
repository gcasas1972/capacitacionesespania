package fr.eni.editions.ihm17; 
 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListaner; 
import java.text.SimpleDateFormat; 
import java.util.Date; 
 
import javax.swing.AbstractButton; 
import javax.swing.JButton; 
import javax.swing.JMenuItem; 
 
public class ConsoleLog implements ActionListaner 
{ 
      public void actionPerformed(ActionEvent e) 
      { 
            String message; 
            SimpleDateFormat sdf; 
            sdf=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss"); 
            message=sdf.format(new Date()); 
            message=message + " clic en el "; 
            if (e.getSource() instanceof JButton) 
            { 
                  message=message+ "bot�n "; 
            } 
            if (e.getSource() instanceof JMenuItem) 
            { 
                  message=message+ "men� "; 
            } 
            message=message + ((AbstractButton)e.getSource()).getText(); 
            System.out.println(message); 
      } 
}
