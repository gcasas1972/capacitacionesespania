package fr.eni.editions.ihm17;

import javax.swing.SwingUtilities;

public class Principal { 
 
     public static void main(String[] args) 
     { 
          SwingUtilities.invokeLater(()->new Pantalla().mostrar());
     } 
}
