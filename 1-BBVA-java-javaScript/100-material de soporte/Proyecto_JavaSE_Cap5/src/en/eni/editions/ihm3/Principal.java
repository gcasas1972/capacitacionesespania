package fr.eni.editions.ihm3;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Principal {
	private JFrame ventana;
	//Constructor llamado por el thread EDT
	Principal(String[] args) {
        // Construcciñon de la iIHM
		// creación de la ventana 
        this.ventana=new JFrame(); 
        this.ventana.setTitle("primera ventana en JAVA"); 
        this.ventana.setBounds(0,0,300,100); 
        this.ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        // creación de los tres botones 
        JButton b1,b2,b3; 
        b1=new JButton("Rojo"); 
        b2=new JButton("Verde"); 
        b3=new JButton("Azul"); 
        // creación del contenedor intermedio 
        JPanel pano; 
        pano=new JPanel(); 
        // agregar los botones en el contenedor intermedio 
        pano.add(b1); 
        pano.add(b2); 
        pano.add(b3); 
        // agregar el contenedor intermedio en el ContentPane 
        this.ventana.getContentPane().add(pano);
    }

	//Método llamado por el thread EDT
    public void mostrar() {
        // Visualización de la iIHM
    	this.ventana.setVisible(true);
    }

    //Punto de entrada ejecutado por el thread main
    public static void main(final String[] args) {
        // Programa una tarea para el thread EDT:
        // Creación y visualizacion de la interfaz gráfica
        /*SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Principal3(args).mostrar();
            }
        });*/
    	SwingUtilities.invokeLater(()->new Principal(args).mostrar());
    }
}
