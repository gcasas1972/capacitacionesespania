package fr.eni.editions.ihm39;


import javax.swing.SwingWorker;

public class CuentaHaciaAtrasSwingWorker extends SwingWorker<String, Integer> {

	//Método ejecutado en un thread hijo
	@Override
	protected String doInBackground() throws Exception {
		for(int i=10;i>=0;i--)
		{
			System.out.println(i);
			this.setProgress(i);
			Thread.sleep(1000);
		}
		return "terminado";
	}
}
