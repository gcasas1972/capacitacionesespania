package fr.eni.editions.ihm26;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Pantalla extends JFrame

{
	JPanel pano;

	public Pantalla() {
		setTitle("Presentación JLabel");
		setBounds(0, 0, 400, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creación de los componentes
		pano = new JPanel();
		
		JLabel lNom=new JLabel("Nom:"); 
		JTextField tfNom=new JTextField();
		tfNom.setPreferredSize(new Dimension(100,25));
		//Raccourci para accéder a la zona de introducido:
		lNom.setDisplayedMnemonic('n');
		lNom.setLabelFor(tfNom);
		
		JLabel lApellido=new JLabel("Prénom:"); 
		JTextField tfApellido=new JTextField();
		tfApellido.setPreferredSize(new Dimension(100,25));
		//Raccourci para accéder a la zona de introducido:
		lApellido.setDisplayedMnemonic('p');
		lApellido.setLabelFor(tfApellidobre);
				
		
		pano.add(lNom);
		pano.add(tfNom);
		pano.add(lApellidobre);
		pano.add(tfApellidobre);
		
		getContentPane().add(pano);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
