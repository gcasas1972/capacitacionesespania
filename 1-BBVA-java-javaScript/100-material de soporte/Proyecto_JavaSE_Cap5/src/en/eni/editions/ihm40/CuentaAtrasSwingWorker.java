package fr.eni.editions.ihm40;


import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

public class CuentaHaciaAtrasSwingWorker extends SwingWorker<String, Integer> {

	private JLabel label;
	private JTextArea textArea;
	
	public CuentaHaciaAtrasSwingWorker(JLabel label, JTextArea textArea) {
		this.label=label;
		this.textArea=textArea;
	}

	//Método ejecutado en un thread hijo
	@Override
	protected String doInBackground() throws Exception {
		for(int i=10;i>=0;i--)
		{
			System.out.println(i);
			this.publish(i);
			Thread.sleep(1000);
		}
		return "terminado";
	}
	
	//Ejecutado en el thread EDT
	@Override
	protected void process(List<Integer> chunks) {
		label.setText("Tiempo restante : " + chunks.get(chunks.size()-1));
	}
	
	//Ejecutado en el thread EDT
	@Override
	protected void done() {
		try {
			label.setText(this.get() + 
					", número de caracteres introducidos : " + 
					textArea.getText().length());
			textArea.setEnabled(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
