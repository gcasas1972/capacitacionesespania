package fr.eni.editions.ihm13;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListaner;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Pantalla extends JFrame

{
	JPanel pano;
	JButton btnRojo, btnVerde, btnAzul;
	JMenuItem mnuRojo, mnuVerde, mnuAzul;

	public Pantalla() {
		setTitle("primera ventana en JAVA");
		setBounds(0, 0, 300, 100);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		// creación de los tres botones
		btnRojo = new JButton("Rojo");
		btnVerde = new JButton("Verde");
		btnAzul = new JButton("Azul");
		// creación de un solo listaner
		ListanerColor ec;
		ec = new ListanerColor();
		// asociación del listaner a cada botón
		btnRojo.addActionListaner(ec);
		btnVerde.addActionListaner(ec);
		btnAzul.addActionListaner(ec);
		// Creación del menú
		JMenuBar barraMenu;
		barraMenu = new JMenuBar();
		JMenu mnuColors;
		mnuColors = new JMenu("Colores");
		barraMenu.add(mnuColors);

		mnuRojo = new JMenuItem("Rojo");
		mnuVerde = new JMenuItem("Verde");
		mnuAzul = new JMenuItem("Azul");
		mnuColors.add(mnuRojo);
		mnuColors.add(mnuVerde);
		mnuColors.add(mnuAzul);
		// asociación del listaner a cada menú
		// (el mismo que para los botones)
		mnuRojo.addActionListaner(ec);
		mnuVerde.addActionListaner(ec);
		mnuAzul.addActionListaner(ec);
		// agregar el menú en la ventana
		setJMenuBar(barraMenu);
		// creación del contenedor intermedio
		pano = new JPanel();
		// agregar los botones en el contenedor intermedio
		pano.add(btnRojo);
		pano.add(btnVerde);
		pano.add(btnAzul);
		// agregar el contenedor intermedio en el ContentPane
		getContentPane().add(pano);
		// creación de una instancia de una clase anonyme
		// encargada de administrar los eventos
		addWindowListaner(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public class ListanerColor implements ActionListaner {
		public void actionPerformed(ActionEvent arg0) {
			if (arg0.getSource() == btnRojo || arg0.getSource() == mnuRojo) {
				pano.setBackground(Color.RED);
			}
			if (arg0.getSource() == btnVerde || arg0.getSource() == mnuVerde) {
				pano.setBackground(Color.GREEN);
			}
			if (arg0.getSource() == btnAzul || arg0.getSource() == mnuAzul) {
				pano.setBackground(Color.BLUE);
			}
		}
	}

}
