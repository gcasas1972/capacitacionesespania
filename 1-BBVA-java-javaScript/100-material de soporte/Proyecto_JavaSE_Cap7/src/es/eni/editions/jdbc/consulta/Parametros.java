package fr.eni.editions.jdbc.consulta;

import java.io.FileInputStream;
import java.util.Properties;

public abstract class Parametros {
	//Objeto que permite almacenar en memoria el contenido del fichero properties
	private static Properties props = new Properties();
	
	//Cambio del fichero al cambio de la clase
	static {
		try(FileInputStream input = new FileInputStream("./conf/configuration.properties")){
			props.load(input);
	    } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param clave
	 * @return el valor asociada a la clave o una {@link RuntimeException} en caso de ausencia de valor
	 */
	public static String getValor(String clave) {
		String valor = props.getProperty(clave);
		if(valor!=null) {
			return valor;
		}
		throw new RuntimeException("El fichero configuration.properties no está disponible");
	}
}
 