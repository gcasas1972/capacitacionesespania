package fr.eni.editions.collections;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TestArrayList {
	public static void main(String[] args) {
		// Declaración de dos variables
		// del tipo de la interfaz deseada
		List<Persona> lista1;
		List<Persona> lista2;
		// creación de las dos instancias
		lista1 = new ArrayList<Persona>();
		lista2 = new ArrayList<Persona>();

		// creación de las personas para rellenar la lista
		Persona p1, p2, p3, p4, p5;
		p1 = new Persona("Wayne", "John", LocalDate.of(1907, 5, 26));
		p2 = new Persona("McQueen", "Steeve", LocalDate.of(1930, 3, 24));
		p3 = new Persona("Lennon", "John", LocalDate.of(1940, 10, 9));
		p4 = new Persona("Gibson", "Mel", LocalDate.of(1956, 1, 3));
		p5 = new Persona("Willis", "Bruce", LocalDate.of(1955, 3, 19));

		// agregar cuatro personas a la lista
		lista1.add(p1);
		lista1.add(p3);
		lista1.add(p4);
		lista1.add(p5);

		// inserción de una persona entre p1 y p3
		// donc a la posición 1 de la lista
		lista1.add(1, p2);

		// agregar el contenido de una lista en otra lista
		// las dos listas contienen ahora los mismos objetos.
		// !!!! no confundir con lista2=lista1; !!!
		lista2.addAll(lista1);

		// visualizacion del número de elementos de la lista
		System.out.println("Hay " + lista1.size() + " persona(s) en la lista");

		System.out.println("Se muestran las personas en la lista 1 (foreach):");
		// recorrido de la primera lista con el bucle foreach
		for (Persona persona : lista1) {
			System.out.println(persona.getNombre());
		}

		System.out.println("Se muestran las personas en la lista 1 (for):");
		// recorrido de la primera lista con el bucle for
		for (int index = 0; index < lista1.size(); index++) {
			System.out.println(lista1.get(index).getNombre());
		}

		System.out.println("Se muestran las personas en la lista 1 (Iterator):");
		// recorrido de la primera lista del inicio hasta el final
		Iterator<Persona> it;
		it = lista1.iterator();
		Persona p;
		// mientras haya elementos

		while (it.hasNext()) {
			// recuperación del elemento actual
			p = it.next();
			System.out.println(p.getNombre());
		}

		System.out.println("Se muestran las personas en la lista 1 (ListIterator):");
		// recorrido de la primera lista desde el final al inicio
		// recuperación de un ListIterator posicionado después
		// del último elemento (el número de elementos de la lista)

		ListIterator<Persona> lit;
		lit = lista1.listIterator(lista1.size());
		// mientras haya elementos
		while (lit.hasPrevious()) {
			// recuperación del elemento actual
			// remontando en la lista
			p = lit.previous();
			System.out.println(p.getNombre());
		}

		// cambio de un elemento de lista
		lista1.set(2, new Persona("Grant", "Cary", LocalDate.of(1904, 1, 18)));

		// visualizacion del elemento a la tercera posición de la lista
		System.out.println(lista1.get(2).getNombre());

		// búsqueda de un elemento en la lista
		int position;
		position = lista1.indexOf(p4);
		if (position == -1)
			System.out.println("no encontrado en la lista");
		else
			System.out.println(lista1.get(position).getNombre());

		// búsqueda de un elemento inexistente en la lista.
		// John Lennon seha cambiado por Cary Grant
		// La búsqueda se inicia al final de la lista

		position = lista1.lastIndexOf(p3);
		if (position == -1)
			System.out.println("no encontrado en la lista");
		else
			System.out.println(lista1.get(position).getNombre());
	}
}
