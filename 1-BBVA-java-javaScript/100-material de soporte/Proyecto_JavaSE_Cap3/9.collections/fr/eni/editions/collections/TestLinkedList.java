package fr.eni.editions.collections;

import java.time.LocalDate;
import java.util.LinkedList; 
 
public class TestLinkedList  
{ 
    public static void main(String[] args) 
    { 
        LinkedList<Persona> ll; 
        ll=new LinkedList<Persona>(); 
        // creación de las personas para rellenar el HashSet 
        Persona p1,p2,p3,p4,p5; 
        p1 = new Persona("Wayne", "John",LocalDate.of(1907,5,26)); 
        p2 = new Persona("McQueen", "Steeve",LocalDate.of(1930,3,24)); 
        p3 = new Persona("Lennon", "John",LocalDate.of(1940,10,9)); 
        p4 = new Persona("Gibson", "Mel",LocalDate.of(1956,1,3)); 
        p5 = new Persona("Willis", "Bruce",LocalDate.of(1955,3,19)); 
        // agregar los elementos en la lista 
        ll.addFirst(p1); 
        ll.addFirst(p2); 
        ll.addFirst(p3); 
        ll.addFirst(p4); 
        ll.addFirst(p5); 
        Persona p=null; 
        // extraction y eliminación de los elementos  
        // de la lista empezando por el más antiguo
        do  
        { 
            p=ll.pollLast(); 
            if (p!=null) 
                System.out.println(p.getNombre()); 
        } while(p!=null); 
         
        ll.clear();
    } 
}

