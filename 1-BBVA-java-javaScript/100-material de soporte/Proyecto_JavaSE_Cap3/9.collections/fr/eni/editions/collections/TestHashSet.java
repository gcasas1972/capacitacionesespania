package fr.eni.editions.collections;

import java.time.LocalDate; 
import java.util.ArrayList; 
import java.util.HashSet; 
import java.util.Iterator; 
import java.util.ListIterator;

public class TestHashSet {
	public static void main(String[] args) {
		HashSet<Persona> hash1;
		HashSet<Persona> hash2;
		// creación de las dos instancias
		hash1 = new HashSet<Persona>();
		hash2 = new HashSet<Persona>();

		// creación de las personas para rellenar el HashSet
		Persona p1, p2, p3, p4, p5;
		p1 = new Persona("Wayne", "John", LocalDate.of(1907, 5, 26));
		p2 = new Persona("McQueen", "Steeve", LocalDate.of(1930, 3, 24));
		p3 = new Persona("Lennon", "John", LocalDate.of(1940, 10, 9));
		p4 = new Persona("Gibson", "Mel", LocalDate.of(1956, 1, 3));
		p5 = new Persona("Willis", "Bruce", LocalDate.of(1955, 3, 19));

		// agregar cuatro personas al HashSet
		hash1.add(p1);
		hash1.add(p3);
		hash1.add(p4);
		hash1.add(p5);

		// agregar el contenido de un HashSet a otro HashSet
		// los dos HashSet contienen ahora los mismos 
		// objetos.
		// !!!! no confundir con hash2=hash1; !!!
		hash2.addAll(hash1);

		// visualizacion del número de elementos del HashSet
		System.out.println("hay " + hash1.size() + " persona(s) en el HashSet");

		System.out.println("Aquí las personas en la lista 1 (foreach):");
		// recorrido de la primera lista con el bucle foreach
		for (Persona persona : hash1) {
			System.out.println(persona.getNombre());
		}

		System.out.println("Se muestran las personas en la lista 1 (Iterator):");
		// recorrido del primer HashSet del inicio hasta el final
		Iterator<Persona> it;
		it = hash1.iterator();
		// mientras haya elementos en el HashSet
		Persona p;
		while (it.hasNext()) {
			// recuperación del elemento actual
			p = it.next();
			System.out.println(p.getNombre());
		}
	}
}
