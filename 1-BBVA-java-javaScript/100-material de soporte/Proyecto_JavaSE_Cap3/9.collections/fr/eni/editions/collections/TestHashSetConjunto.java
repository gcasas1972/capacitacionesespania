package fr.eni.editions.collections;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;

public class TestHashSetConjunto {
	public static void main(String[] args) {
		HashSet<Persona> actores;
		HashSet<Persona> cantantes;
		actores = new HashSet<Persona>();
		cantantes = new HashSet<Persona>();

		// creación de las personas para rellenar la lista
		Persona p1, p2, p3, p4, p5;
		p1 = new Persona("Wayne", "John", LocalDate.of(1907, 5, 26));
		p2 = new Persona("McQueen", "Steeve", LocalDate.of(1930, 3, 24));
		p3 = new Persona("Lennon", "John", LocalDate.of(1940, 10, 9));
		p4 = new Persona("Gibson", "Mel", LocalDate.of(1956, 1, 3));
		p5 = new Persona("Willis", "Bruce", LocalDate.of(1955, 3, 19));

		actores.add(p1);
		actores.add(p2);
		actores.add(p4);
		actores.add(p5);

		cantantes.add(p1);
		cantantes.add(p3);

		// test si los cantantes también son actores
		if (actores.containsAll(cantantes))
			System.out.println("todos los cantantes también son actores");
		else
			System.out.println("algunos cantantes no son también actores");
		System.out.println("******* los artistas *****************");
		// creación de un HashSet artistas con los cantantes
		// y actores
		HashSet<Persona> artistas;
		artistas = new HashSet<Persona>(cantantes);
		artistas.addAll(actores);
		// recorrido del primer HashSet de los artistas
		Iterator<Persona> it;
		it = artistas.iterator();
		// mientras haya  elementos en el HashSet
		Persona p;
		while (it.hasNext()) {
			// recuperación del elemento actual
			p = it.next();
			System.out.println(p.getNombre());
		}
		System.out.println("***** cantantes y actores *******************");
		// creación de un HashSet de las personas que son 
		// cantantes y actores

		HashSet<Persona> act_cant;
		act_cant = new HashSet<Persona>(cantantes);
		act_cant.retainAll(actores);
		it = act_cant.iterator();
		// mientras haya elementos en el HashSet
		while (it.hasNext()) {
			// recuperación del elemento actual
			p = it.next();
			System.out.println(p.getNombre());
		}
		System.out.println("***** cantantes solo *******************");
		// creación de un HashSet de las personas
		// solo actores
		HashSet<Persona> soloCantantes;
		soloCantantes = new HashSet<Persona>(cantantes);
		soloCantantes.removeAll(actores);
		for (Persona pe : soloCantantes) {
			System.out.println(pe.getNombre());
		}

		System.out.println("***** actores solo *******************");
		// creación de un HashSet de las personas
		// solo actores
		HashSet<Persona> soloActores;
		soloActores = new HashSet<Persona>(actores);
		soloActores.removeAll(cantantes);
		for (Persona pe : soloActores) {
			System.out.println(pe.getNombre());
		}

	}
}
