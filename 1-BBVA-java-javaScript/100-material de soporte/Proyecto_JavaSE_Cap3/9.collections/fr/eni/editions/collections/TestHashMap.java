package fr.eni.editions.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TestHashMap {

	public static void main(String[] args) {
		Map<Integer, String> departamentos = new HashMap<Integer, String>();
		departamentos.put(35, "Ille y Vilaine");
		departamentos.put(44, "Loira-Atlántica");
		departamentos.put(56, "Morbihan");
		departamentos.put(29, "Fin");
		//Correction por eliminación:
		departamentos.put(29, "Finisterre");
		departamentos.put(22,"Costa de Armor");
		//Delete de una entrada
		departamentos.remove(22);
		
		String dep35 = departamentos.get(35);
		System.out.println("El nombre del departamento 35 es : " + dep35);
		
		System.out.println("¿El departamento 29 tiene referencia?");
		//Búsqueda de un elemento por la clave
		if(departamentos.containsKey(29))
		{
			System.out.println("sí");
		}
		else
		{
			System.out.println("no");
		}
		
		System.out.println("¿El departamento Morbihan tiene referencia?");
		//Búsqueda de un elemento por el valor
		if(departamentos.containsValue("Morbihan"))
		{
			System.out.println("sí");
		}
		else
		{
			System.out.println("no");
		}
		
		System.out.println("Lista de las claves");
		//Recorrer la colección de claves (es un Set porque cada clave es única)
		for(int cle:departamentos.keySet())
		{
			System.out.println("-"+clave);
		}
		
		System.out.println("Lista de las valores");
		//Recorrer la colección de valores
		for(String valor:departamentos.values())
		{
			System.out.println("-"+valor);
		}
		
		System.out.println("Lista de las entradas clave/valor");
		//Recorrer la colección de clave/valor (es un Set porque cada clave es única)
		for(Entry<Integer, String> entrada:departamentos.entrySet())
		{
			System.out.println(entrada.getKey()+"="+entrada.getValue());
		}
	}
}
