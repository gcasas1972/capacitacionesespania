package fr.eni.editions.clases_internas;

public class Externa {
	final double IMPUESTO=1.2;
	class Interna
	{
		public double calculoIVA(double precio)
		{
			return precio*IMPUESTO;
		}
	}
}
