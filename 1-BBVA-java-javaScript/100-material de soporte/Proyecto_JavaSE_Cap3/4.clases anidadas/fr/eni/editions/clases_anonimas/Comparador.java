package fr.eni.editions.clases_anonymes;

public interface Comparateur 
{ 
  /**
   * este método se podrá llamar para comparar los dos
   * objetos recibidos como parámetro
   * el método devuelve un entero cuyo valor depende
   * de las reglas siguientes
   * @return
   * 1 si la instancia o1 es superior a o2
   * 0 si las dos instancias son iguales
   * -1 si la instancia o1 es inferior a o2
   * -99 si la comparación es imposible 
   */
  int compare(Object o1,Object o2); 
   
  public static final int INFERIOR=-1; 
  public static final int IGUAL=0; 
  public static final int SUPERIOR=1; 
  public static final int ERROR=-99; 
   
}

