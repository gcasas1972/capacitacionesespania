package fr.eni.editions.clases_anonymes;

import java.time.LocalDate;
import java.util.Arrays;

public class TestComparador {

public static void main(String[] args) {
	Persona[] tab = new Persona[5];
	tab[0] = new Persona("toto2", 
						  "apellido2", 
						  LocalDate.of(1922, 2, 15));
	tab[1] = new Persona("toto1", 
						  "apellido1 ", 
						  LocalDate.of(1911, 1, 15));
	tab[2] = new Persona("toto5", 
						  "apellido5 ", 
						  LocalDate.of(1955, 05, 15));
	tab[3] = new Persona("toto3", 
						  "apellido3 ", 
						  LocalDate.of(1933, 03, 15));
	tab[4] = new Persona("toto4", 
						  "apellido4 ", 
						  LocalDate.of(1944, 04, 15));

	System.out.println("Ordenación por  nombre:");
	Persona[] tabOrdenada = (Persona[]) tri(tab,
			// creación de una instancia de clase que implementa 
			// la interfaz Comparador
			new Comparador()
			// El code de la clase
			{
				// como exige la interfaz este es el método compare
				public int compare(Object o1, Object o2) {
					Persona p1, p2;
					if (o1 instanceof Persona & o2 instanceof Persona) {
						p1 = (Persona) o1;
						p2 = (Persona) o2;
					} else {
						return Comparador.ERROR;
					}
					if (p1.getNombre().compareTo(p2.getNombre()) < 0) {
						return Comparador.INFERIOR;
					}
					if (p1.getNombre().compareTo(p2.getNombre()) > 0) {
						return Comparador.SUPERIOR;
					}

					return Comparador.IGUAL;

				} // llave de cierre del método compare

			} // llave de cierre de la clase
	); // fin de la llamada de la función de ordenación

	// visualizacion de la tabla ordenada

	for (int i = 0; i < tabOrdenada.length; i++) {
		System.out.println(tabOrdenada[i]);
	}

	System.out.println("Ordenación por edad:");
	tabOrdenada = (Persona[]) tri(tab,
			// creación de una instancia de clase que implementa
			// la interfaz Comparador
			new Comparador()
			// el code de la clase
			{
				// como la exige la interfaz este es el método compare
				public int compare(Object o1, Object o2) {
					Persona p1, p2;
					if (o1 instanceof Persona & o2 instanceof Persona) {
						p1 = (Persona) o1;
						p2 = (Persona) o2;
					} else {
						return Comparador.ERROR;
					}
					if (p1.calcularEdad()<p2.calcularEdad()) {
						return Comparador.INFERIOR;
					}
					if (p1.calcularEdad()>p2.calcularEdad()) {
						return Comparador.SUPERIOR;
					}
				
					return Comparador.IGUAL;
				
				} // llave de cierre del método compare

			} // llave de cierre de la clase
	); // fin de la llamada de la función de tri

	// visualizacion de la tabla ordenada

	for (int i = 0; i < tabOrdenada.length; i++) {
		System.out.println(tabOrdenada[i]);
	}
}

	public static Object[] tri(Object[] tabla, Comparador trieur) {
		int i, j;
		Object c;
		Object[] tablaOrden;
		tablaOrdenación = Arrays.copyOf(tabla, tabla.length);
		for (i = 0; i < tablaOrden.length; i++) {
			for (j = i + 1; j < tablaOrden.length; j++) {
				// utilice la función compare del objeto recibido como parámetro
				// para comparar el contenido de dos casillas de la tabla
				if (trieur.compare(tablaOrden[j], tablaOrden[i]) == Comparador.INFERIOR) {
					c = tablaOrden[j];
					tablaOrden[j] = tablaOrden[i];
					tablaOrden[i] = c;
				} else if (trieur.compare(tablaOrden[j], tablaOrden[i]) == Comparador.ERROR) {
					return null;
				}
			}
		}
		return tablaOrden;
	}
}
