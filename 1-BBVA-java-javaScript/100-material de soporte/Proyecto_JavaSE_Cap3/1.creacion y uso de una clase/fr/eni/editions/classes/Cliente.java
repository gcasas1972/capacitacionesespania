package fr.eni.editions.clases;

public class Cliente extends Persona {
	//La firma dede ser idéntica a un método de la clase madre
	//Si no es el caso, hay un error de compilación
	@Override
	public int calcularEdad() {
		return 0;
	}
	
	//El método siguiente no compila con @Override porque no respeta pas la firma de un método de la clase madre.
	//Para que compile, hay que comentar la anotación. El método se hace también una sobrecargar.
	//@Override 
    public int calcularEdad(int unite) 
    {
		return 0;
    }
}
