package fr.eni.editions.clases;

import java.time.LocalDate;

public class GestionMemoria 
{ 
    public static void main(String[] args) throws InterruptedException 
    { 
        double total; 
        double resta; 
        double porcentaje; 
        double restaAnterior=-1;   
        for (int j=0;j<1000;j++) 
        { 
            creacionTabla(); 
            total=Runtime.getRuntime().totalMemory(); 
            resta=Runtime.getRuntime().freeMemory();
            if(resta>resteAnterior && restaAnterior!=-1)
            {
            	System.out.println("La papelera ha pasado");
            }
            restaAnterior=reste;
            porcentaje=100-(resta/total)*100; 
            System.out.println("creación de la " + j + "enésima tabla memoria llena a : " + porcentaje + "%" ); 
            // una pequeña pausa para poder leer los mensajes
            Thread.sleep(100); 
        } 
    } 
 
    public static void creacionTabla() 
    { 
        // creación de un tabla de 1000 Personas en una 
        // variable local
        // al final de esta función los elementos de la tabla no 
        // son más accessibles y se pueden eliminar
        // de la memoria 
        Persona[] tabla; 
        tabla=new Persona[1000]; 
        for (int i=0;i<1000;i++) 
        { 
             tabla[i]=new Persona("Dupont",
                                   "albert",
                                   LocalDate.of(1956,12,13)); 
        } 
    }
}

