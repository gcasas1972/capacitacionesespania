package fr.eni.editions.clases;

import java.time.LocalDate;

public class TestPersona {
	@SuppressWarnings("deprecation") 
	public static void main(String[] args) {
		Persona p = new Persona();
		p.setNombre("dupont"); 
		p.setApellido("albert"); 
		
		System.out.println("Visualización de las características de la persona");
		System.out.println(p.getNombre()); 
		System.out.println(p.getApellido());
		System.out.println("Numéro : " + p.getNumero());
		
		System.out.println("Visualización de la persona con el método depreciado mostrar()");
		p.mostrar();
		
		System.out.println("Visualización de la persona con el método mostrar(true)");
		p.mostrar(true);
		
		System.out.println("Visualización de la persona con el método mostrar(false)");
		p.mostrar(false);
		
		
		Persona pe=new Persona("Martin",
                				 "Benoit", 
                				 LocalDate.of(1975,2,5));
		System.out.println("Visualización de la persona con una fecha de nacimiento");
		pe.mostrar(true);
		System.out.println("Visualización del número de la persona");
		System.out.println("Numéro : " + pe.getNumero());
	}
	
}
