package fr.eni.editions.herencia;

public class TestGetClass {

	public static void main(String[] args) {
		Cliente c = new Client();
		infoClasse(c);
	}
	
public static void infoClasse(Object o) 
{ 
     Class c; 
     c=o.getClass(); 
     System.out.println("nombre de la clase : " + c.getName()); 
     System.out.println("está en el paquete : " +  
    		 			c.getPackage().getName()); 
     System.out.println("hereda de la clase  : " +  
    		 			c.getSuperclass().getName()); 
     System.out.println("tiene los campos : "); 
     for (int i=0;i<c.getFields().length;i++) 
     { 
          System.out.print("\t" + c.getFields()[i].getName()); 
          System.out.println(" de tipo :" +  
        		  		c.getFields()[i].getType().getName()); 
     } 
     System.out.println("tiene los métodos : "); 
     for (int i=0;i<c.getMethods().length;i++) 
     { 
          System.out.print("\t" + c.getMethods()[i].getName()); 
          System.out.print(" que espera como parámetro ("); 
          for (int j=0;j<c.getMethods()[i]
        		          .getParameterTypes()
        		          .length;j++) 
          {
        	  System.out.print(c.getMethods()[i]
        			            .getParameterTypes()[j]+ " "); 
          } 
          System.out.println(")"); 
     } 
}


}
