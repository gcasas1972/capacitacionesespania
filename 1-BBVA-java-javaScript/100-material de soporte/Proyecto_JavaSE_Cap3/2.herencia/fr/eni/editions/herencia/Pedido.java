package fr.eni.editions.herencia;

public class Comando implements Cloneable 
{ 
     Cliente elClient; 
     LineasDeComando lasLineas; 
 
     public Comando() 
     { 
          super(); 
          lasLineas=new LineasDeComando(); 
 
     } 
     public Object clone() throws CloneNotSupportedException 
     { 
          Comando cmd; 
          // creación de una copia del comando 
          cmd=(Comando)super.clone(); 
          // duplicación de las líneas del comando 
          cmd.lasLineas=(LineasDeComando)lasLineas.clone(); 
          return cmd; 
           
     } 
     public Cliente getElCliente() 
     { 
          return elClient; 
     } 
     public void setElCliente(Cliente elClient) 
     { 
          this.elCliente = elClient; 
     } 
     public LineasDeComando getLasLineas() 
     { 
          return lasLineas; 
     } 
     public void setLasLineas(LineasDeComando lasLineas) 
     { 
          this.lasLineas = lasLineas; 
     } 
}

