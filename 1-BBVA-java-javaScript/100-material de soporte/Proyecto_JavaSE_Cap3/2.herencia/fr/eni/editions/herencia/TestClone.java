package fr.eni.editions.herencia;

import java.time.LocalDate;

public class TestClone {

	public static void main(String[] args) throws CloneNotSupportedException{
		Cliente c; 
		c=new Client("ENI","",LocalDate.of(1981,05,15),TypeClient.EMPRESA); 
		Comando cmd1,cmd2; 
		// creación y inicialización de un comando 
		cmd1=new Comando(); 
		cmd1.setElCliente(c); 
		System.out.println("hashCode del comando : " + cmd1.hashCode()); 
		System.out.println("hashCode del Cliente : " + cmd1.getElCliente().hashCode()); 
		System.out.println("hashCode de las líneas : " 
		+cmd1.getLasLineas().hashCode()); 
		cmd2=(Comando)cmd1.clone(); 
		System.out.println("hashCode de la copia  : " +cmd2.hashCode()); 
		System.out.println("hashCode del Cliente de la copia: " + cmd2.getElCliente().hashCode()); 
		System.out.println("hashCode de las líneas de la copia: " + cmd2.getLasLineas().hashCode());
	}

}
