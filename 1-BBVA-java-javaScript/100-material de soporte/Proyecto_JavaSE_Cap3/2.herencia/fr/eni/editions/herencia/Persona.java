package fr.eni.editions.herencia;

import java.time.LocalDate;
import java.util.Random;

public class Persona {
	
//Variables miembros
	private String nombre; 
    private String apellido; 
    private LocalDate fechaNacimiento;
    //campo privado que representa el número de la Persona 
    private int numero; 
    //campo estático privado que representa el contador de Personas 
    private static int numInstancias/*=initContador()*/; 

//Bloque estático ejecutado al cambio de la clase
    static
    {
    	Random rd = new Random();
    	numInstancias = rd.nextInt(1000);
    }
//Aceso / Getters/Setters
    public String getNombre() 
    { 
        return nombre; 
    } 
 
    public void setNombre(String n) 
    { 
        if(n!=null)
        {
            nombre = n.toUpperCase(); 
        }
    } 
 
    public String getApellido() 
    { 
        return apellido; 
    } 
 
    public void setApellido(String p) 
    { 
        if(p!=null)
        {
            apellido = p.toLowerCase(); 
        }
    }
    
    public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	// método de instancia que permita obtener el número de una Persona 
    public int getNumero() 
    { 
         return numero; 
    } 
    // método estático que permita obtener el número de instancias creadas 
    public static int getNbInstances() 
    { 
         return numInstancias; 
    }

//Constructores
    public Persona() 
    { 
        setNombre(""); 
        setApellido(""); 
        fechaNacimiento=null;
        // creación de una nueva Persona incrementando el contador 
        numInstancias++; 
        // asignación a la nueva Persona de su número 
        numero=numInstancias; 
    } 
    public Persona(String n, String p, LocalDate d) 
    {   
        setNombre(n);
        setApellido(p);
        fechaNacimiento=d; 
        // creación de una nueva Persona incrementando el contador 
        numInstancias++; 
        // asignación a la nueva Persona de su número 
        numero=numInstancias; 
    }
    //Método estático para initializar el contador
    //Se puede usar sustituyendo el bloque estático
    private static int initContador()
    { 
         int cpt=0; 
         Random rd = new Random();
         cpt=numInstancias = rd.nextInt(1000);
         return cpt; 
    }

//Destructor
    protected void finalize() throws Throwable 
    { 
     
    }
    
    
//Métodos
    public int calcularEdad() 
    { 
    	if(fechaNacimiento!=null)
    	{
    		return fechaNacimiento.until(LocalDate.now()).getYears();
    	}
    	return 0;
    } 
 
    //@Deprecated
    public void mostrar() 
    { 
        System.out.println("nombre : " + nombre); 
        System.out.println("apellido : " + apellido); 
        System.out.println("edad : " + calcularEdad()); 
    } 
    
    //Sobrecargar el método mostrar()
    public void mostrar(boolean espagnol) 
    { 
        if (espagnol) 
        { 
            System.out.println("nombre : " + nombre); 
            System.out.println("apellido : " + apellido); 
            System.out.println("edad : " + calcularEdad()); 
        } 
        else 
        { 
            System.out.println("name : " + nombre); 
            System.out.println("first name : " + apellido); 
            System.out.println("age : " + calcularEdad()); 
        } 
    }
    //El método siguiente no compila porque hay ya un método con esta firma : mostrar(boolean)
    /*public void mostrar(boolean mayuscula) 
    { 
        if (mayuscula) 
        { 
            System.out.println("nombre : " + nombre.toUpperCase()); 
            System.out.println("apellido : " + apellido.toUpperCase()); 
            System.out.println("edad : " + calcularEdad()); 
        } 
        else 
        { 
            System.out.println("nombre : " + nombre.toLowerCase()); 
            System.out.println("apellido : " + apellido.toLowerCase()); 
            System.out.println("edad : " + calcularEdad()); 
        } 
    }*/

    public void mostrar(String...colors) 
    { 
        { 
            if (colors==null) 
            { 
                System.out.println("sin color"); 
                return; 
            } 
            switch (colors.length) 
            { 
                case 1: 
                    System.out.println("una color"); 
                    break; 
                case 2: 
                    System.out.println("dos colors"); 
                    break; 
                case 3: 
                    System.out.println("tres colors"); 
                    break; 
                default : 
                    System.out.println("más de tres colores"); 
            } 
        } 
    }
    
	@Override
	public String toString() 
	{ 
	     String cadena; 
	     cadena="nombre : " + this.getNombre()+ "\r\n"; 
	     cadena=cadena + "apellido : " + this.getApellido(); 
	     return cadena; 
	}


}
