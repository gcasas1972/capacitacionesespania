package fr.eni.editions.herencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;


public class TestCliente {

	public static void main(String[] args) throws FileNotFoundException {
		Cliente c; 
		c=new Client(); 
		c.setNombre("ENI"); 
		c.setApellido(""); 
		c.setFechaNacimiento(LocalDate.of(1981,05,15)); 
		c.setType(TypeClient.EMPRESA); 
		c.mostrar();
		
		Cliente c2 = new Client("ENI", "", LocalDate.of(1981,05,15), TypeClient.EMPRESA);
		c2.mostrar();
		FileInputStream fis = new FileInputStream("ficheroDesconocido.txt");
	}

}
