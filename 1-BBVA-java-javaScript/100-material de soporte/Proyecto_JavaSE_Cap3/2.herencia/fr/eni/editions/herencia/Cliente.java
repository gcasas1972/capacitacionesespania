package fr.eni.editions.herencia;

import java.time.LocalDate;

public class Cliente extends Persona {
//Variables miembros
	private TipoCliente type;

//Acceso	
	public TipoCliente getType() {
		return type;
	}

	public void setType(TipoCliente t) {
		type = t;
	}

//Constructores
	public Client() {
		super();
	}
	public Client(String nombre, String apellido, LocalDate fechaNacimiento, TipoCliente type) {
		super(nombre, apellido, fechaNacimiento);
		this.type = type;
	}
	
//Métodos	
	@Override
	public void mostrar() {
		/*System.out.println("nombre : " + getNombre()); 
        System.out.println("apellido : " + getApellido()); 
        System.out.println("edad : " + calcularEdad());*/
		//mostrar();//StackOverflowError
		super.mostrar();
	    switch (type) 
	    { 
	    	case PARTICULAR: 
	            System.out.println("tipo de cliente : Particular"); 
	            break; 
	    	case EMPRESA: 
	    		System.out.println("tipo de cliente : Empresa"); 
	    		break; 
	    	case ADMINISTRACION: 
	    		System.out.println("tipo de cliente : Administración"); 
	    		break; 
	     } 
	}
	
	@Override
	public boolean equals(Object obj) 
	{ 
	     Cliente c; 
	     // verificación si obj es null o referencia a una instancia 
	     // de otra clase 
	     if (obj ==null || obj.getClass()!=this.getClass()) 
	     { 
	          return false; 
	     } 
	     else        
	     { 
	          c=(Client)obj; 
	          // verificación de los criterios de igualdad en 
	          // - el nombre 
	          // - el apellido 
	          // - la fecha de nacimiento 
	          // - el tipo de cliente 
	          if (c.getNombre().equals(this.getNombre())&& 
	                    c.getApellido().equals(this.getApellido()) && 
	                    c.getFechaNacimiento().equals(this.getFechaNacimiento()) && 
	                    c.getType().equals(this.getType())) 
	          { 
	               return true; 
	          } 
	          else 
	          { 
	               return false; 
	          } 
	     } 
	}
	
	public int hashCode() 
	{ 
	     return this.getNombre().hashCode()+ 
	            this.getApellido().hashCode() + 
	            this.getFechaNacimiento().hashCode()+ 
	            this.getType().hashCode(); 
	}


}





































