package fr.eni.editions.herencia;

import java.time.LocalDate;

public class TestToString {
	public static void main(String[] args) {
		Cliente c; 
		c=new Client("ENI",
				     "",
				     LocalDate.of(1981,05,15),
				     TypeClient.EMPRESA); 
		//El método println recibe un objeto como parámetro
		//prueba que es no null el objeto antes de llamar
		//al método toString de este objeto
		System.out.println(c);
		//El método println siguiente muestra la cadena
		//de caracteres que se pasa como parámetro
		System.out.println(c.toString());
	}
}
