package fr.eni.editions.herencia;

public abstract /*final*/ class EstarVivo 
{ 
     private double tamanio; 
     private double peso; 
     public double getTamanio() 
     { 
          return tamanio; 
     } 
     public void setTamanio(double tamanio) 
     { 
          this.tamanio = tamanio; 
     } 
     public double getPeso() 
     { 
          return peso; 
     } 
     public void setPeso(double peso) 
     { 
          this.peso = peso; 
     } 
     // este método se deberá implementar en las clases derivadas 
     protected abstract void Moverse(); 
}

