package fr.eni.editions.herencia;

public enum TipoCliente {
	PARTICULAR,
	EMPRESA,
	ADMINISTRACION;
}
