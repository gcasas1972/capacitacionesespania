package fr.eni.editions.herencia;

import java.time.LocalDate;

public class TestEquals {
	public static void main(String[] args) {
		Cliente c1,c2; 
		c1=new Client("ENI",
				      "",
				      LocalDate.of(1981,05,15),
				      TypeClient.EMPRESA); 
		c2=new Client("ENI",
				      "",
				      LocalDate.of(1981,05,15),
				      TypeClient.EMPRESA); 
		if (c1.equals(c2)) 
		{ 
		     System.out.println("los dos clientes son idénticos"); 
		} 
		else 
		{ 
		     System.out.println("los dos clientes son diferentes"); 
		}
	}
}
