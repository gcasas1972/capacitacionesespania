package fr.eni.editions.exceptions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestExceptions {

	public static void main(String[] args) {
		leerFichero("unFicheroDesconocido.txt");

	}

	/*public static void leerFichero(String nombre) {
		FileInputStream fichero = null;
		BufferedReader br = null;
		String linea = null;
		try {
			fichero = new FileInputStream(nombre);
			br = new BufferedReader(new InputStreamReader(fichero));
			linea = br.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = br.readLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("El fichero n'a pas été trouvé");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/

	public static void leerFichero(String nombre) 
	{
		FileInputStream fichero = null;
		BufferedReader br = null;
		String linea = null;
		double suma = 0;
	
		try 
		{
			fichero = new FileInputStream(nombre);
			br = new BufferedReader(new InputStreamReader(fichero));
			linea = br.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = br.readLine();
				suma = suma + Double.parseDouble(linea);
			}
			System.out.println("total : " + suma);
		}
		catch (IOException | NumberFormatException e) 
		{
			e.printStackTrace();
		}
	}

}
