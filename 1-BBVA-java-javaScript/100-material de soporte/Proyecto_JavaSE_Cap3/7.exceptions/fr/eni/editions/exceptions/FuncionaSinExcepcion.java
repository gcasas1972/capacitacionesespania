package fr.eni.editions.exceptions;

import java.util.ArrayList;

public class NoFuncionaException extends Exception {
	public NoFuncionaException()
	{
		super();
	}
	public NoFuncionaException(String message)
	{
		super(message);
	}
	public NoFuncionaException(Throwable cause)
	{
		super(cause);
	}
	public NoFuncionaException(String message, Throwable cause)
	{
		super(message, cause);
	}	
}
