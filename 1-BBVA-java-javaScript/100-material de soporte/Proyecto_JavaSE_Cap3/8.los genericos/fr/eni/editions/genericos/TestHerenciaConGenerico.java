package fr.eni.editions.genericas;

import java.time.LocalDate;

public class TestHerenciaConGenerico {

	public static void main(String[] args) throws Exception {
		//ListaGenerica<Client> listaCliente = new ListaGenerica<Client>(5);
		//La l�nea siguiente no compila
		//ListaGenerica<Persona> listaPersona = listaCliente;
		
		ListaGenerica<Persona> listaPersonas = new ListaGenerica<>(5);
		
		listaPersonas.agregar(new Persona("toto2", "apellido2", LocalDate.of(1922, 2, 15)));
		listaPersonas.agregar(new Persona("toto1", "apellido1", LocalDate.of(1911, 1, 15)));
		listaPersonas.agregar(new Persona("toto5", "apellido5", LocalDate.of(1955, 5, 15)));
		listaPersonas.agregar(new Persona("toto3", "apellido3", LocalDate.of(1933, 3, 15)));
		listaPersonas.agregar(new Persona("toto4", "apellido4", LocalDate.of(1944, 4, 15)));
		
		visualizacion(listaPersonas);
		
		ListaGenerica<Client> listaClientes = new ListaGenerica<>(5);
		
		listaClientes.agregar(new Client("toto6", "apellido6", LocalDate.of(19666, 6, 15)));
		listaClientes.agregar(new Client("toto7", "apellido7", LocalDate.of(1977, 7, 15)));
		
		visualizacion(listaClientes);
	}
	
	public static void visualizacion(
			ListaGenerica<? extends Persona> lista) throws Exception 
	{
		lista.primer();
		for (int i = 0; i < lista.getNumElementos(); i++) {
			System.out.println(lista.getElemento(i).getNombre());
			System.out.println(lista.getElemento(i).getApellido());
			System.out.println("-----------------------------");
		}
	}

}















