package fr.eni.editions.genericas;

import java.util.ArrayList; 
public class ListaGenerica <E> 
{ 
	// para almacenar los elementos de la lista 
    private ArrayList<E> lista; 
    // puntero de posición en la lista 
    private int position; 
    // número de elementos de la lista 
    private int numElementos; 
    // constructor con un parámetro que permita dimensionar 
    // la lista 
    public ListaGenerica(int tamanio) 
    { 
     lista=new ArrayList<E>(tamanio); 
    } 
    public void agregar(E element) 
    { 
        lista.add(element); 
        numElementos = numElementos + 1; 
    } 

    public void insert(E element,int index) 
    { 
        // se verifica si el índice no es superior al número 
        // de elementos o si el índice no es inferior a 0 
        if (index >= numElementos || index < 0) 
        { 
           return; 
        } 
        lista.add(index,element); 
        // se actualiza el número de elementos 
        numElementos = numElementos + 1; 
    } 
     
    public void sustituye(E element,int index) 
    { 
        // se verifica si el índice no es superior al número 
        // de elementos o si el índice no es inferior a 0 
        if (index >= numElementos || index < 0) 
        { 
           return; 
        } 
        lista.set(index,element); 
    } 
    public void elimina(int index) 
    { 
        int i; 
        // se verifica si el índice no es superior al número 
        // de elementos o si el índice no es inferior a 0 
        if (index >= numElementos || index < 0) 
        { 
           return; 
        } 
        lista.remove(index); 
        // se actualiza el número de elementos 
        numElementos = numElementos - 1; 
    } 
    public E getElemento(int j)  
    { 
       return lista.get(j); 
    } 
    public int getNumElementos() 
    { 
        return numElementos; 
    } 
    public E primer() throws Exception 
    { 
        if (numElementos == 0) 
        { 
            throw new Exception("lista vacía"); 
        } 
        // movemos el puntero al primer elemento 
        position = 0; 
        return lista.get(0); 
    } 
    public E ultimo() throws Exception 
    { 
        if (numElementos == 0) 
        { 
            throw new Exception("lista vacía"); 
        } 
        // movemos el puntero en el último elemento 
        position = numElementos - 1; 
        return lista.get(position); 
    } 
    public E siguiente() throws Exception 
    { 
        if (numElementos == 0) 
        { 
            throw new Exception("lista vacía"); 
        } 
        // se verifica si estamos al final de la lista 
        if (position == numElementos - 1) 
        { 
            throw new Exception("no hay elemento siguiente"); 
        } 
        // movemos el puntero al elemento siguiente 
        position = position + 1; 
        return lista.get(position); 
    } 
    public E anterior() throws Exception 
    { 
        if (numElementos == 0) 
        { 
            throw new Exception("lista vacía"); 
        } 
        // se verifica si estamos en el primer elemento 
        if (position == 0) 
        { 
            throw new Exception("sin elemento anterior"); 
        } 
        // nos movemos al elemento anterior
        position = position - 1; 
        return lista.get(position); 
    } 
}

