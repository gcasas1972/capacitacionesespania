package fr.eni.editions.genericas;

import java.time.LocalDate;

public class Cliente extends Persona {

	public Client() {
		super();
	}

	public Client(String n, String p, LocalDate d) {
		super(n, p, d);
	}

}
