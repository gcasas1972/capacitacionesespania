package fr.eni.editions.genericas;
/**
 * esta interface se deberá implementar por los clases 
 * para para las que se prevé una clasificación de las instancias
 */
public interface Classable 
{
	/**
	 * este método se podrá llamar para comparar la instancia
	 * actual con la que se recibe como parámetro
	 * el método devuelve un entero cuyo valor depende
	 * de las reglas siguientes
	 * 1 si la instancia actual est superior a la recibida
	 * en parámetro
	 * 0 si las dos instances son iguales
	 * -1 si la instancia actual est inferior a la recibida 
	 * en parámetro 
	 * -99 si la comparación es imposible 
	 */
  	int compara(Object o); 
  	default boolean isInferior(Object o)
  	{
  		return false;
  	}
    default boolean isSuperior(Object o)
    {
    	return false;
    }

   
  	public static final int INFERIOR=-1; 
  	public static final int IGUAL=0; 
  	public static final int SUPERIOR=1; 
  	public static final int ERROR=-99; 
}


