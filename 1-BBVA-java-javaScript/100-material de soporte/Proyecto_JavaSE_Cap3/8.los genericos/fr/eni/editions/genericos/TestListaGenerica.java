package fr.eni.editions.genericas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;

public class TestListaGenerica {

	// static ListaGenerica<String> lista = new ListaGenerica<String>(5);
	static ListaGenerica<Persona> lista = new ListaGenerica<Persona>(5);

	public static void main(String[] args) {
		/*
		 * lista.agregar("primer"); lista.agregar("segunda"); lista.agregar("tercera");
		 * lista.agregar("cuarta"); lista.agregar("quinta");
		 */
		
		lista.agregar(new Persona("toto2", "apellido2", LocalDate.of(1922, 2, 15)));
		lista.agregar(new Persona("toto1", "apellido1", LocalDate.of(1911, 1, 15)));
		lista.agregar(new Persona("toto5", "apellido5", LocalDate.of(1955, 5, 15)));
		lista.agregar(new Persona("toto3", "apellido3", LocalDate.of(1933, 3, 15)));
		lista.agregar(new Persona("toto4", "apellido4", LocalDate.of(1944, 4, 15)));
		
		try {
			//Llamada de un método genérico para realizar la ordenación
			//TestListaGenerica.tri(lista);
			//TestListaGenerica.<Persona>tri(lista);
			menu();		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void menu() {
		char opcion = '\0';
		System.out.println("p (primero) < (anterior) >(siguiente) d (ultimo) f (fin)");
		while (opcion != 'f') {
			try {
				BufferedReader br;
				br = new BufferedReader(new InputStreamReader(System.in));
				opcion = br.readLine().charAt(0);
				switch (opcion) {
				case 'p':
					System.out.println("el primer: " + lista.primer());
					break;
				case '<':
					System.out.println("el anterior: " + lista.anterior());
					break;
				case '>':
					System.out.println("el siguiente: " + lista.siguiente());
					break;
				case 'd':
					System.out.println("el último: " + lista.ultimo());
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println("p (primero) < (anterior) >(siguiente) d (ultimo) f (fin)");
		}
	}

	//Método genérico
	public static <T extends Classable> void tri(ListaGenerica<T> lista) 
			throws Exception 
	{
		int i, j;
		T c;
		for (i = 0; i < lista.getNumElementos() - 1; i++) {
			for (j = i + 1; j < lista.getNumElementos(); j++) {
				if (lista.getElemento(j).compare(lista.getElemento(i)) == 
						Classable.INFERIOR) {
					c = lista.getElemento(j);
					lista.sustituye(lista.getElemento(i), j);
					lista.sustituye(c, i);
				} else if (lista.getElemento(j).compare(lista.getElemento(i)) == 
						Classable.ERROR) {
					throw new Exception("error durante la ordenación");
				}
			}
		}
	}

}
