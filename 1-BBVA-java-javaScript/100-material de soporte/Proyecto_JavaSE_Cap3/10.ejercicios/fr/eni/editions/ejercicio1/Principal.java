package fr.eni.editions.ejercicio1;

public class Principal {
	public static void main(String[] args) {
		Articulo a1, a2;
		a1 = new Articulo();
		a1.setReferencia(100);
		a1.setTitulo("tintin en el congo");
		a1.setPrecio(8.5);

		a2 = new Articulo(110, "El cangrejo de las pinzas de oro", 8.5);

		test(a1);
		System.out.println(a1.toString());

		test(a2);
		System.out.println(a2.toString());

	}

	public static void test(Articulo a) {
		System.out.println("referencia : " + a.getReferencia());
		System.out.println("título : " + a.getTitulo());
		System.out.println("precio : " + a.getPrecio() + " €");
	}
}
