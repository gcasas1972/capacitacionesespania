package fr.eni.editions.ejercicio3;

import java.time.Duration; 
import java.time.LocalDate;

public class Principal {
	public static void main(String[] args) {
		Libro l;
		Dvd d;
		l = new Libro();
		l.setReferencia(100);
		l.setTitulo("El cangrejo de las pinzas de oro");
		l.setPrecio(8.5);
		l.setNumPaginas(86);
		l.setAutor(new Persona("Hergé", "Georges", LocalDate.of(1907, 05, 22)));
		testLibro(l);
		System.out.println(l.toString());

		d = new Dvd();
		d.setReferencia(110);
		d.setTitulo("La sopa de repollo");
		d.setPrecio(19.50);
		d.setDuracion(Duration.ofMinutes(98));
		d.setEscritor(new Persona("Girault", "jean", LocalDate.of(1924, 05, 9)));
		testDvd(d);
		System.out.println(d.toString());

	}

	public static void test(Articulo a) {
		System.out.println("referencia : " + a.getReferencia());
		System.out.println("título : " + a.getTitulo());
		System.out.println("precio : " + a.getPrecio() + " €");
	}

	public static void testLibro(Libro l) {
		test(l);
		System.out.println("número de páginas : " + l.getNumPaginas());
		System.out.println("autor : " + l.getAutor().toString());
	}

	public static void testDvd(Dvd d) {
		test(d);
		System.out.println("duración : " + d.getDuracion().toMinutes() + " minutos");
		System.out.println("escritor : " + d.getEscritor().toString());
	}
 }
