package fr.eni.editions.ejercicio3;

import java.time.LocalDate; 

public class Persona {
	private String nombre;
	private String apellido;
	private LocalDate fecha_nacimiento;

	public Persona() {
		super();
	}

	public Persona(String n, String p, LocalDate d) {
		this.nombre = n;
		this.apellido = p;
		this.fecha_nacimiento = d;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String toString() {
		return apellido + " " + nombre;
	}

 } 
