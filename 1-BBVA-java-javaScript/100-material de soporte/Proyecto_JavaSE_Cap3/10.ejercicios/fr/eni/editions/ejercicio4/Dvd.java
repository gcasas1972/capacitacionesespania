package fr.eni.editions.ejercicio4;

import java.time.Duration; 
import java.util.ArrayList; 
 
 
public class Dvd extends Articulo 
{ 
     private Duracion duracion; 
     private Persona escritor; 
      
     public Dvd() 
     { 
         super(); 
     } 
      
     public Dvd(int referencia,String titulo,double 
precio,Duracion duracion,Persona escritor) 
     { 
         super(referencia,titulo,precio); 
      
         setDuracion(duracion); 
         setEscritor(escritor); 
     } 
  
     public Duracion getDuracion()  
     { 
         return duracion; 
     } 
  
     public void setDuracion(Duracion duracion)  
     { 
         this.duracion = duracion; 
     } 
  
     public Persona getEscritor()  
     { 
         return escritor; 
     } 
  
     public void setEscritor(Persona escritor)  
     { 
         //si el libro pertenece a otro escritor
         //se elimina de la lista de este escritor
         if(this.escritor!=null)
         {
              this.escritor.getTrabajos().remove(this);
         }
         this.escritor = escritor; 
         ArrayList<Articulo> lst; 
         lst=escritor.getTrabajos(); 
         if (!lst.contains(this)) 
         { 
             lst.add(this); 
         } 
     } 
  
   public String toString() 
   { 
         return super.toString() + " " + 
getDuracion().toMinutes() + " " + getEscritor(); 
	}
}
