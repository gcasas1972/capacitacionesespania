package fr.eni.editions.ejercicio4;

import java.time.LocalDate;
import java.util.ArrayList;

public class Persona {
	private String nombre;
	private String apellido;
	private LocalDate fecha_nacimiento;
	private ArrayList<Articulo> trabajos;

	public Persona() {
		super();
		trabajos = new ArrayList<>();
	}

	public Persona(String n, String p, LocalDate d) {
		this();
		this.nombre = n;
		this.apellido = p;
		this.fecha_nacimiento = d;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public ArrayList<Articulo> getTrabajos() {
		return trabajos;
	}

	public String toString() {
		return apellido + " " + nombre;
	}
}

