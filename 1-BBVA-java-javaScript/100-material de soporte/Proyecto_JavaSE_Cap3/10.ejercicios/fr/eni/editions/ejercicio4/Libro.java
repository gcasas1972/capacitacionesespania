package fr.eni.editions.ejercicio4;

import java.util.ArrayList;

public class Libro extends Articulo {
	private String isbn;
	private int numPaginas;
	private Persona autor;

	public Libro() {
		super();
	}

	public Libro(int referencia, String titulo, double precio, String isbn, int numPaginas, Persona autor) {
		super(referencia, titulo, precio);
		setIsbn(isbn);
		setNumPaginas(numPaginas);
		setAutor(autor);
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getNumPaginas() {
		return numPaginas;
	}

	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	public Persona getAutor() {
		return autor;
	}

	public void setAutor(Persona autor) {
		// si el libro pertenece a otro autor
		// se elimina de la lista de este autor
		if (this.autor != null) {
			this.autor.getTrabajos().remove(this);
		}
		this.autor = autor;
		ArrayList<Articulo> lst;
		lst = autor.getTrabajos();
		if (!lst.contains(this)) {
			lst.add(this);
		}
	}

	public String toString() 
     { 
         return super.toString() + " " + getNumPaginas() + " " + getAutor(); 
     }

}
