package fr.eni.editions.ejercicio4;

public class Articulo {
	private int referencia;
	private String titulo;
	private double precio;

	public Articulo() {
		super();
	}

	public Articulo(int referencia) {
		this();
		setReferencia(referencia);
	}

	public Articulo(int referencia, String titulo) {
		this(referencia);
		setTitulo(titulo);
	}

	public Articulo(int referencia, String titulo, double precio) {
		this(referencia, titulo);
		setPrecio(precio);
	}

	public int getReferencia() {
		return referencia;
	}

	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String toString() {
		return getReferencia() + " " + getTitulo() + " " + getPrecio();
	}

}
