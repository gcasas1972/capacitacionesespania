package fr.eni.editions.ejercicio2;

import java.time.Duration;

public class Dvd extends Articulo {
	private Duracion duracion;
	private String escritor;

	public Dvd() {
		super();
	}

	public Dvd(int referencia, String titulo, double precio, Duracion duracion, String escritor) {
		super(referencia, titulo, precio);

		setDuracion(duracion);
		setEscritor(escritor);
	}

	public Duracion getDuracion() {
		return duracion;
	}

	public void setDuracion(Duracion duracion) {
		this.duracion = duracion;
	}

	public String getEscritor() {
		return escritor;
	}

	public void setEscritor(String escritor) {
		this.escritor = escritor;
	}

	public String toString() {
		return super.toString() + " " + getDuracion().toMinutes() + " " + getEscritor();
	}
}
