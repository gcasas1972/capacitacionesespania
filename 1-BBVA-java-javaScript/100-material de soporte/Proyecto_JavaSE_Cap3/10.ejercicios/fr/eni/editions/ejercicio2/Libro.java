package fr.eni.editions.ejercicio2;

public class Libro extends Articulo 
{ 
    private String isbn; 
    private int numPaginas; 
    private String autor; 
     
    public Libro() 
    { 
        super(); 
    } 
      
    public Libro(int referencia,String titulo,double 
precio,String isbn,int numPaginas,String autor) 
    { 
        super(referencia,titulo,precio); 
        setIsbn(isbn); 
        setNumPaginas(numPaginas); 
        setAutor(autor); 
    } 
  
    public String getIsbn() { 
        return isbn; 
    } 
  
    public void setIsbn(String isbn) { 
        this.isbn = isbn; 
    } 
  
    public int getNumPaginas() { 
        return numPaginas; 
    } 
  
    public void setNumPaginas(int numPaginas) { 
        this.numPaginas = numPaginas; 
    } 
  
    public String getAutor() { 
        return autor; 
    } 
  
    public void setAutor(String autor) { 
        this.autor = autor; 
    } 
    public String toString() 
    { 
        return super.toString() + " " + getNumPaginas() + 
" " + getAutor(); 
	}

  } 
