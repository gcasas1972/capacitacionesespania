package fr.eni.editions.ejercicio2;

import java.time.Duration;

public class Principal {
	public static void main(String[] args) {
		Libro l;
		Dvd d;
		l = new Libro();
		l.setReferencia(100);
		l.setTitulo("El cangrejo de las pinzas de oro");
		l.setPrecio(8.5);
		l.setNumPaginas(86);
		l.setAutor("Hergé");
		System.out.println(l.toString());

		d = new Dvd();
		d.setReferencia(110);
		d.setTitulo("La sopa de repollo");
		d.setPrecio(19.50);
		d.setDuracion(Duration.ofMinutes(98));
		d.setEscritor("Girault");
		System.out.println(d.toString());

	}
}
