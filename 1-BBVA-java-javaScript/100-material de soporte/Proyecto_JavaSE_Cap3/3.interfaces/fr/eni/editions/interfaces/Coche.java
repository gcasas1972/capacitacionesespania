package fr.eni.editions.interfaces;

public class Coche implements Classable 

{ 
   private String matriculacion; 
   private String marca; 
   private String modelo; 
   private int potencia; 
   
   public Coche() 
   { 
      super(); 
   } 
    
   public Coche(String matriculacion,
		          String marca,
		          String modelo,
		          int potencia) 
   { 
      this.matriculacion=matriculacion; 
      this.marca=marca; 
      this.modelo=modelo; 
      this.potencia=potencia; 
   } 
 
   public String getMatriculacion() 
   { 
      return matriculacion; 
   } 
 
   public void setMatriculacion(String matriculacion) 
   { 
      this.matriculacion = matriculacion; 
   } 
   public String getMarca()  
   { 
      return marca; 
   } 
   public void setMarca(String marca) 
   { 
      this.marca = marca; 
   } 
   public String getModelo()  
   { 
      return modelo; 
} 
 
   public void setModelo(String modelo)  
   { 
      this.modelo = modelo; 
   } 
   public int getPotencia()  
   { 
      return potencia; 
   } 
 
   public void setPotencia(int potencia)  
   { 
      this.potencia = potencia; 
   } 
 
   /**
    * Comparación de la potencia del coche
    */
   @Override 
   public int compare(Object o)  
   { 
      Coche v; 
      if (o instanceof Coche) 
      { 
         v=(Coche)o; 
      } 
      else 
      { 
         return Classable.ERROR; 
      } 
      if (this.getPotencia()<v.getPotencia()) 
      { 
         return Classable.INFERIOR; 
      } 
      if (this.getPotencia()>v.getPotencia()) 
      { 
         return Classable.SUPERIOR; 
      } 
       
      return Classable.IGUAL; 
   } 
   /**
    * @return true si la potencia del coches inferior
    */
   @Override 
   public boolean isInferieur(Object o) 
   { 
      return this.compare(o)==Classable.INFERIOR; 
   }
   /**
    * @return true si la potencia del coches superior
    */
   @Override 
   public boolean isSuperieur(Object o) 
   { 
      return this.compare(o)==Classable.SUPERIOR;
   } 
    
}

