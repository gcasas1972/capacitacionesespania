package fr.eni.editions.interfaces;

import java.time.LocalDate;
import java.util.Arrays;

public class TestClassable {

public static void main(String[] args) 
{
	Persona[] tablaNoOrdenada = new Persona[5]; 
	tablaNoOrdenada[0] = new Persona("toto2", "apellido2",LocalDate.of(1922,2,15)); 
	tablaNoOrdenada[1] = new Persona("toto1", "apellido1",LocalDate.of(1911,1,15)); 
	tablaNoOrdenada[2] = new Persona("toto5", "apellido5",LocalDate.of(1955,05,15)); 
	tablaNoOrdenada[3] = new Persona("toto3", "apellido3",LocalDate.of(1933,03,15)); 
	tablaNoOrdenada[4] = new Persona("toto4", "apellido4",LocalDate.of(1944,04,15)); 
	Persona[] tablaOrdenada; 
	tablaOrdenada=(Persona[])tri(tablaNoOrdenada); 
	for (int i=0;i<tablaOrdenada.length;i++) 
	{ 
		System.out.println(tablaOrdenada[i]); 
	}
	
	String[] tablaDeCadenas = {"Hola", "todo", "el", "mundo"};
	//No compila porque la clase String no implementa la interfaz Classable
	//tri(tablaDeCadenas);
}
	
public static Classable[] tri(Classable[] tabla) 
{ 
     int i,j; 
     Classable c; 
     Classable[] tablaOrden;
     tablaOrdenacion = Arrays.copyOf(tabla, tabla.length);
     for (i=0;i< tablaOrden.length;i++) 
     { 
        for( j = i + 1; j<tablaOrden.length;j++) 
        { 
            if (tablaOrden[j].compare(tablaOrden[i])==Classable.INFERIOR) 
            { 
                c = tablaOrden[j]; 
                tablaOrden[j] = tablaOrden[i]; 
                tablaOrden[i] = c; 
            } 
            else if (tablaOrden[j].compare(tablaOrden[i])==Classable.ERROR) 
            { 
                 return null; 
            } 
        } 
     } 
     return tablaOrden; 
}

}
