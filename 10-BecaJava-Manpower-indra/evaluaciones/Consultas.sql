-----**************** ver respositorio *****************

	select alu_id, alu_apellido, alu_nombre, aalu_linkgit
	from alumnos

-- **********************************ver promedios generales de  practicasresueltas*****************************************

-- obtengo el promedio de las notas
Select alu.alu_id, alu.alu_nombre, alu.alu_apellido, count(alu.alu_id) cantNotas, avg(pr_nota) promedio
from alumnos alu, practicasresueltas pr
where alu.alu_id = pr.alu_id
group by alu.alu_id

-- las notas ordenadas por alumno y practica
SELECT  alu.alu_id, pr_id, alu_nombre, alu_apellido,  prac.prac_id, PRAC_NOMBRE,  pr_nota, pr_observaciones, alu_git
FROM practicasresueltas pr , alumnos alu, practicas prac
where alu.alu_id = pr.alu_id
      and pr.prac_id = prac.prac_id
      and alu.alu_id = 4
order by alu_id, prac_id

-- modificar practicas resueltas para un alumno

update practicasresueltas
set pr_observaciones='Al 18 y 21 esta no tiene nada'
where alu_id=1

-- modificar practicas resueltas para un pr_id

update practicasresueltas
set pr_nota=8, pr_observaciones='Al 18 nada, 21 se puso las pilas me gusto como saco el ejercicio Mod4_ejercicio8, muy bueno lo del charAt(0) en el ejercicio11, el ejercicio20 esta raro'
where pr_id=7 || pr_id=8

-- modificar nota
update practicasresueltas
set pr_nota = 9, pr_observaciones='23/09 practica completa'
where pr_id=24

-- insertar practicas resueltas
insert into practicasresueltas(alu_id, prac_id, pr_nota, pr_observaciones)
values(12    ,0       , 1      , 'nada de esto')

-- promedio de notas
select alu_id, avg(pr_nota) promedio
from practicasresueltas
group by alu_id

-- ***********************************fin practicasresueltas***********************************************


---------------------------------inicio practicas ---------------------------------------

-- insertar practicas...
insert into practicas (prac_nombre) values ('practica 9 -jdbc-junit ');

-- actualizar practicas 
update practicas
set prac_nombre ='practica 4-ciclos for-while-do while'
where prac_id=4

-- eliminar practicasresueltas
	delete from practicasresueltas
	where pr_id=78


----------------------------------------fin practicas+---------------------------------------------

--- insertar alumnos
INSERT into alumnos(alu_nombre, alu_apellido, alu_estudios, alu_linkgit) values ('Monsef', 'Bakhtaoui Dahraoui' ,	'FP2  Desarrollo De Aplicaciones Multiplataforma', ' https://github.com/JMonsef/JavaCourse.git')

--  modificar datos de los alumnos *********************
update alumnos
 set alu_linkgit = ' https://gitlab.com/elliotstrike/cursojava2021.git'
where alu_id =1


