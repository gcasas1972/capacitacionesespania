Temario  detallado

1- Your First TypeScript Application
2- Install Node.js
3- Install Git
4- Install TypeScript
5- Install a Programmerís Editor
6- Creating the Project
7- Initializing the Project
8- Creating the Compiler Configuration File
9- Compiling and Executing the Code
10- Defining the Data Model
11- Creating the Todo Item Collection Class
12- Adding Features to the Collection Class

temario de 
Essential TypeScript From Beginner to Pro
